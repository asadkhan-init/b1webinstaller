﻿using B1WebInstaller.BusinessEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using Sap.Data.Hana;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using System.Threading;
using System.Security.AccessControl;
using System.Security.Principal;

namespace B1WebInstaller
{
    public partial class frmB1Setup : MaterialForm
    {
        private int _currentStep = 1;
        private int _totalSteps = 11;

        

        BusinessLogic _businessLogic = null;
        private string _licenseKey = "invalidLicenseKey";
        private string _Name = "";
        private string _Company = "";
        private string _Email = "";
        List<IDictionary<bool, string>> resultList;
        List<ConnectionStatus> finalStepConfigurationList;

        delegate void SetTextCallback(string text);

        bool _isNavigationButtonClick = false;
        bool _isUpdate = true;

        InstallerModel objInstaller = null;

        string _zipFilePath = string.Empty;
        string _zipFileFolder = string.Empty;
        string userLicenseFilePathToTempFolder = string.Empty;
        string userLicenseFilePathToReleaseFolder = string.Empty;

        DataTable dtDatabaseInfo = null;
        public frmB1Setup(string[] args)
        {

            _businessLogic = new BusinessLogic();

            InitializeComponent();
            SetTheme();
            setScreenResolution();
            ShowHideStep(1, true);
            _totalSteps = wizardTab.TabCount;
            objInstaller = new InstallerModel();
            finalStepConfigurationList = new List<ConnectionStatus>();
            picLoader.Visible = false;

            _zipFilePath = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER_ZIP); //Environment.GetEnvironmentVariable("TEMP") + @"\" + Constants.SystemConstants.INSTALLATION_DATA_FOLDER_ZIP;
            _zipFileFolder = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER);
            userLicenseFilePathToTempFolder = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.LICENSE_FILE);


            //RemoveFilesAndFoldersInTempDirectory(); uncomment
            // Helper.SendEmail();

            if (args != null && args.Length == 3)
            {
                _Name = args[0];
                _Company = args[1];
                _Email = args[2];


                FillEdiableUserDetails();

            }


            //ddlDatabaseType.SelectedIndex = 0;

            //var hostName = "195.50.154.117";//"nu-db01.init.sig-noc.net";
            //var userName = "root";
            //var password = "InitAdmin4SIG";
            //var port = 22;

            RenderInstruction();

            CreateDataGridTable();

            LoadInstallerInCaseOfUpdate();

            // show controls if update


            //PopulateDatabasesToBeIncludedList();

            //JValue s1 = new JValue("A string");
            //JValue s2 = new JValue("A string");
            //JValue s3 = new JValue("A STRING");

            //var x = JToken.DeepEquals(s1, s2);
            //// true

            //var y = JToken.DeepEquals(s2, s3);

        }

      

        #region Button Events
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            _isNavigationButtonClick = true;

            if (_currentStep == (_totalSteps - 1))
            {
                if (PerformValidation())
                {
                    PerformNavigationToNextStep();
                    btnNext.Visible = false;
                    btnFinish.Visible = true;
                    FillIntallerModel();
                    ShowInstallationFinalStep();
                }
            }
            else if (PerformValidation())
            {
                PerformNavigationToNextStep();

                ButtonPreviewInCaseOfNext();
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            _isNavigationButtonClick = true;

            if (_currentStep == 2)
            {
                btnBack.Visible = false;
                PerformNavigationToBackStep();
            }
            else if (_currentStep > 1)
            {
                PerformNavigationToBackStep();
                ButtonPreviewInCaseOfBack();
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            //backgroundWorker1.RunWorkerAsync(2000);
            if (cbUpdateDatabase.Checked)
            {
                if (!(objInstaller.SBOModelList.Count > 0 && cblIncludedDatabases.CheckedItems.Count > 0))
                {
                    MessageBox.Show(Constants.SystemMessages.DATABASE_LIST_EMPTY);
                    return;
                }
            }
            
            if((cbUpdateDatabase.Checked) || (cbCopyRelease.Checked) || (cbUpdateIIS.Checked) || (cbUpdateSP.Checked))
            {
                //FillIntallerModel();
                //_businessLogic.SetUrlConstants(txtServiceLayerIP.Text,txtServiceLayerIPPort.Text);
                //ShowInstallationFinalStep();
                StartInstallation();
            }
            else
            {
                MessageBox.Show(Constants.SystemMessages.SELECT_INSTALLATION_OPTION);
            }

        }

        private void FillDatabaseList()
        {

            foreach (DataGridViewRow row in gvDatabaseInformation.Rows)
            {
                var DatabaseName = Convert.ToString(row.Cells["Database"].Value);
                var SBOUserName = Convert.ToString(row.Cells["UserName"].Value);
                var SBOPassword = Convert.ToString(row.Cells["Password"].Value);
                var SBOLocalization = Convert.ToString(row.Cells["Localization"].Value);
                

                SBOUserModel model = new SBOUserModel();
                //model.serviceLayerUrl = txtServiceLayerIP.Text;
                //model.serviceLayerPort = txtServiceLayerIPPort.Text;
                model.sboUserName = SBOUserName;
                model.sboPassword = SBOPassword;
                model.database = DatabaseName;
                model.localization = SBOLocalization;

                objInstaller.SBOModelList.Add(model);
            }
        }
        private bool UpdateConfiguration()
        {

            //ServiceLayerUrl
            //Companies
            //DatabaseServer
            //LicenseServer
            //DatabaseType
            //DatabaseLanguage
            //AppKey
            //PathToDocumentsDirectory
            //PathToEVNsDirectory
            //CORS
            //NumberPoolObject
            //XSEngineUrl
            //SBOUsername                   
            //SBOPassword
            //SecureWebsockets                    
            //SecureWebsocketsCertPath
            //SecureWebsocketsCertPassword
            //CategorySP
            //KeyResolverSP                               
            //UserManagerPassword

            try
            {
                var backEndFonfigPath = Path.Combine(objInstaller.BackendPath, "web.config");

                if (!File.Exists(backEndFonfigPath))
                {
                    return false;
                }

                var serviceLayerURL = string.Format("{0}:{1}{2}", objInstaller.ServiceLayerIP, objInstaller.ServiceLayerPort, "/b1s/v1/");
                objInstaller.ServiceLayerUrl = serviceLayerURL;
                Helper.UpdateConfigurationFile(backEndFonfigPath, "ServiceLayerUrl", serviceLayerURL);


                //replace with concatenation
                Helper.UpdateConfigurationFile(backEndFonfigPath, "Companies", GetDatabaseString());


                var databaseServerUrl = string.Format("{0}:{1}", objInstaller.ServiceLayerIP.Replace("http://", "").Replace("https://", ""), objInstaller.DatabaseEnginePort);
                var encryptedPassword = Helper.EncryptStringAES(objInstaller.UserManagementPassword);

                Helper.UpdateConfigurationFile(backEndFonfigPath, "DatabaseServer", databaseServerUrl);
                //Helper.UpdateConfigurationFile(backEndFonfigPath, "AppKey", txtDatabaseNames.Text);

                if(string.IsNullOrEmpty(objInstaller.SSLPath))
                    Helper.UpdateConfigurationFile(backEndFonfigPath, "SecureWebsockets", "false");
                
                Helper.UpdateConfigurationFile(backEndFonfigPath, "SecureWebsocketsCertPath", objInstaller.SSLPath);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "SecureWebsocketsCertPassword", objInstaller.SSLPassword);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "UserManagerPassword", encryptedPassword);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "License", objInstaller.LicenseKey);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "DatabaseType", objInstaller.DatabaseType);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "SBOUsername", objInstaller.HanaSQLUserName);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "SBOPassword", objInstaller.HanaSQLPassword);
               

                var sshEncryptedPassword = string.Empty;

                if (rbDatabaseTypeHana.Checked && !string.IsNullOrEmpty(objInstaller.SSHUserName) && !string.IsNullOrEmpty(objInstaller.SSHPassword))
                {

                    Helper.UpdateConfigurationFile(backEndFonfigPath, "SSHUsername", objInstaller.SSHUserName);
                    sshEncryptedPassword = Helper.EncryptStringAES(objInstaller.SSHPassword);
                    Helper.UpdateConfigurationFile(backEndFonfigPath, "SSHPassword", sshEncryptedPassword);
                }

                

                var LicenseServerUrl = string.Format("{0}:{1}", objInstaller.ServiceLayerIP.Replace("http://", "").Replace("https://", ""), objInstaller.LicenseServerPort);
                Helper.UpdateConfigurationFile(backEndFonfigPath, "LicenseServer", LicenseServerUrl);

                var XSEngineUrl = string.Format("{0}:{1}/{2}", objInstaller.ServiceLayerIP.Replace("http://", "").Replace("https://", ""), objInstaller.XSEnginePort, "sap/sbo/platform/");
                Helper.UpdateConfigurationFile(backEndFonfigPath, "XSEngineUrl", XSEngineUrl);

                var CORSString = PrepareCORsString();
                Helper.UpdateConfigurationFile(backEndFonfigPath, "CORS", CORSString);

                //  Helper.UpdateConfigurationFile(configPath, "SecureWebsockets", txtDatabaseNames.Text);

                var frontEndFonfigPath = Path.Combine(objInstaller.FrontEndPath, "web.config");

                if (!File.Exists(frontEndFonfigPath))
                {
                    return false;
                }

                var frontEndEnvFile = Path.Combine(objInstaller.FrontEndPath, "assets", "config", "env.json");

                Helper.UpdateConfigurationFile(frontEndFonfigPath, "UserManagerPassword", encryptedPassword);

                var valueToBeReplacedEndpoint = string.Empty;

                if (string.IsNullOrEmpty(txtSSLCertificatePath.Text))
                {
                    valueToBeReplacedEndpoint = string.Format("{0}://{1}:{2}/{3}", "http", objInstaller.SiteInfo.DomainName,objInstaller.SiteInfo.Port, "api/");
                }
                else
                {
                    valueToBeReplacedEndpoint = string.Format("{0}://{1}:{2}/{3}", "https", objInstaller.SiteInfo.DomainName, objInstaller.SiteInfo.Port, "api/");
                }

                var valueToBeReplacedWSEndpoint = string.Format("{0}://{1}:{2}", "wss", objInstaller.SiteInfo.DomainName, "8081");

                Helper.EditJSONFile(frontEndEnvFile, "endpoint", valueToBeReplacedEndpoint);
                Helper.EditJSONFile(frontEndEnvFile, "ws_endpoint", valueToBeReplacedWSEndpoint);

                objInstaller.B1WebClientVersion = Helper.ReadJSONKey(frontEndEnvFile, "version");
              
                return true;
            }
            catch (Exception ex)
            {
                Helper.LogMessage(ex.Message);
                return false;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtB1FrontendFolderPath.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnBrowseBackendPath_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtB1BackendFolderPath.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnSSLCertificatePath_Click(object sender, EventArgs e)
        {
            DialogResult result = opd.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                txtSSLCertificatePath.Text = opd.FileName;
            }
        }
        #endregion
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // StartInstallation(); 
        }

        private void btnGenerateLicenseKey_Click(object sender, EventArgs e)
        {
            Thread threadInput = new Thread(GenerateLicenseKey);
            threadInput.Start();
        }

        private void GenerateLicenseKey()
        {

            SetLoading(true);


            try
            {
                // Generate License Key
                this.Invoke((MethodInvoker)delegate
                {
                    if (string.IsNullOrEmpty(txtInstallationNumber.Text))
                    {
                        MessageBox.Show(this, Constants.SystemMessages.INSTALLATION_NO_REQUIRED);
                    }
                    else if (string.IsNullOrEmpty(txtEmailEditable.Text) || !Helper.IsValidEmail(txtEmailEditable.Text))
                    {
                        MessageBox.Show(Constants.SystemMessages.INVALID_EMAIL);
                        //pnlUserDetails.IsExpanded = true;
                    }

                    else
                    {

                        _licenseKey = _businessLogic.GetLicenseKey(_Name, _Company, _Email, txtInstallationNumber.Text);
                        txtB1License.Enabled = true;
                        MessageBox.Show(this, string.Format(Constants.SystemMessages.LICENSE_KEY_SENT, _Email));

                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            SetLoading(false);

        }
        private void btnAddUCDatabaseInformation_Click(object sender, EventArgs e)
        {
            //Thread threadInput = new Thread(AddDatabaseToGrid);
            //threadInput.Start();

            AddDatabaseToGrid();
        }

        private void AddDatabaseToGrid()
        {

            SetLoading(true);


            try
            {

                var dbInfo = ucDatabaseInformation1.GetDatabaseInformation();

                if (dbInfo.Localization != Constants.SystemConstants.PLEASE_SELECT)
                {
                    SBOUserModel model = new SBOUserModel();

                    model.sboUserName = dbInfo.SBOUserName;
                    model.sboPassword = dbInfo.SBOPassword;
                    model.database = dbInfo.DatabaseName;
                    model.localization = dbInfo.Localization;

                    //this.Invoke((MethodInvoker)delegate
                    //{
                    var result = _businessLogic.SendLoginRequest(model);

                    if (result.State)
                    {
                        if (!FindDatabaseInGrid(dbInfo.DatabaseName))
                        {
                            PopulateDataGrid(model);
                            //gvDatabaseInformation.Rows.Add(dbInfo.DatabaseName, dbInfo.SBOUserName, dbInfo.SBOPassword,dbInfo.Localization);
                            //FillFinalStepConfigurationForSBO(result, model);
                        }
                        else
                        {
                            MessageBox.Show(Constants.SystemMessages.DATABASE_ALREADY_EXISTS);
                        }
                    }
                    else
                    {
                        //FillFinalStepConfigurationForSBO(result, model);
                        MessageBox.Show(Constants.SystemMessages.UNABLE_TO_CONNECT_WITH_SERVICE_LAYER);
                    }
                }
                else {

                    MessageBox.Show(Constants.SystemMessages.SELECT_LOCALIZATION);
                }
                //});
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            SetLoading(false);
        }

        private void CreateDataGridTable()
        {
            //dbInfo.DatabaseName, dbInfo.SBOUserName, dbInfo.SBOPassword,dbInfo.Localization
            dtDatabaseInfo = new DataTable();
            // add column to datatable  

            dtDatabaseInfo.Columns.Add("Include", typeof(bool));
            dtDatabaseInfo.Columns.Add("Database", typeof(string));
            dtDatabaseInfo.Columns.Add("UserName", typeof(string));
            dtDatabaseInfo.Columns.Add("Password", typeof(string));
            dtDatabaseInfo.Columns.Add("Localization", typeof(string));
        }
        private void PopulateDataGrid(SBOUserModel dbInfo)
        {
            //DataGridViewCheckBoxColumn dgvCmb = new DataGridViewCheckBoxColumn();
            //dgvCmb.ValueType = typeof(bool);
            //dgvCmb.Name = "Include";
            //dgvCmb.HeaderText = "Include";
            //dgvCmb.TrueValue = true;
            //dgvCmb.FalseValue = false;
            //gvDatabaseInformation.Columns.Add(dgvCmb);

            try
            {
                dtDatabaseInfo.Rows.Add(true, dbInfo.database, dbInfo.sboUserName, dbInfo.sboPassword, dbInfo.localization);
                gvDatabaseInformation.DataSource = dtDatabaseInfo;
                gvDatabaseInformation.Columns["Password"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }

        private void btnTestSSHConnection_Click(object sender, EventArgs e)
        {
            Thread threadInput = new Thread(CheckSSHConnection);
            threadInput.Start();
        }

        private void CheckSSHConnection()
        {

            SetLoading(true);

            this.Invoke((MethodInvoker)delegate
            {
                var sshConnection = _businessLogic.CheckSSHConnection(txtServiceLayerIP.Text, 22, txtSSHRootUserName.Text, txtSSHRootPassword.Text);

                if (sshConnection.State)
                {
                    MessageBox.Show(Constants.SystemMessages.SSH_CONNECTION_SUCCESSFUL);
                }
                else
                {
                    MessageBox.Show(this, Constants.SystemMessages.SSH_CONNECTION_FAILED);
                }


                FillFinalStepConfigurationForSSHConnection(sshConnection);
            });

            SetLoading(false);
        }
       

        private void FillFinalStepConfigurationForSBO(ConnectionStatus result, SBOUserModel dbInfo)
        {

            var isValid = result.State;
            var text = string.Empty;
            //var sboConnectionResult = new Dictionary<bool, string>();

            if (isValid)
            {
                text = string.Format(" Connection successful for Database : {0} ", dbInfo.database);

                if (!FindDatabaseInGrid(dbInfo.database))
                {
                    PopulateDataGrid(dbInfo);
                    //gvDatabaseInformation.Rows.Add(dbInfo.database, dbInfo.sboUserName, dbInfo.sboPassword);
                }

            }
            else
            {
                text = string.Format(" Connection failed for Database : {0} ", dbInfo.database);
            }

            result.Message = text;
            //sboConnectionResult.Add(isValid, text);


            if (finalStepConfigurationList.Count > 0)
            {
                var sbo = finalStepConfigurationList.Where(i => i.Type == Constants.ConnectionType.DATABASE && i.Message.Contains(dbInfo.database)).FirstOrDefault();

                if (sbo != null)
                    finalStepConfigurationList.Remove(sbo);
            }

            //var valueAlreadyExists = finalStepConfigurationList.FirstOrDefault(d => d.Values.Contains(text));

            //if (valueAlreadyExists == null)
            finalStepConfigurationList.Add(result);

        }
        private void FillFinalStepConfigurationForSSHConnection(ConnectionStatus sshConnection)
        {
            //var isValid = sshConnection.State;
            //var text = sshConnection.Message;
            //var sshConnectionResult = new Dictionary<bool, string>();
            //sshConnectionResult.Add(isValid, text);

            if (finalStepConfigurationList.Count > 0)
            {
                var sbo = finalStepConfigurationList.Where(i => i.Type == Constants.ConnectionType.SSH).FirstOrDefault();

                if (sbo != null)
                    finalStepConfigurationList.Remove(sbo);
            }

            finalStepConfigurationList.Add(sshConnection);
        }

        private void btnUpdateUserDetails_Click(object sender, EventArgs e)
        {

        }

        private void wizardTab_Selecting(object sender, TabControlCancelEventArgs e)
        {
            //MessageBox.Show("selection in progress");
            try
            {
                if (!_isNavigationButtonClick)
                {
                    var tabControl = (MaterialTabControl)sender;
                    var selectedTab = tabControl.SelectedTab;
                    var tabNo = Convert.ToInt32(selectedTab.Text);

                    if (tabNo > _currentStep)
                    {
                        if (_currentStep == (_totalSteps - 1))
                        {
                            if (PerformValidation())
                            {
                                PerformNavigationToNextStep();
                                btnNext.Visible = false;
                                btnCancel.Visible = true;
                            }
                        }
                        else if (PerformValidation())
                        {
                            PerformNavigationToNextStep();

                            ButtonPreviewInCaseOfNext();
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        if (_currentStep == 2)
                        {
                            btnBack.Visible = false;
                            PerformNavigationToBackStep();
                        }
                        else if (_currentStep > 1)
                        {
                            PerformNavigationToBackStep();
                            ButtonPreviewInCaseOfBack();
                        }
                    }
                }

                _isNavigationButtonClick = false;

            }
            catch (Exception ex)
            {
            }
        }

        #region Private Methods

        private async void StartInstallation()
        {

            try
            {
                lblProgressText.Visible = true;
                btnFinish.Enabled = false;

                // stop iis 
                _businessLogic.StopIIS(objInstaller);
                _businessLogic.StopAppPool(objInstaller);

                if (cbCopyRelease.Checked || cbUpdateDatabase.Checked)
                {
                    //ExtractReleaseFiles(); uncomment
                }

                var areFilesCopy = true;//cbCopyRelease.Checked ?  CopyReleaseFiles() : true; uncomment


                if (areFilesCopy)
                {
                    if (!_isUpdate)
                    {
                        Helper.LogMessage("-- Clearing UDS --");
                        UpdateProgress(" Clearing UDS ");
                        await ClearUDs();
                        Helper.LogMessage("-- Done Clearing UDS --");
                        UpdateProgress(" Done Clearing UDS ");
                    }

                    DoProgress(5);

                    Helper.LogMessage("-- Updating Configuration --");
                    UpdateProgress(" Updating Configuration ");
                    //lblProgressText.Text = "";
                    //lblProgressText.Refresh();

                    // Update Configuration 

                    if (UpdateConfiguration())
                    {
                        DoProgress(5);

                        Helper.LogMessage("-- Configuration Updated --");
                        //lblProgressText.Text = "Configuration Updated ";
                        //lblProgressText.Refresh();
                        UpdateProgress(" Configuration Updated  ");

                        var areUDsCreated = true;

                        if (cbUpdateDatabase.Checked)
                        {
                            Helper.LogMessage("-- Creating UDs --");
                            //lblProgressText.Text = "Creating UDs ";
                            //lblProgressText.Refresh();

                            UpdateProgress(" Creating UDs  ");

                            //Add UDs
                            areUDsCreated = await AddUDs();
                        }

                        if (areUDsCreated)
                        {
                            if (cbUpdateSP.Checked)
                            {
                                ExecuteStoredProcudure();
                            }

                            DoProgress(5);

                            // Run Model Generator
                            RunModelGenerator();

                            // create application pool
                            if (cbUpdateIIS.Checked)
                            {
                                var appPoolResult = _businessLogic.CreateAppPool(objInstaller.SiteInfo.ApplicationPoolName);

                                var isAppPoolCreated = appPoolResult.Keys.FirstOrDefault();
                                var appPoolMessage = appPoolResult[isAppPoolCreated];

                                // if (isAppPoolCreated)
                                // {
                                UpdateProgress(appPoolMessage);

                                DoProgress(3);

                                // create IIS Site
                                var iisSiteResult = _businessLogic.CreateSiteInIIS(objInstaller);

                                var iisSitCreated = iisSiteResult.Keys.FirstOrDefault();
                                var iisSitMessage = iisSiteResult[iisSitCreated];

                                //if (iisSitCreated)
                                // {
                                UpdateProgress(" IIS Site created  ");
                            }

                            DoProgress(3);

                            DoEntriesInRegistry();

                            UpdateProgress(" Registry Updated ");

                            DoProgress(2);


                            UpdateProgress(" Removing unwanted files  ");

                            RemoveFilesAndFoldersInTempDirectory();

                            DoProgress(2);


                            Helper.LogMessage("-- Installation Finished--");
                            UpdateProgress(" Installation Finished  ");

                            btnFinish.Visible = false;
                            btnBack.Visible = false;
                            //}
                            //else {

                            //    MessageBox.Show(iisSitMessage);
                            //}
                            //}
                            //else
                            //{
                            //    MessageBox.Show(appPoolMessage);

                            //}
                        }
                        else
                        {
                            MessageBox.Show(Constants.SystemMessages.UDS_UPDATE_FAILED);
                            btnFinish.Enabled = true;
                        }
                        //lblProgressText.Text = "Installation Finished";
                        //lblProgressText.Refresh();
                    }
                    else
                    {

                        MessageBox.Show(Constants.SystemMessages.CONFIGURATION_UPDATE_FAILED);
                        btnFinish.Enabled = true;
                    }

                }
                else
                {
                    MessageBox.Show(this, Constants.SystemMessages.COPY_FILE_FAILED);
                    btnFinish.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.SystemMessages.GENERAL_ERROR + " " + ex.Message);
            }

            // start IIS again
            _businessLogic.StartIIS(objInstaller);
            _businessLogic.StartAppPool(objInstaller);
        }

        private void RunModelGenerator()
        {
            // Run Model Generator

            foreach (var model in objInstaller.SBOModelList)
            {
                UpdateProgress(" Generating Meta Data Started ");

                _businessLogic.RunModelGenerator(Constants.CommandLineArguments.METADATA, model, objInstaller.ServiceLayerUrl);

                _businessLogic.RunModelGenerator(Constants.CommandLineArguments.IMPORT, model, objInstaller.ServiceLayerUrl);

                UpdateProgress(" Generating Meta Data  End ");
            }
        }

        private void DoEntriesInRegistry()
        {
            try
            {
                // encrypt passwords before entry in registry
                objInstaller.SBOModelList.ForEach(s => s.sboPassword = Helper.EncryptStringAES(s.sboPassword));


                if (!string.IsNullOrEmpty(objInstaller.HanaSQLPassword))
                    objInstaller.HanaSQLPassword = Helper.EncryptStringAES(objInstaller.HanaSQLPassword);

                if (!string.IsNullOrEmpty(objInstaller.SSHPassword))
                    objInstaller.SSHPassword = Helper.EncryptStringAES(objInstaller.SSHPassword);

                if (!string.IsNullOrEmpty(objInstaller.SSLPassword))
                    objInstaller.SSLPassword = Helper.EncryptStringAES(objInstaller.SSLPassword);

                if (!string.IsNullOrEmpty(objInstaller.UserManagementPassword))
                    objInstaller.UserManagementPassword = Helper.EncryptStringAES(objInstaller.UserManagementPassword);

                _businessLogic.SetInstallerInformationEntriesInRegistry("INSTALLER_SETTINGS", objInstaller);
            }
            catch (Exception ex)
            {

            }

        }
        private InstallerModel ReadInsatllerSettingsFromRegistry()
        {

            var strInstallerSettings = _businessLogic.ReadRegistryInformation("INSTALLER_SETTINGS");

            if (strInstallerSettings != null)
            {
                var objInstallerModel = JsonConvert.DeserializeObject<InstallerModel>(strInstallerSettings);
                return objInstallerModel;
            }

            return null;
        }
        private void ExecuteStoredProcudure()
        {

            //creating stored procedures for hana
            foreach (var row in objInstaller.SBOModelList)
            {
                //lblProgressText.Text = "Creating SPs ";
                //lblProgressText.Refresh();
                UpdateProgress(" Creating SPs  " + row.database);

                if (rbDatabaseTypeHana.Checked)
                {
                    _businessLogic.CreateStoredProcedureForHana(row.database);
                }
                else
                {
                    
                    _businessLogic.DropStoredProcedureForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text);

                    _businessLogic.CreateStoredProcedureForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text);

                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_DELETE_SQL);


                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_SALESB1WEBQUERY);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_ACCBAL);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_LASTEVALPR);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_LASTPURPR);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_OCLGASSGN);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_OCLS);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_OOIR);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_OPMGOPENACC);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_B1WEB_OPRT);
                    _businessLogic.ExecuteViewQueryForSQL(txtSQLServerInstanceName.Text, row.database, txtHANAUser.Text, txtHANAUserPassword.Text, Constants.StoredProcedures.VIEW_CREATE_SQL_MONTHNAMEQUERY);
                }
                //lblProgressText.Text = "SPs Successfully  ";
                //lblProgressText.Refresh();
                UpdateProgress(" SPs created Successfully  " + row.database);

            }
        }
        private void RenderInstruction()
        {

            var resouceFileName = "B1WebInstaller.readme.txt";
            using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resouceFileName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    txtInstructions.Text = reader.ReadToEnd();
                }
            }
        }
        private bool CopyReleaseFiles()
        {
            try
            {
                //string _zipPath = Environment.GetEnvironmentVariable("TEMP") + @"\" + Constants.SystemConstants.INSTALLATION_DATA_FOLDER_ZIP;


                //var filePath = Constants.SystemConstants.LOG_DIR_PATH;
                //Helper.EmptyFolder(filePath);

                KeepBackupOfLicenseFileInCaseOfUpdate();


                Helper.LogMessage("-- Installation Started --");
                UpdateProgress(" Installation Started ");
                //lblProgressText.Text = "installation started";
                //lblProgressText.Refresh();


                Helper.LogMessage("-- Removing Source Files if already exist--");
                UpdateProgress(" Removing Source Files if already exist ");
                //lblProgressText.Text = "Removing Source Files if already exist";
                //lblProgressText.Refresh();

                // remove all folders if already exists
                Helper.EmptyFolder(objInstaller.BackendPath);
                Helper.EmptyFolder(objInstaller.FrontEndPath);
                //Helper.EmptyFolder(Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER));
                //Helper.RemoveFile(Path.Combine(_zipFilePath));

                Helper.LogMessage("-- Done Removing Source Files if already exist--");
                UpdateProgress(" Done Removing Source Files if already exist ");
                //lblProgressText.Text = "Done Removing Source Files if already exist";
                //lblProgressText.Refresh();


                
                //lblProgressText.Text = " Done Extracting Source Files";
                //lblProgressText.Refresh();

                Helper.LogMessage("-- Copying Backend --");
                UpdateProgress(" Copying Backend ");
                //lblProgressText.Text = "Copying Backend";
                //lblProgressText.Refresh();

                // Copy frontend and backend files
                var backendSourceFolder = string.Empty;


                //if (rbDatabaseTypeHana.Checked)
                //{
                backendSourceFolder = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "Backend");
                //}
                //else
                //{
                //    backendSourceFolder = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "SQLBACKEND");
                //}

                var isBackendCopied = _businessLogic.CopyFilesToInstallerDirectory(Path.Combine(objInstaller.BackendPath), backendSourceFolder);

                Helper.LogMessage("-- Backend Copied --");
                UpdateProgress(" Backend Copied");
                //lblProgressText.Text = "Backend Copied";
                //lblProgressText.Refresh();

                DoProgress(10);

                Helper.LogMessage("-- Copying Frontend --");
                UpdateProgress(" Copying Frontend  ");
                //lblProgressText.Text = "Copying Frontend";
                //lblProgressText.Refresh();

                var frontendSourceFolder = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "Frontend");
                var isFrontendCopied = _businessLogic.CopyFilesToInstallerDirectory(Path.Combine(objInstaller.FrontEndPath), frontendSourceFolder);

                Helper.LogMessage("-- Frontend Copied --");
                UpdateProgress(" Frontend Copied  ");
                //lblProgressText.Text = "Frontend Copied";
                //lblProgressText.Refresh();

                if (_isUpdate)
                {
                    try
                    {
                        userLicenseFilePathToReleaseFolder = Path.Combine(objInstaller.BackendPath, "App_Data", "user.lic");
                        File.Delete(userLicenseFilePathToReleaseFolder);
                        File.Copy(userLicenseFilePathToTempFolder, userLicenseFilePathToReleaseFolder,true);
                        File.Delete(userLicenseFilePathToTempFolder);
                    }
                    catch (Exception ex)
                    {
                        Helper.LogMessage(" CopyReleaseFiles() " + ex.Message);
                    }
                }

                DoProgress(10);

                // copy crystal report template files
                _businessLogic.CreateCrystalReportFolders(objInstaller);

                // give permissions to folder
                var accountType = "IIS_IUSRS";
                Helper.GiveFolderPermission(accountType, objInstaller.BackendPath);
                Helper.GiveFolderPermission(accountType, objInstaller.FrontEndPath);
                
                return (isBackendCopied && isFrontendCopied);
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" CopyReleaseFiles() " + ex.Message);
                return false;
            }
        }

        private void KeepBackupOfLicenseFileInCaseOfUpdate()
        {
            userLicenseFilePathToReleaseFolder = Path.Combine(objInstaller.BackendPath, "App_Data", Constants.SystemConstants.LICENSE_FILE);
            // in case of update copy user.lic file to temp folder so that it can be replaced again to release file to restore user license/ passwords
            if (_isUpdate)
            {
                Helper.LogMessage("-- Copy  License file to temp folder --");
                UpdateProgress(" Copy  License file to temp folder ");

                if (File.Exists(userLicenseFilePathToReleaseFolder))
                    File.Copy(userLicenseFilePathToReleaseFolder, userLicenseFilePathToTempFolder, true);
            }
        }

       
        private void ExtractReleaseFiles()
        {
            Helper.LogMessage(" Extracting Source Files ");
            UpdateProgress(" Extracting Source Files ");
            //lblProgressText.Text = "Extracting Source Files";
            //lblProgressText.Refresh();

            Helper.ExtractZip(_zipFilePath, Constants.SystemConstants.TEMP_PATH, "B1WebInstaller.Installationdata.zip");

            Helper.LogMessage("-- Done Extracting Source Files--");
            UpdateProgress(" Done Extracting Source Files ");
        }

        private void setScreenResolution()
        {

            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            int w = 850;//Width >= screen.Width ? screen.Width : Convert.ToInt32((screen.Width + Width) / 2.5);
            int h = 600;//Height >= screen.Height ? screen.Height : (screen.Height + Height) / 2;
            this.Location = new Point((screen.Width - w) / 2, (screen.Height - h) / 2);
            this.Size = new Size(w, h);

        }
        private void PerformNavigationToBackStep()
        {

            //ShowHideStep(_currentStep, false);
            _currentStep--;
            wizardTab.SelectedTab = wizardTab.TabPages[_currentStep - 1];
            //ShowHideStep(_currentStep, true);
        }
        private void PerformNavigationToNextStep()
        {
            _currentStep++;
            wizardTab.SelectedTab = wizardTab.TabPages[_currentStep - 1];


            // ShowHideStep(_currentStep, false);

            //  ShowHideStep(_currentStep, true);

            if (string.IsNullOrEmpty(txtB1License.Text) && _currentStep == 2)
            {

                //txtB1License.Text = Helper.GenerateRandomString(16);
            }
        }
        private bool PerformValidation()

        {
            string errorMessage = string.Empty;

            if (_currentStep == 1)
            {
                
            }

            else if (_currentStep == 2)
            {
                if (!rbDatabaseTypeHana.Checked && !rbDatabaseTypeSQL.Checked)
                {
                    MessageBox.Show(" Please select database type. ");
                    return false;
                }

            }

            else if (_currentStep == 3)
            {
                errorMessage = _businessLogic.CheckRequiredField(txtB1FrontendFolderPath);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtB1BackendFolderPath);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }


            }

            else if (_currentStep == 4)
            {
                errorMessage = _businessLogic.CheckRequiredField(txtInstallationNumber);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }


                errorMessage = _businessLogic.CheckRequiredField(txtUserNameEditable);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtCompanyNameEditable);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtEmailEditable);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }

                if (!Helper.IsValidEmail(txtEmailEditable.Text))
                {
                    MessageBox.Show(Constants.SystemMessages.INVALID_EMAIL);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }


                errorMessage = _businessLogic.CheckRequiredField(txtPhoneNo);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }

                if (!Helper.ValidatePhoneNumber(txtPhoneNo.Text, true))
                {
                    MessageBox.Show(Constants.SystemMessages.INVALID_PHONE_NO);
                    //pnlUserDetails.IsExpanded = true;
                    return false;
                }


                _Name = txtUserNameEditable.Text;
                _Company = txtCompanyNameEditable.Text;
                _Email = txtEmailEditable.Text;


                errorMessage = _businessLogic.CheckRequiredField(txtB1License);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                if (txtB1License.Text.Trim().Length != 172)
                {
                    MessageBox.Show(Constants.SystemMessages.INVALID_LICENSE_KEY);
                    return false;
                }

                FillEdiableUserDetails();

            }


            else if (_currentStep == 5)
            {


                errorMessage = _businessLogic.CheckRequiredField(txtUserManagementPassword);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }


                errorMessage = _businessLogic.CheckRequiredField(txtConfirmUserManagementPassword);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                if (txtUserManagementPassword.Text != txtConfirmUserManagementPassword.Text)
                {
                    MessageBox.Show(Constants.SystemMessages.PASSWORD_UNMACHTED);
                    return false;
                }

            }

            else if (_currentStep == 6)
            {

                errorMessage = _businessLogic.CheckRequiredField(txtServiceLayerIP);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtHANAUserPassword);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtServiceLayerIPPort);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtXSEnginePort);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtDatabaseEnginePort);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtLicenseServerPort);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                

                if (ddlDatabaseType.Text == Constants.SystemConstants.PLEASE_SELECT)
                {
                    MessageBox.Show(ddlDatabaseType.Tag.ToString());
                    return false;
                }

                //db connection check
                //hana connection check
                if (rbDatabaseTypeHana.Checked)
                {
                    SetLoading(true);

                    var result = _businessLogic.CheckHanaStudioConnection(txtServiceLayerIP.Text, txtDatabaseEnginePort.Text, txtHANAUser.Text, txtHANAUserPassword.Text);

                    //var databaseList = _businessLogic.GetHANADatabaseList(txtServiceLayerIP.Text, txtDatabaseEnginePort.Text, txtHANAUser.Text, txtHANAUserPassword.Text);

                    FillFinalStepConfigurationForDatabaseConnection(result);

                   // ucDatabaseInformation1.BindDatabaseList(databaseList);

                    SetLoading(false);

                    return result.State;
                }
                else
                {
                    SetLoading(true);

                    var result = _businessLogic.CheckSQLStudioConnection(txtSQLServerInstanceName.Text, "master", txtHANAUser.Text, txtHANAUserPassword.Text);

                    //var databaseList = _businessLogic.GetSQLDatabaseList(txtSQLServerInstanceName.Text, "master", txtHANAUser.Text, txtHANAUserPassword.Text);

                    FillFinalStepConfigurationForDatabaseConnection(result);

                   // ucDatabaseInformation1.BindDatabaseList(databaseList);

                    SetLoading(false);

                    return result.State;
                }
              
            }

          

            else if (_currentStep == 8)
            {
               
                errorMessage = _businessLogic.CheckRequiredField(txtSiteName);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtPortNumber);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtApplicationPool);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtIPAddress);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(ddlProtocolType);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                errorMessage = _businessLogic.CheckRequiredField(txtDomainName);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }
            }


            else if (_currentStep == 9)
            {
                errorMessage = _businessLogic.CheckRequiredField(txtCertificatePassword);

                if (!string.IsNullOrEmpty(errorMessage) && !string.IsNullOrEmpty(txtSSLCertificatePath.Text))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }


                errorMessage = _businessLogic.CheckRequiredField(txtConfirmCertificatePassword);

                if (!string.IsNullOrEmpty(errorMessage) && !string.IsNullOrEmpty(txtSSLCertificatePath.Text))
                {
                    MessageBox.Show(errorMessage);
                    return false;
                }

                if ((txtCertificatePassword.Text != txtConfirmCertificatePassword.Text && !string.IsNullOrEmpty(txtSSLCertificatePath.Text)))
                {
                    MessageBox.Show(Constants.SystemMessages.PASSWORD_UNMACHTED);
                    return false;
                }

                _businessLogic.SetUrlConstants(txtServiceLayerIP.Text, txtServiceLayerIPPort.Text);
                //errorMessage = _businessLogic.CheckRequiredField(txtSBOUserName);

                //if (!string.IsNullOrEmpty(errorMessage))
                //{
                //    MessageBox.Show(errorMessage);
                //    return false;
                //}

                //errorMessage = _businessLogic.CheckRequiredField(txtSBOPassword);

                //if (!string.IsNullOrEmpty(errorMessage))
                //{
                //    MessageBox.Show(errorMessage);
                //    return false;
                //}

                // _businessLogic.GetHANADatabaseList(txtServiceLayerIP.Text, txtServiceLayerIPPort.Text, txtSBOUserName.Text, txtSBOPassword.Text);

            }

            else if (_currentStep == 10)
            {
                if (gvDatabaseInformation.Rows.Count == 0)
                {
                    MessageBox.Show(Constants.SystemMessages.DATABASE_LIST_EMPTY);
                    return false;
                }
            }

            return true;
        }
        private void FillFinalStepConfigurationForDatabaseConnection(ConnectionStatus result)
        {
            //var isValid = result.State;

            //var text = result.Message;


            if (finalStepConfigurationList.Count > 0)
            {
                var hanaStatus = finalStepConfigurationList.Where(c => c.Type == Constants.ConnectionType.HANA || c.Type == Constants.ConnectionType.SQL).FirstOrDefault();

                if (hanaStatus != null)
                    finalStepConfigurationList.Remove(hanaStatus);
            }

            //var valueAlreadyExists = finalStepConfigurationList.FirstOrDefault(d => d.Values.Contains(text));

            //if (valueAlreadyExists == null)
            finalStepConfigurationList.Add(result);

            //finalStepConfigurationList.Add(osHanaConnectionResult);
        }
        private void ShowHideStep(int stepNo, bool visibleType)
        {
            var pnlStep = pnlParent.Controls.OfType<Panel>().Where(i => (string)i.Tag == Convert.ToString(_currentStep)).FirstOrDefault();

            if (pnlStep != null)
            {
                pnlStep.Visible = visibleType;
                pnlStep.Location = new Point(150, 50);

                pnlStep.AutoScroll = false;

                // disable horizontal scrollbar
                pnlStep.HorizontalScroll.Enabled = false;

                pnlStep.Width = 540;
                pnlStep.Height = 420;
                pnlStep.AutoScroll = true;
                pnlStep.AutoScrollMinSize = new Size(540, 420);
            }

        }
        private void ButtonPreviewInCaseOfNext()
        {

            btnNext.Visible = true;
            btnFinish.Visible = false;
            btnBack.Visible = true;
        }
        private void ButtonPreviewInCaseOfBack()
        {

            btnBack.Visible = true;
            btnNext.Visible = true;
            btnFinish.Visible = false;
        }
        private void HideAllSteps()
        {

            foreach (Panel p in pnlParent.Controls.OfType<Panel>())
            {
                p.Visible = false;
            }

        }
        private void CheckPreInstallConfigurations()
        {
            pnlSystemCompatibility.Controls.Clear();

            //Label requirmentLabel = null;
            int txtBoxStartPosition = 10;
            int txtBoxStartPositionV = 10;
            IDictionary<bool, string> result;

            bool isValid;
            string text;

            resultList = new List<IDictionary<bool, string>>();

            result = _businessLogic.isValidIISVersion();

            isValid = result.Keys.FirstOrDefault();
            text = result[isValid];

            var iisVersionResult = new Dictionary<bool, string>();
            iisVersionResult.Add(isValid, text);
            resultList.Add(iisVersionResult);

            result = _businessLogic.isValidWindowsVersion();

            isValid = result.Keys.FirstOrDefault();
            text = result[isValid];


            var osVersionResult = new Dictionary<bool, string>();
            osVersionResult.Add(isValid, text);
            resultList.Add(osVersionResult);


            result = _businessLogic.isValidDotNetFrameWorkVersion();

            isValid = result.Keys.FirstOrDefault();
            text = result[isValid];


            var dotnetVersionResult = new Dictionary<bool, string>();
            dotnetVersionResult.Add(isValid, text);
            resultList.Add(dotnetVersionResult);


            //result = _businessLogic.isValidRewriteURLVersion();

            //isValid = result.Keys.FirstOrDefault();
            //text = result[isValid];


            //var urlVersionResult = new Dictionary<bool, string>();
            //urlVersionResult.Add(isValid, text);
            //resultList.Add(urlVersionResult);


            if (rbDatabaseTypeHana.Checked)
            {
                result = _businessLogic.CheckHanaClient();

                isValid = result.Keys.FirstOrDefault();
                text = result[isValid];


                var hanaClientResult = new Dictionary<bool, string>();
                hanaClientResult.Add(isValid, text);
                resultList.Add(hanaClientResult);
            }
            else
            {
                result = _businessLogic.CheckSQLServer();

                isValid = result.Keys.FirstOrDefault();
                text = result[isValid];


                var sqlServerResult = new Dictionary<bool, string>();
                sqlServerResult.Add(isValid, text);
                resultList.Add(sqlServerResult);
            }


            txtBoxStartPositionV += 30;

            foreach (var versionResult in resultList)
            {
                isValid = versionResult.Keys.FirstOrDefault();
                text = versionResult[isValid];

                Label newLabel = new Label();
                newLabel.Location = new System.Drawing.Point(txtBoxStartPosition, txtBoxStartPositionV);
                newLabel.Width = 300;

                RenderSystemRequirment(newLabel, text, isValid);

                pnlSystemCompatibility.Controls.Add(newLabel);
                txtBoxStartPositionV += 30;
            }



        }
        private void RenderSystemRequirment(Label lbl, string text, bool result)
        {

            if (result)
            {
                lbl.Text = string.Format("{0} {1}", "✓", text);
                lbl.ForeColor = Color.Green;
            }
            else
            {
                lbl.Text = string.Format("{0} {1}", "X", text);
                lbl.ForeColor = Color.Red;
            }
        }
        private async Task<bool> AddUDs()
        {
            var areUDsCreatedSuccessfully = false;

            // get list of all database that are checked in final step
            var includedDatabaseList = getIncludedDatabasesList();

            // filter sbo model list to get only selected databases
            var sboList = objInstaller.SBOModelList.Where(s => includedDatabaseList.Contains(s.database));

            // run installation/update only on selected databases
            foreach (var model in sboList)
            {
                var dbName = model.database;
                var totalDBs = objInstaller.SBOModelList.Count;

                Helper.LogMessage("-- Creating  User Defined Tables --" + dbName);
                UpdateProgress(" Creating  User Defined Tables " + dbName);

                var areUserDefinedTablesCreate = await _businessLogic.AddUserDefinedTables(model);
                Helper.LogMessage("-- Done Creating User Defined Tables --" + dbName);
                UpdateProgress(" Done Creating User Defined Tables " + dbName);

                DoProgress(15 / totalDBs);

                if (areUserDefinedTablesCreate)
                {
                    var dialogResultUDT = PromptSSHMessageORRestartServiceLayer();
                    if (dialogResultUDT == DialogResult.OK)
                    {
                        //do something

                        Helper.LogMessage("-- Creating  User Defined Fields -- " + dbName);
                        UpdateProgress(" Creating  User Defined Fields " + dbName);

                        var areUserDefinedFieldsCreated =  await _businessLogic.AddUserDefinedFields(model);

                        Helper.LogMessage("-- Done Creating User Defined Fields --" + dbName);
                        UpdateProgress(" Done Creating User Defined Fields " + dbName);

                        DoProgress(15 / totalDBs);

                        if (areUserDefinedFieldsCreated)
                        {
                            DialogResult dialogResultUDF = PromptSSHMessageORRestartServiceLayer();
                            if (dialogResultUDF == DialogResult.OK)
                            {
                                Helper.LogMessage("-- Creating  User Defined Objects -- " + dbName);
                                UpdateProgress(" Creating  User Defined Objects " + dbName);

                                var areUserDefinedbjectsCreate = await _businessLogic.AddUserDefinedObjects(model);

                                Helper.LogMessage("-- Done Creating  User Defined Objects -- " + dbName);
                                UpdateProgress(" Done Creating  User Defined Objects " + dbName);

                                DoProgress(10 / totalDBs);

                                if (areUserDefinedbjectsCreate)
                                {
                                    DialogResult dialogResultUDO = PromptSSHMessageORRestartServiceLayer();
                                    if (dialogResultUDF == DialogResult.OK)
                                    {
                                        Helper.LogMessage("-- Adding User Data --" + dbName);
                                        UpdateProgress(" Adding User Data " + dbName);

                                        var isUserDefinedDataAdded = await _businessLogic.AddUserDefinedData(model,_isUpdate);

                                        if (isUserDefinedDataAdded)
                                        {
                                            Helper.LogMessage("-- Done Adding User Data --" + dbName);
                                            UpdateProgress(" Done Adding User Data " + dbName);

                                            DoProgress(15 / totalDBs);

                                            Helper.LogMessage("-- UDs Created-- " + dbName);
                                            //lblProgressText.Text = "UDs Created";
                                            //lblProgressText.Refresh();
                                            UpdateProgress(" UDs Created " + dbName);

                                            areUDsCreatedSuccessfully = true;

                                           

                                        }
                                        else
                                        {
                                            areUDsCreatedSuccessfully = false;
                                            Helper.LogMessage(" Failure while adding user data. ");
                                            break;
                                        }


                                    }
                                }
                                else
                                {
                                    areUDsCreatedSuccessfully = false;
                                    Helper.LogMessage(" Failure while creating UDO. ");
                                    break;
                                }
                            }
                        }
                        else
                        {
                            areUDsCreatedSuccessfully = false;
                            Helper.LogMessage(" Failure while creating UDF. ");
                            break;
                        }
                    }
                    else
                    {
                        areUDsCreatedSuccessfully = false;
                        
                        break;
                    }
                }
                else
                {
                    areUDsCreatedSuccessfully = false;
                    Helper.LogMessage(" Failure while creating UDT. ");
                    break;
                }
            }

            return areUDsCreatedSuccessfully;
        }
        private string PrepareCORsString()
        {
            var serviceLayerIP = txtServiceLayerIP.Text.Replace("http://", "").Replace("https://", "");
            //var domainName = txtDomainName.Text.Replace("http://", "").Replace("https://", "");

            var backEndFonfigPath = Path.Combine(txtB1BackendFolderPath.Text, "web.config");

            var value = Helper.ReadConfigurationKey(backEndFonfigPath, "CORS");
            var domainAndPort = string.Format("{0}:{1}", txtDomainName.Text, txtPortNumber.Text);
            var finalString = value.Replace("192.168.97.17:5521", domainAndPort).Replace("192.168.97.17", domainAndPort).Replace("dev.b1-web.com", domainAndPort);

            return finalString;
        }
        private string GetDatabaseString()
        {
            var dbList = string.Empty;

            foreach (var row in objInstaller.SBOModelList)
            {
                var DatabaseName = Convert.ToString(row.database);
                dbList += DatabaseName + ",";
            }

            return dbList.TrimEnd(','); ;
        }
        private void DoProgress(int progressNumber)
        {

            pg.Value += progressNumber;
        }
        private void WriteProgress(string text)
        {
            if (this.lblProgressText.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(WriteProgress);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.lblProgressText.Text = text;
            }
        }
        private bool FindDatabaseInGrid(string searchValue)
        {
            int rowIndex = -1;
            foreach (DataGridViewRow row in gvDatabaseInformation.Rows)
            {
                if (row.Cells["Database"].Value.ToString().Equals(searchValue))
                {
                    rowIndex = row.Index;
                    return true;
                }
            }

            return false;
        }
        private void pnlStep8_Paint(object sender, PaintEventArgs e)
        {

        }
        private void ucDatabaseInformation2_Load(object sender, EventArgs e)
        {

        }
        private DialogResult PromptSSHMessageORRestartServiceLayer()
        {

            if (rbDatabaseTypeSQL.Checked)
            {
                var SBOJobServiceBackEnd = _businessLogic.RestartSAPServiceBackend();
                var isValid = SBOJobServiceBackEnd.Keys.FirstOrDefault();

                if (!isValid)
                {
                    DialogResult dialogResultUDT = MessageBox.Show(this, Constants.SystemMessages.RESTART_SBO_Job_Service_BackEnd, "Restart SBO Job Service BackEnd", MessageBoxButtons.OK);
                    return dialogResultUDT;
                }
                else
                {
                    return DialogResult.OK;
                }

            }
            else
            {
                var sshConnection = _businessLogic.CheckSSHConnection(txtServiceLayerIP.Text, 22, txtSSHRootUserName.Text, txtSSHRootPassword.Text);
                var isValid = sshConnection.State;

                if (!isValid)
                {
                    DialogResult dialogResultUDT = MessageBox.Show(this, Constants.SystemMessages.RESTART_SERVICE_LAYER, "Restart Service Layer", MessageBoxButtons.OK);
                    return dialogResultUDT;
                }
                else
                {
                    _businessLogic.RestartServiceLayer(txtServiceLayerIP.Text, 22, txtSSHRootUserName.Text, txtSSHRootPassword.Text);
                    return DialogResult.OK;
                }
            }
        }
        private void FillEdiableUserDetails()
        {

            lblName.Text = _Name;
            lblCompanyName.Text = _Company;
            lblEmail.Text = _Email;

            txtUserNameEditable.Text = _Name;
            txtCompanyNameEditable.Text = _Company;
            txtEmailEditable.Text = _Email;
        }
        private async Task ClearUDs()

        {
            foreach (DataGridViewRow row in gvDatabaseInformation.Rows)
            {
                var DatabaseName = Convert.ToString(row.Cells["Database"].Value);
                var SBOUserName = Convert.ToString(row.Cells["UserName"].Value);
                var SBOPassword = Convert.ToString(row.Cells["Password"].Value);

                SBOUserModel model = new SBOUserModel();
                //model.serviceLayerUrl = txtServiceLayerIP.Text;
                //model.serviceLayerPort = txtServiceLayerIPPort.Text;
                model.sboUserName = SBOUserName;
                model.sboPassword = SBOPassword;
                model.database = DatabaseName;

                var dialogResultUDT = PromptSSHMessageORRestartServiceLayer();

                if (dialogResultUDT == DialogResult.OK)
                {
                    try
                    {
                        var retval = await _businessLogic.ClearUDOS(model);
                        if (retval.ContainsValue(false))
                        {
                            Helper.LogMessage("Clearing UDOs failed," + DatabaseName + "-" + string.Join(";", retval.Where(x => !x.Value).Select(x => x.Key)));

                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.LogMessage("Clearing UDOs failed," + DatabaseName + "-" + ex.ToString());
                    }

                    try
                    {
                        var retval = await _businessLogic.ClearUDTS(model);
                        if (retval.ContainsValue(false))
                        {
                            Helper.LogMessage("Clearing UDTs failed," + DatabaseName + "-" + string.Join(";", retval.Where(x => !x.Value).Select(x => x.Key)));

                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.LogMessage("Clearing UDTs failed," + DatabaseName + "-" + ex.ToString());
                    }

                    try
                    {
                        var retval = await _businessLogic.ClearUDFS(model);
                        if (retval.ContainsValue(false))
                        {
                            Helper.LogMessage("Clearing UDFs failed," + DatabaseName + "-" + string.Join(";", retval.Where(x => !x.Value).Select(x => x.Key)));

                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.LogMessage("Clearing UDFs failed," + DatabaseName + "-" + ex.ToString());
                    }

                    // clear UDTs which were not removed before because of user fields
                    try
                    {
                        var retval = await _businessLogic.ClearUDTS(model);
                        if (retval.ContainsValue(false))
                        {
                            Helper.LogMessage("Clearing UDTs failed," + DatabaseName + "-" + string.Join(";", retval.Where(x => !x.Value).Select(x => x.Key)));

                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.LogMessage("Clearing UDTs failed," + DatabaseName + "-" + ex.ToString());
                    }
                }
                else
                {

                }
            }
        }
        private void SetTheme()
        {

            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            //materialSkinManager.ROBOTO_MEDIUM_10 = new Font("Microsoft Sans Serif", 5);
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);



            var allButtons = Helper.GetAllControls(this, typeof(Button)).ToList();
            foreach (var btn in allButtons)
            {
                btn.BackColor = materialSkinManager.ColorScheme.PrimaryColor;
                btn.ForeColor = Color.White;
            }

            var allTextBoxes = Helper.GetAllControls(this, typeof(MaterialSingleLineTextField)).ToList();
            foreach (var txt in allTextBoxes)
            {
                txt.Width = 400;
            }
        }
        private void FillIntallerModel()
        {
            var dbType = ddlDatabaseType.SelectedValue.ToString().Split('$')[0];

            objInstaller = new InstallerModel();
            objInstaller.SBOModelList = new List<SBOUserModel>();

            objInstaller.BackendPath = txtB1BackendFolderPath.Text.Trim();
            objInstaller.FrontEndPath = txtB1FrontendFolderPath.Text.Trim();
            objInstaller.InstallationNumber = txtInstallationNumber.Text.Trim();
            objInstaller.LicenseKey = txtB1License.Text.Trim();
            objInstaller.CustomerName = txtUserNameEditable.Text.Trim();
            objInstaller.CustomerCompany = txtCompanyNameEditable.Text.Trim();
            objInstaller.CustomerEmail = txtEmailEditable.Text.Trim();
            objInstaller.CustomerPhoneNo = txtPhoneNo.Text.Trim();
            objInstaller.UserManagementPassword = txtUserManagementPassword.Text.Trim();
            objInstaller.DatabaseType = dbType.Trim();
            objInstaller.ServiceLayerIP = txtServiceLayerIP.Text.Trim();
            objInstaller.HanaSQLUserName = txtHANAUser.Text.Trim();
            objInstaller.HanaSQLPassword = txtHANAUserPassword.Text.Trim();
            objInstaller.ServiceLayerPort = txtServiceLayerIPPort.Text.Trim();
            objInstaller.DatabaseEnginePort = txtDatabaseEnginePort.Text.Trim();
            objInstaller.LicenseServerPort = txtLicenseServerPort.Text;
            objInstaller.ServiceLayerUrl = string.Format("{0}:{1}{2}", objInstaller.ServiceLayerIP, objInstaller.ServiceLayerPort, "/b1s/v1/");
            objInstaller.XSEnginePort = txtXSEnginePort.Text.Trim();
            objInstaller.SSHUserName = txtSSHRootUserName.Text.Trim();
            objInstaller.SSHPassword = txtSSHRootPassword.Text.Trim();
            objInstaller.SSLPath = txtSSLCertificatePath.Text.Trim();
            objInstaller.SSLPassword = txtCertificatePassword.Text.Trim();
            objInstaller.SQLInstanceName = txtSQLServerInstanceName.Text.Trim();

            objInstaller.SiteInfo.SiteName = txtSiteName.Text.Trim();
            objInstaller.SiteInfo.Port = txtPortNumber.Text.Trim();
            objInstaller.SiteInfo.ApplicationPoolName = txtApplicationPool.Text.Trim();
            objInstaller.SiteInfo.DomainName = txtDomainName.Text.Trim();
            objInstaller.SiteInfo.IPAddress = txtIPAddress.Text.Trim();
            objInstaller.SiteInfo.ProtocolType = string.IsNullOrEmpty(ddlProtocolType.Text) ? "https": ddlProtocolType.Text;


            if (rbDatabaseTypeHana.Checked)
                objInstaller.InstallationType = Constants.InstallationType.HANA;
            else
                objInstaller.InstallationType = Constants.InstallationType.SQL;

            FillDatabaseList();
        }
        private void ShowInstallationFinalStep()
        {

            //SetLoading(true);
            pnlStep12.Controls.Clear();

            //List<ConnectionStatus> result;

            bool isValid;
            string text;

            wizardTab.SelectedTab = wizardTab.TabPages[10];


            int txtBoxStartPosition = 50;
            int txtBoxStartPositionV = 25;

            // if update load configuration from registry
            if (_isUpdate)
            {

                finalStepConfigurationList = new List<ConnectionStatus>();
               
                btnFinish.Text = " Run Update ";
                _currentStep = 11;

                // set constants
                _businessLogic.SetUrlConstants(objInstaller.ServiceLayerIP, objInstaller.ServiceLayerPort);


                if (objInstaller.InstallationType == Constants.InstallationType.HANA)
                {


                    /**** Check Hana Connection ****/
                    var hanaConnectionStatus = _businessLogic.CheckHanaStudioConnection(objInstaller.ServiceLayerIP, objInstaller.DatabaseEnginePort, objInstaller.HanaSQLUserName, objInstaller.HanaSQLPassword);

                    FillFinalStepConfigurationForDatabaseConnection(hanaConnectionStatus);

                    /**** Check Hana Connection ****/

                    /**** Check SSH Connection ****/
                    var sshConnectionStatus = _businessLogic.CheckSSHConnection(objInstaller.ServiceLayerIP, 22, objInstaller.SSHUserName, objInstaller.SSHPassword);

                    FillFinalStepConfigurationForSSHConnection(sshConnectionStatus);

                    /**** Check SSH Connection****/
                }

                else
                {
                    var result = _businessLogic.CheckSQLStudioConnection(objInstaller.SQLInstanceName, "master", objInstaller.HanaSQLUserName, objInstaller.HanaSQLPassword);

                    FillFinalStepConfigurationForDatabaseConnection(result);
                }


                FillControls();
                
            }


            /**** Check SBO Connections ****/
            foreach (var db in objInstaller.SBOModelList)
            {
                //this.Invoke((MethodInvoker)delegate
                //{
                    var sboConnection = _businessLogic.SendLoginRequest(db);
                    FillFinalStepConfigurationForSBO(sboConnection, db);
                //});
            }
            /**** Check SBO Connections ****/

            PopulateDatabasesToBeIncludedList();

            // set constants
            _businessLogic.SetUrlConstants(objInstaller.ServiceLayerIP, objInstaller.ServiceLayerPort);

            txtBoxStartPositionV += 30;

            foreach (var versionResult in finalStepConfigurationList)
            {
                isValid = versionResult.State;
                text = versionResult.Message;

                Label newLabel = new Label();
                newLabel.Location = new System.Drawing.Point(txtBoxStartPosition, txtBoxStartPositionV);
                newLabel.Width = 300;

                RenderSystemRequirment(newLabel, text, isValid);


                pnlStep12.Controls.Add(newLabel);
                txtBoxStartPositionV += 30;
            }

            var connectionList = finalStepConfigurationList.Where(i => i.State == false).ToList();

            if (connectionList != null && connectionList.Count > 0)
            {
                btnFinish.Visible = false;
            }
            else
            {
                btnFinish.Visible = true;
            }

            btnBack.Visible = true;
            btnNext.Visible = false;

            pnlStep12.Controls.Add(cbCopyRelease);
            pnlStep12.Controls.Add(cbUpdateDatabase);
            pnlStep12.Controls.Add(cbUpdateSP);
            pnlStep12.Controls.Add(cbUpdateIIS);

            cbCopyRelease.Enabled = _isUpdate;
            cbUpdateDatabase.Enabled = _isUpdate;
            cbUpdateSP.Enabled = _isUpdate;
            cbUpdateIIS.Enabled = _isUpdate;
            //SetLoading(false);
        }
        private void RemoveFilesAndFoldersInTempDirectory()
        {

            try
            {
                Helper.EmptyFolder(_zipFileFolder);
                Helper.RemoveFile(_zipFilePath);
                Helper.RemoveFile(userLicenseFilePathToTempFolder);
            }
            catch (Exception ex)
            {
                Helper.LogMessage("RemoveFilesAndFoldersInTempDirectory() " + ex.Message);
            }
        }
        private void FillControls()
        {

            if (objInstaller.InstallationType == Constants.InstallationType.HANA)
            {
                rbDatabaseTypeHana.Checked = true;
                ddlDatabaseType.SelectedItem = objInstaller.DatabaseType + "$";
            }
            else
            {
                rbDatabaseTypeSQL.Checked = true;
                ddlDatabaseType.SelectedValue = objInstaller.DatabaseType + "$" + objInstaller.SQLInstanceName;
            }

            txtB1BackendFolderPath.Text = objInstaller.BackendPath;
            txtB1FrontendFolderPath.Text = objInstaller.FrontEndPath;
            txtInstallationNumber.Text = objInstaller.InstallationNumber;
            txtB1License.Text = objInstaller.LicenseKey;
            txtUserNameEditable.Text = objInstaller.CustomerName;
            txtCompanyNameEditable.Text = objInstaller.CustomerCompany;
            txtEmailEditable.Text = objInstaller.CustomerEmail;
            txtPhoneNo.Text = objInstaller.CustomerPhoneNo;

            txtUserManagementPassword.Text = objInstaller.UserManagementPassword;
            txtConfirmUserManagementPassword.Text = objInstaller.UserManagementPassword;
            
            txtServiceLayerIP.Text = objInstaller.ServiceLayerIP;
            txtHANAUser.Text = objInstaller.HanaSQLUserName;
            txtHANAUserPassword.Text = objInstaller.HanaSQLPassword;
            txtServiceLayerIPPort.Text = objInstaller.ServiceLayerPort;
            txtDatabaseEnginePort.Text = objInstaller.DatabaseEnginePort;
            txtLicenseServerPort.Text = objInstaller.LicenseServerPort;
            txtXSEnginePort.Text = objInstaller.XSEnginePort;
            txtSSHRootUserName.Text = objInstaller.SSHUserName;
            txtSSHRootPassword.Text = objInstaller.SSHPassword;
            txtSQLServerInstanceName.Text = objInstaller.SQLInstanceName;
            txtSSLCertificatePath.Text = objInstaller.SSLPath;
            txtCertificatePassword.Text = objInstaller.SSLPassword;
            txtConfirmCertificatePassword.Text = objInstaller.SSLPassword;


            txtSiteName.Text = objInstaller.SiteInfo.SiteName;
            txtPortNumber.Text = objInstaller.SiteInfo.Port;
            txtApplicationPool.Text = objInstaller.SiteInfo.ApplicationPoolName;
            txtDomainName.Text = objInstaller.SiteInfo.DomainName;
            txtIPAddress.Text = objInstaller.SiteInfo.IPAddress;
            ddlProtocolType.SelectedItem = string.IsNullOrEmpty(objInstaller.SiteInfo.ProtocolType) ? "https" : objInstaller.SiteInfo.ProtocolType;


        }
        private void UpdateProgress(string text)
        {

            lblProgressText.Text = text;
            //lblProgressText.Refresh();
            lblProgressText.Invalidate();
            lblProgressText.Update();
            lblProgressText.Refresh();
            Application.DoEvents();
        }
        #endregion
        private void ddlDatabaseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDatabaseType.SelectedIndex > 0 && rbDatabaseTypeSQL.Checked)
            {
                var instanceName = ddlDatabaseType.SelectedValue.ToString().Split('$')[1];
                txtSQLServerInstanceName.Text = instanceName;
            }
            
        }
        private void rbDatabaseType_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                

                if (((RadioButton)sender) == rbDatabaseTypeHana)
                {
                    List<DatabaseTypeModel> DatabaseTypeModelList = 
                        new List<DatabaseTypeModel>() { 
                       new DatabaseTypeModel { serverInstanceName="", serverKey="dst_HANADB", serverName = "HANA" } };
                   
                    // Do stuff 
                    ddlDatabaseType.DataSource = DatabaseTypeModelList;
                    ddlDatabaseType.DisplayMember = "serverName";
                    ddlDatabaseType.ValueMember = "modelKey";

                    txtSQLServerInstanceName.Visible = false;
                    lblSQLServerInstanceName.Visible = false;
                }
                else if (((RadioButton)sender) == rbDatabaseTypeSQL)
                {
                    var instanceNameResult = _businessLogic.SQLVersion();
                   // var isValid = instanceNameResult.Keys.FirstOrDefault();

                    if (instanceNameResult != null)
                    {
                        instanceNameResult.Insert(0, new DatabaseTypeModel { serverName = Constants.SystemConstants.PLEASE_SELECT });
                        //var instanceList =  instanceNameResult[isValid];

                        // ddlSQLInstanceName.Items.AddRange(instanceList.ToArray());

                        ddlDatabaseType.DataSource = instanceNameResult;

                        ddlDatabaseType.DisplayMember = "serverName";
                        ddlDatabaseType.ValueMember = "modelKey";

                        txtSQLServerInstanceName.Visible = true;
                        lblSQLServerInstanceName.Visible = true;

                    }
                }

                CheckPreInstallConfigurations();

                var inValidConfiguration = resultList.Where(i => i.ContainsKey(false)).FirstOrDefault();

                if (inValidConfiguration != null && inValidConfiguration.Count > 0)
                {
                    btnNext.Visible = false;
                    lblSystemCompatibilityError.Visible = true;
                }
                else
                {

                    btnNext.Visible = true;
                    lblSystemCompatibilityError.Visible = false;
                }
            }
        }
        private void LoadInstallerInCaseOfUpdate()
        {
            if (_isUpdate)
            {
                try
                {
                    objInstaller = ReadInsatllerSettingsFromRegistry();

                    if (objInstaller != null)
                    {
                        // decrypt passwords
                        objInstaller.SBOModelList.ForEach(s => s.sboPassword = Helper.DecryptStringAES(s.sboPassword));

                        if (!string.IsNullOrEmpty(objInstaller.HanaSQLPassword))
                            objInstaller.HanaSQLPassword = Helper.DecryptStringAES(objInstaller.HanaSQLPassword);

                        if (!string.IsNullOrEmpty(objInstaller.SSHPassword))
                            objInstaller.SSHPassword = Helper.DecryptStringAES(objInstaller.SSHPassword);

                        if (!string.IsNullOrEmpty(objInstaller.UserManagementPassword))
                            objInstaller.UserManagementPassword = Helper.DecryptStringAES(objInstaller.UserManagementPassword);

                        ShowInstallationFinalStep();
                    }
                    else
                    {
                        MessageBox.Show(Constants.SystemMessages.CONFIGURATION_DOESNT_EXISTS);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void SetLoading(bool displayLoader)
        {
            if (displayLoader)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    picLoader.Visible = true;
                    this.Cursor = Cursors.WaitCursor;
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    picLoader.Visible = false;
                    this.Cursor = Cursors.Default;
                });
            }
        }

        private void PopulateDatabasesToBeIncludedList()
        {

            var rowList = gvDatabaseInformation.Rows
                     .Cast<DataGridViewRow>()
                     .Where(r => r.Cells["Include"].Value.Equals(true))
                     .Select(r => r.Cells["Database"].Value)
                     .ToList();

            if (rowList.Count > 0)
            {

                cblIncludedDatabases.DataSource = rowList;
                cblIncludedDatabases.DisplayMember = "database";
                cblIncludedDatabases.ValueMember = "localization";


                AddDatabaseToBeIncludedLabel();
                pnlStep12.Controls.Add(cblIncludedDatabases);

                for (int i = 0; i < cblIncludedDatabases.Items.Count; i++)
                {
                    cblIncludedDatabases.SetItemChecked(i, true);
                }
            }

        }

        private void AddDatabaseToBeIncludedLabel() {

            if (_isUpdate)
                lblDatabasesToBeUpdated.Text = "Following databases will be updated";
            else
                lblDatabasesToBeUpdated.Text = "Installation will be done on following databases";

            pnlStep12.Controls.Add(lblDatabasesToBeUpdated);
        }

        private List<string> getIncludedDatabasesList() {

            var list = new List<string>();

            foreach (var checkedItem in cblIncludedDatabases.CheckedItems)
            {
                var database = Convert.ToString(checkedItem);
                list.Add(database);
            }

            return list;
        }
        private void pnlStep1_Click(object sender, EventArgs e)
        {

        }

        private void cblIncludedDatabases_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var selectedDatabase = Convert.ToString(cblIncludedDatabases.Items[e.Index]);
            var checkOrUncheck = e.CurrentValue == CheckState.Checked ? false : true;
            CheckUncheckDatabaseInfoGrid(selectedDatabase, checkOrUncheck);
        }

        private void CheckUncheckDatabaseInfoGrid(string database,bool value) {

            var row  = gvDatabaseInformation.Rows.Cast<DataGridViewRow>().Where(r => r.Cells["Database"].Value.ToString().Equals(database)).FirstOrDefault();
            var rowIndex = row.Index;
            gvDatabaseInformation.Rows[rowIndex].Cells[0].Value = value;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void ucDatabaseInformation1_Load(object sender, EventArgs e)
        {

        }

        private void lblSQLServerInstanceName_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void pnlStep3_Click(object sender, EventArgs e)
        {

        }
    }
}