﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using B1WebInstaller.BusinessEntities;
using System.Net;
using Microsoft.Win32;
using System.Data;
using System.Dynamic;
using Renci.SshNet;
using System.Runtime.InteropServices;
//using Sap.Data.Hana;
using System.Reflection;
using MaterialSkin;
using MaterialSkin.Controls;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.ServiceProcess;
using System.Data.SqlClient;
using Microsoft.Web.Administration;
using System.Diagnostics.Contracts;
using System.Management;
using ObjectsComparer;
using System.Text;
using System.Collections;

namespace B1WebInstaller
{
    public class BusinessLogic
    {
        const int no_of_retries = 3;

        const int TOTAL_TRIES = 4;

        const int OS_ANYSERVER = 29;

        [DllImport("shlwapi.dll", SetLastError = true, EntryPoint = "#437")]
        private static extern bool IsOS(int os);

        //private HanaConnection hanaConnection;

        Type typeHanaConnection = null;
        Type typeHanaDataAdapter = null;
        Type typeHanaCommand = null;
        object objHanaConnection = null;
        object objHanaAdapter = null;

        public List<UDT> existingUDTList = null;
        public List<UDF> existingUDFList = null;
        public List<dynamic> existingUDOList = null;
        public List<dynamic> existingUDODataList = null;


        const string pathToDLL = @"C:\Program Files\SAP\hdbclient\ado.net\v4.5\Sap.Data.Hana.v4.5.dll";
        const string crytalReportRunTimePath = @"C:\Windows\assembly\GAC\CrystalDecisions.CrystalReports.Engine";
        const string crytalReportRunTimePath_64Bit = @"C:\Windows\assembly\GAC_MSIL\CrystalDecisions.CrystalReports.Engine";
        

        public BusinessLogic()
        {

            existingUDTList = new List<UDT>();
            existingUDFList = new List<UDF>();
            existingUDOList = new List<dynamic>();
            existingUDODataList = new List<dynamic>();

        }
        public string CheckRequiredField(Control control)
        {
            try
            {
                if (control is MaterialSingleLineTextField)
                {
                    var textField = (MaterialSingleLineTextField)control;

                    if (string.IsNullOrEmpty(textField.Text) && !string.IsNullOrEmpty(textField.Tag.ToString()))
                    {
                        return textField.Tag.ToString();
                    }
                }
                else if(control is ComboBox)
                {
                    var ddl = (ComboBox)control;

                    if (string.IsNullOrEmpty(ddl.Text) && !string.IsNullOrEmpty(ddl.Tag.ToString()) && (ddl.SelectedIndex == -1))
                    {
                        return ddl.Tag.ToString();
                    }
                }


            }
            catch (Exception ex)
            {
            }

            return string.Empty;
        }
        public bool CopyFilesToInstallerDirectory(string destination, string sourceFolder)
        {
            // copy directories
            try
            {
                var source = Path.Combine(sourceFolder);

                Helper.DirectoryCopy(source, destination, true);


                return true;
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" CopyFilesToInstallerDirectory() " + ex.Message);
                throw;
            }

        }


        public ConnectionStatus SendLoginRequest(SBOUserModel model)
        {
            ConnectionStatus connectionState = new ConnectionStatus();
            connectionState.Type = Constants.ConnectionType.DATABASE;

            //IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = string.Format(" Login Failed for  Database : {0}. ", model.database);

            Login login = new Login { CompanyDB = model.database, Password = model.sboPassword, UserName = model.sboUserName };

            var uri = Constants.SERVICEURL.LOGIN;

            var resultLogin = Interact(uri, "POST", JsonConvert.SerializeObject(login, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented }));

            if (resultLogin.ContainsKey(true))
            {
                var resultObject = JsonConvert.DeserializeObject<dynamic>(resultLogin[true]);
                var sessionId = Convert.ToString(resultObject.SessionId);
                connectionState.State = true;
                connectionState.Message = sessionId;
                //result.Add(true, sessionId);
                return connectionState;
            }

            connectionState.State = false;
            connectionState.Message = requiredVersionMessage;
            //result.Add(false, requiredVersionMessage);
            return connectionState;
        }

        public IDictionary<bool, string> isValidDotNetFrameWorkVersion()
        {

            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " (NET Framework 4.6.2 or later version is required) ";

            try
            {

                const string subkey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";

                using (var ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey))
                {
                    if (ndpKey != null && ndpKey.GetValue("Release") != null)
                    {
                        result = CheckFor45PlusVersion((int)ndpKey.GetValue("Release"));
                        //Console.WriteLine($".NET Framework Version: {}");
                    }
                    else
                    {
                        result.Add(false, requiredVersionMessage);
                    }
                }

                // Checking the version using >= enables forward compatibility.

            }
            catch
            {
                result.Add(false, string.Concat(" NET Framework is not installed.", requiredVersionMessage));

            }

            return result;
        }

        IDictionary<bool, string> CheckFor45PlusVersion(int releaseKey)
        {
            IDictionary<bool, string> result = new Dictionary<bool, string>();

            if (releaseKey >= 528040)
                result.Add(true, " NET Framework Version 4.8 or later");
            else if (releaseKey >= 461808)
                result.Add(true, " NET Framework Version 4.7.2");
            else if (releaseKey >= 461308)
                result.Add(true, " NET Framework Version 4.7.1");
            else if (releaseKey >= 460798)
                result.Add(true, " NET Framework Version 4.7");
            else if (releaseKey >= 394802)
                result.Add(true, " NET Framework Version 4.6.2");
            else if (releaseKey >= 394254)
                result.Add(false, " NET Framework Version 4.6.1");
            else if (releaseKey >= 393295)
                result.Add(false, " NET Framework Version 4.6");
            else if (releaseKey >= 379893)
                result.Add(false, " NET Framework Version 4.5.2");
            else if (releaseKey >= 378675)
                result.Add(false, " NET Framework Version 4.5.1");
            else if (releaseKey >= 378389)
                result.Add(false, " NET Framework Version 4.5");
            else
                result.Add(false, " NET Framework Version No 4.5 or later version detected");
            // This code should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return result;
        }

        public IDictionary<bool, string> isValidWindowsVersion()
        {
            var type = " Current Windows Version ";
            IDictionary<bool, string> result = new Dictionary<bool, string>();

            try
            {

                string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
                RegistryKey key = Registry.LocalMachine;
                RegistryKey skey = key.OpenSubKey(subKey);

                string name = skey.GetValue("ProductName").ToString();

                var osVersion = Environment.OSVersion.Version;
                var isWindows2012OeAbove = osVersion.Major > 6 || (osVersion.Major == 6 && osVersion.Minor >= 2);


                if ((IsWindowsServer() || name.Contains("Windows 10")) && isWindows2012OeAbove)
                {
                    result.Add(true, string.Concat(type, name));
                    return result;
                }
                else
                {
                    result.Add(false, string.Concat(type, name, " , Windows 10, Windows Server 2012 or Above is required. "));
                    return result;
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" isValidWindowsVersion() " + ex.Message);
                result.Add(false, string.Concat(type, " Windows 10, Windows Server 2012 or Above is required. "));
                return result;
            }
        }

        public static bool IsWindowsServer()
        {
            return IsOS(OS_ANYSERVER);
        }

        public IDictionary<bool, string> isValidIISVersion()
        {

            var type = " IIS Version ";
            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " (8.5 or later version is required) ";

            try
            {
                string _iisRegKey = @"Software\Microsoft\InetStp";
                using (RegistryKey iisKey = Registry.LocalMachine.OpenSubKey(_iisRegKey))
                {
                    var currentVersion = Convert.ToDouble(iisKey.GetValue("MajorVersion"));
                    if (currentVersion >= Constants.SystemConstants.REQUIRED_IIS_VERSION)
                    {
                        result.Add(true, string.Concat(type, currentVersion.ToString()));
                        return result;
                    }
                    result.Add(false, string.Concat(type, currentVersion.ToString(), requiredVersionMessage));
                    return result;
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" isValidIISVersion() " + ex.Message);
                result.Add(false, string.Concat(" IIS is not installed ", requiredVersionMessage));
                return result;
            }
        }
       
        public IDictionary<bool, string> CheckSQLServer()
        {

            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " SQL Server Installed ";

            try
            {
                Microsoft.Win32.RegistryKey key;

                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\MICROSOFT\Microsoft SQL Server");
                if (key != null)
                {
                    string[] instances = (string[])key.GetValue("InstalledInstances");

                    if (instances != null)
                    {
                        result.Add(true, requiredVersionMessage);
                    }
                    else
                    {
                        result.Add(false, Constants.SystemMessages.SQL_SERVER_NOT_FOUND);
                    }

                    return result;
                }
                else
                {
                    result.Add(false, Constants.SystemMessages.SQL_SERVER_NOT_FOUND);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(Constants.SystemMessages.SQL_SERVER_NOT_FOUND + " " + ex.Message);
                result.Add(false, Constants.SystemMessages.SQL_SERVER_NOT_FOUND);
                return result;
            }
        }

        public List<DatabaseTypeModel> SQLVersion()
        {

            try
            {
                List<DatabaseTypeModel> DatabaseTypeModelList = new List<DatabaseTypeModel>();

                Microsoft.Win32.RegistryKey key;

                //key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\MICROSOFT\Microsoft SQL Server");
                // find the installed SQL Server instance names
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL");

                if (key == null)
                    return null;

                // loop over those instances
                foreach (string sqlInstance in key.GetValueNames())
                {
                    Console.WriteLine("SQL Server instance: {0}", sqlInstance);

                    var fullInstanceName = Environment.MachineName + "\\" + sqlInstance;

                    // find the SQL Server internal name for the instance
                    string internalName = key.GetValue(sqlInstance).ToString();
                    Console.WriteLine("\tInternal instance name: {0}", internalName);

                    // using that internal name - find the "Setup" node in the registry
                    string instanceSetupNode = string.Format(@"SOFTWARE\Microsoft\Microsoft SQL Server\{0}\Setup", internalName);

                    RegistryKey setupKey = Registry.LocalMachine.OpenSubKey(instanceSetupNode, false);

                    if (setupKey != null)
                    {
                        // in the "Setup" node, you have several interesting items, like
                        // * edition and version of that instance
                        // * base path for the instance itself, and for the data for that instance
                        string edition = setupKey.GetValue("Edition").ToString();
                        string pathToInstance = setupKey.GetValue("SQLBinRoot").ToString();
                        string version = setupKey.GetValue("Version").ToString();

                        DatabaseTypeModel databaseTypeModel = SQLVersionYear(version);
                        databaseTypeModel.serverInstanceName = fullInstanceName;

                        DatabaseTypeModelList.Add(databaseTypeModel);
                    }
                }

                return DatabaseTypeModelList;
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" SQLVersion() " + ex.Message);
                return null;
            }
        }

        private DatabaseTypeModel SQLVersionYear(string version)
        {

            //dst_HANADB
            //dst_MSSQL2005
            //dst_MSSQL2008
            //dst_MSSQL2012
            //dst_MSSQL2014
            //dst_MSSQL2016
            //dst_MSSQL2017

            DatabaseTypeModel model = new DatabaseTypeModel();

            if (version.StartsWith("15."))
            {
                model.serverName = "SQL Server 2019";
                model.serverKey = "dst_MSSQL2019";
                return model;
            }
            else if (version.StartsWith("14."))
            {
                model.serverName = "SQL Server 2017";
                model.serverKey = "dst_MSSQL2017";
                return model;

            }
            else if (version.StartsWith("13."))
            {

                model.serverName = "SQL Server 2016";
                model.serverKey = "dst_MSSQL2016";
                return model;

            }
            else if (version.StartsWith("12."))
            {
                model.serverName = "SQL Server 2014";
                model.serverKey = "dst_MSSQL2014";
                return model;

            }
            else if (version.StartsWith("11."))
            {
                model.serverName = "SQL Server 2012";
                model.serverKey = "dst_MSSQL2012";
                return model;

            }
            else if (version.StartsWith("10."))
            {
                model.serverName = "SQL Server 2008";
                model.serverKey = "dst_MSSQL2008";
                return model;

            }
            else if (version.StartsWith("9."))
            {
                model.serverName = "SQL Server 2005";
                model.serverKey = "dst_MSSQL2005";
                return model;

            }
            else if (version.StartsWith("8."))
            {
                model.serverName = "SQL Server 2000";
                model.serverKey = "dst_MSSQL2000";
                return model;

            }


            return null;
        }
     
        public async Task<bool> AddUserDefinedTables(SBOUserModel model)
        {
            var uri = Constants.SERVICEURL.BATCH;//Constants.SERVICEURL.USER_DEFINED_TABLE; 
            var reqString = string.Empty;

            try
            {
                //UDs/UDT.json
                var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDT.json");
                //var result = Helper.ReadJSONFile(source);
                var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);

                var loginResult = SendLoginRequest(model);
                var isSuccess = loginResult.State;

                if (isSuccess)
                {
                    var sessionId = loginResult.Message;

                    var logMessage = string.Format("Total {0} User Defined Tables to be created for Database : {1}", userDefinedTableResult.Length, model.database);
                    Helper.LogMessage(logMessage);

                    existingUDTList = new List<UDT>();

                    GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_TABLE, Constants.MetaDataType.USER_DEFINED_TABLES);

                    reqString = "--batch_Installer_UserDefinedTables";
                    foreach (var udt in userDefinedTableResult)
                    {
                        var udtUri = string.Empty;
                       var obj = JsonConvert.DeserializeObject<UDT>(udt);

                        // get existing object
                        var existingObject = existingUDTList.Where(i => i.TableName == obj.TableName).FirstOrDefault();

                        if (existingObject == null)
                        {
                            udtUri = string.Format("{0} /b1s/v1/{1}", "POST", "UserTablesMD");
                            

                            reqString += PrepareBatchStatementRequest(udtUri,udt, "batch_Installer_UserDefinedTables");
                        }
                        else
                        {
                            var isObjectSimilar = existingObject == null ? false : IsTableObjectSame(existingObject, obj);

                            // do not post/update if both objects are similar
                            if (!isObjectSimilar)
                            {
                                var resultantObject = JsonConvert.DeserializeObject<UDT>(udt);
                                udtUri = string.Format("{0} /b1s/v1/{1}", "PATCH", "UserTablesMD");
                                udtUri += "('" + resultantObject.TableName + "')";

                               
                                reqString += PrepareBatchStatementRequest(udtUri, udt, "batch_Installer_UserDefinedTables");
                            }
                        }
                    }

                    reqString += "--batch_Installer_UserDefinedTables--";

                    if (reqString.Contains("POST") || reqString.Contains("PATCH"))
                    {
                       
                        var result = await Task.Run(() => Interact(uri, "POST", reqString, sessionId, null, no_of_retries, true, "multipart/mixed;boundary=batch_Installer_UserDefinedTables"));
                        //result = await Task.Run(() => Interact(uri, "POST", reqString, sessionId, null, no_of_retries, true, "multipart/mixed;boundary=batch_Installer_UserDefinedTables"));


                        if (result.ContainsKey(true))
                        {
                            var successMessage = result[true];
                            
                            Helper.LogMessage(successMessage);
                            return true;

                            //TryPostingUserDefinedTableAgain(model, udt, sessionId);
                        }
                        else
                        {
                            //var errorString = PrepareErrorString(result, "PostUserDefinedTable", udt, uri, model, tryNo);
                            var resultObject = result[false];

                            string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", "AddUserDefinedTables()", resultObject, reqString, uri, model.database, 1);

                            Helper.LogMessage(errorString);
                            
                            return false;
                        }
                    }

                    return true;
                        
                }
                else
                {
                    var errorString = string.Format("Method:{0} \n Error : {1} \n Entry {2} \n Uri:{3} \n Database:{4}", "AddUserDefinedTables", Constants.SystemMessages.LOGIN_FAILED,reqString ,uri, model.database);
                    Helper.LogMessage(errorString);
                    return false;
                }
            }
            catch (Exception ex)
            {
                var errorString = string.Format("Method:{0} \n Error : {1} \n  Entry {2} \n  Uri:{2} \n Database:{3}", "AddUserDefinedTables", ex.Message, reqString, uri, model.database);
                Helper.LogMessage(errorString);
                return false;
                //throw;
            }
        }

        private bool IsObjectSame(string existingObject, string newObject)
        {
            var comparer = new ObjectsComparer.Comparer<dynamic>();
            ////Compare objects
            //IEnumerable<Difference> differences;
            //var isEqual = comparer.Compare(existingObject, newObject, out differences);
            //return isEqual;

            //var settings0Json = LoadJson("Settings0.json");
            var settings0 = JsonConvert.DeserializeObject<ExpandoObject>(existingObject);
            //var settings2Json = LoadJson("Settings2.json");
            var settings2 = JsonConvert.DeserializeObject<ExpandoObject>(newObject);
            IEnumerable<Difference> differences;
            var isEqual = comparer.Compare(settings0, settings2, out differences);
            return isEqual;
        }

        private string PrepareErrorString(dynamic result,string functionName,string udt,string uri, SBOUserModel model, int tryNo)
        {
            var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);

            string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", functionName, resultObject.error, udt, uri, model.database, tryNo);

            return errorString;
        }

        public async Task<bool> AddUserDefinedFields(SBOUserModel model)
        {
            var uri = Constants.SERVICEURL.BATCH;//Constants.SERVICEURL.USER_DEFINED_FIELDS;
            var reqString = string.Empty;

            try
            {
                //UDs/UDF.json
                var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDF.json");

                var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);


                var logMessage = string.Format("Total {0} User Defined Fields to be created for Database : {1}", userDefinedTableResult.Length, model.database);
                Helper.LogMessage(logMessage);


                var loginResult = SendLoginRequest(model);
                var isSuccess = loginResult.State;

                if (isSuccess)
                {
                    var sessionId = loginResult.Message;

                    existingUDFList = new List<UDF>();

                    GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_FIELDS_GET, Constants.MetaDataType.USER_DEFINED_FIELDS);

                    //var userFieldsUrl = string.Format("{0}/{1}", Constants.SERVICEURL.USER_DEFINED_FIELDS,"?$filter = startswith(TableName, 'OUSR')");
                    // GetMetaData(sessionId, userFieldsUrl, Constants.MetaDataType.USER_DEFINED_FIELDS);


                    //foreach (var udf in userDefinedTableResult)
                    //{
                    //    try
                    //    {
                    //        var result = await PostUserDefinedField(model, udf, sessionId);

                    //        if (!result)
                    //        {
                    //            WaitBetweenRetry();
                    //            TryPostingUserDefinedFieldAgain(model, udf, sessionId);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}", " For loop AddUserDefinedFields", ex.Message, udf, uri, model.database);

                    //        Helper.LogMessage(errorString);
                    //        return false;
                    //        //throw;
                    //    }
                    //}

                    //return true;


                    // get existing object



                    // do not post/update if both objects are similar



                    var udtUri = string.Empty;
                    reqString = "--batch_Installer_UserDefinedFields";
                    foreach (var udt in userDefinedTableResult)
                    {
                        var obj = JsonConvert.DeserializeObject<UDF>(udt);

                        // get existing object
                        var existingObject = existingUDFList.Where(f => f.Name == obj.Name && f.TableName == obj.TableName).FirstOrDefault();

                        if (existingObject == null)
                        {
                            //if (isUpdate && obj.TableName == "OUSR")
                            //{
                            //    udtUri = string.Format("{0} /b1s/v1/{1}", "PATCH", "UserFieldsMD");
                            //    reqString += PrepareBatchStatementRequest(udtUri, udt);
                            //}
                            //else
                            //{
                            udtUri = string.Format("{0} /b1s/v1/{1}", "POST", "UserFieldsMD");
                            reqString += PrepareBatchStatementRequest(udtUri, udt, "batch_Installer_UserDefinedFields");
                            //}
                        }
                        else
                        {

                            obj.FieldID = existingObject.FieldID;
                            obj.EditSize = obj.EditSize > existingObject.EditSize ? obj.EditSize : existingObject.EditSize;
                            obj.Size = obj.Size > existingObject.Size ? obj.Size : existingObject.Size;

                            var isObjectSimilar = IsFieldObjectSame(existingObject, obj);

                            if (!isObjectSimilar)
                            {
                                //uri = string.Format("{0}(TableName='{1}',FieldID={2})", uri, obj.TableName, obj.FieldID);

                                var updateString = string.Format("(TableName='{0}',FieldID={1})", existingObject.TableName, existingObject.FieldID);

                                // do not post/update if both objects are similar

                                udtUri = string.Format("{0} /b1s/v1/{1}", "PATCH", "UserFieldsMD" + updateString);

                                var jsonWithoutFieldId = JsonConvert.SerializeObject(obj);
                                reqString += PrepareBatchStatementRequest(udtUri, jsonWithoutFieldId, "batch_Installer_UserDefinedFields");
                            }
                            //try
                            //{
                            //var result = await PostUserDefinedTable(model, udt, sessionId);



                            //if (!result)
                            //{
                            //    TryPostingUserDefinedTableAgain(model, udt, sessionId);
                            //}

                            //}
                            //catch (Exception ex)
                            //{
                            //    string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}", " For loop AddUserDefinedTables", ex.Message, udt, uri, model.database);

                            //    Helper.LogMessage(errorString);
                            //    return false;
                            //    //throw;
                            //}
                        }
                    }

                    reqString += "--batch_Installer_UserDefinedFields--";

                    if (reqString.Contains("POST") || reqString.Contains("PATCH"))
                    {

                        var result = await Task.Run(() => Interact(uri, "POST", reqString, sessionId, null, no_of_retries, true, "multipart/mixed;boundary=batch_Installer_UserDefinedFields"));

                        if (result.ContainsKey(true))
                        {
                            var successMessage = result[true];
                            Helper.LogMessage(successMessage);
                            return true;

                            //TryPostingUserDefinedTableAgain(model, udt, sessionId);
                        }
                        else
                        {
                            //var errorString = PrepareErrorString(result, "PostUserDefinedTable", udt, uri, model, tryNo);
                            var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);

                            string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", "AddUserDefinedFields()", resultObject.error, reqString, uri, model.database, 1);

                            Helper.LogMessage(errorString);

                            return false;
                        }
                    }

                    return true;
                }
                else
                {
                    string errorString = string.Format("Method:{0} \n Error : {1}  \n Entry {2} \n Uri: {3} \n Database:{4}", "AddUserDefinedFields", Constants.SystemMessages.LOGIN_FAILED, reqString, uri, model.database);
                    Helper.LogMessage(errorString);
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorString = string.Format("Method:{0} \n Error : {1} \n Entry {2} \n Uri: {3} \n Database:{4}", "AddUserDefinedFields", ex.Message, reqString, uri, model.database);

                Helper.LogMessage(errorString);
                return false;
                //throw;
            }

        }

        public async Task<bool> AddUserDefinedObjects(SBOUserModel model)
        {
            var uri = Constants.SERVICEURL.BATCH;//Constants.SERVICEURL.USER_DEFINED_OBJECTS;
            var reqString = string.Empty;

            try
            {
                //UDs/UDO.json
                var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDO.json");

                var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);

                var logMessage = string.Format("Total {0} User Defined Objects to be created for Database : {1}", userDefinedTableResult.Length, model.database);
                Helper.LogMessage(logMessage);



                var loginResult = SendLoginRequest(model);
                var isSuccess = loginResult.State;

                if (isSuccess)
                {
                    var sessionId = loginResult.Message;

                    existingUDOList = new List<dynamic>();
                    GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_OBJECTS_GET, Constants.MetaDataType.USER_DEFINED_OBJECTS);


                    var udtUri = string.Empty;
                    reqString = "--batch_Installer_UserDefinedObjects";
                    foreach (var uo in userDefinedTableResult)
                    {
                        var obj = JsonConvert.DeserializeObject<dynamic>(uo);


                        // get existing object
                        var existingObject = existingUDOList.Where(f => f.Code == obj.Code && f.TableName == obj.TableName).FirstOrDefault();
                        

                        if (existingObject == null)
                        {
                            udtUri = string.Format("{0} /b1s/v1/{1}", "POST", "UserObjectsMD");
                            reqString += PrepareBatchStatementRequest(udtUri, uo, "batch_Installer_UserDefinedObjects");
                        }
                        else
                        {
                            var fieldToBeUpdated = JsonConvert.SerializeObject(existingUDOList.Where(f => f.Code == obj.Code && f.TableName == obj.TableName).FirstOrDefault());
                            var isObjectSimilar = existingObject == null ? false : IsObjectSame(fieldToBeUpdated, uo);

                            // do not post/update if both objects are similar
                            if (!isObjectSimilar)
                            {
                                udtUri = string.Format("{0} /b1s/v1/{1}", "PATCH", "UserObjectsMD"  + "('" + obj.Code + "')");
                                reqString += PrepareBatchStatementRequest(udtUri, uo, "batch_Installer_UserDefinedObjects");
                            }
                           
                        }
                    }

                    reqString += "--batch_Installer_UserDefinedObjects--";

                    if (reqString.Contains("POST") || reqString.Contains("PATCH"))
                    {

                        var result = await Task.Run(() => Interact(uri, "POST", reqString, sessionId, null, no_of_retries, true, "multipart/mixed;boundary=batch_Installer_UserDefinedObjects"));

                        if (result.ContainsKey(true))
                        {
                            var successMessage = result[true];
                            Helper.LogMessage(successMessage);
                            return true;

                            //TryPostingUserDefinedTableAgain(model, udt, sessionId);
                        }
                        else
                        {
                            //var errorString = PrepareErrorString(result, "PostUserDefinedTable", udt, uri, model, tryNo);
                            var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);

                            string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", "AddUserDefinedFields()", resultObject.error, reqString, uri, model.database, 1);

                            Helper.LogMessage(errorString);

                            return false;
                        }
                    }

                    return true;
                    //foreach (var uo in userDefinedTableResult)
                    //{
                    //    try
                    //    {
                    //        var result = await PostUserDefinedObject(model, uo, sessionId);

                    //        if (!result)
                    //        {
                    //            TryPostingUserDefinedObjectAgain(model, uo, sessionId);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        //    throw;
                    //        var errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2}  \n ServiceUri : {3} \n Database:{4}", "For Loop AddUserDefinedObjects", ex.Message, uo, uri, model.database);
                    //        Helper.LogMessage(errorString);
                    //        return false;
                    //    }
                    //}

                    //return true;
                }
                else
                {
                    var errorString = string.Format("Method:{0} \n Error : {1} \n Entry {2} \n ServiceUri : {3} \n Database:{4}", " AddUserDefinedObjects", Constants.SystemMessages.LOGIN_FAILED,reqString, uri, model.database);
                    Helper.LogMessage(errorString);
                    return false;
                }
            }
            catch (Exception ex)
            {
                var errorString = string.Format("Method:{0} \n Error : {1} \n Entry {2} \n ServiceUri : {2} \n Database:{3}", " AddUserDefinedObjects", ex.Message,reqString, uri, model.database);
                Helper.LogMessage(errorString);
                return false;
                //throw;
            }
        }

        public async Task<bool> AddUserDefinedData(SBOUserModel model,bool isUpdate)
        {
            //UDs/UDO.json

            try
            {
                var sessionId = SendLoginRequest(model).Message;

                GetUDOData(sessionId, "O_IQ1_WUI_JSCODE");
                GetUDOData(sessionId, "O_IQ1_WUI_FUNCTIONS");
                GetUDOData(sessionId, "O_IQ1_WUI_PAGE");
                GetUDOData(sessionId, "O_IQ1_WUI_DATAGRID");
                //GetUDOData(sessionId, "O_IQ1_WUI_PB_CEL");
                GetUDOData(sessionId, "O_IQ1_WUI_SIDEBAR");
                GetUDOData(sessionId, "O_IQ1_WUI_TREE");
                GetUDOData(sessionId, "O_IQ1_WUI_DASHBOARD");
                GetUDOData(sessionId, "O_IQ1_WUI_DASHCARD");
                GetUDOData(sessionId, "O_IQ1_WUI_DASHCHART");
                GetUDOData(sessionId, "O_IQ1_WUI_VALIDATS");
                GetUDOData(sessionId, "O_IQ1_WUI_GRDLAYOUT");
                GetUDOData(sessionId, "O_IQ1_WUI_NOTTYPE");
                GetUDOData(sessionId, "O_IQ1_WUI_OBJECTS");
                GetUDOData(sessionId, "O_IQ1_B_SELECT");
                GetUDOData(sessionId, "U_U_IQ1_B_SAP_SEL");
                GetUDOData(sessionId, "U_IAG_DOCTYPES");


                await AddData("O_IQ1_WUI_JSCODE", model);
                await AddData("O_IQ1_WUI_FUNCTIONS", model);
                await AddData("O_IQ1_WUI_PAGE", model);
                await AddData("O_IQ1_WUI_DATAGRID", model);
                //await AddData("O_IQ1_WUI_PB_CEL", model);
                await AddData("O_IQ1_WUI_SIDEBAR", model);
                await AddData("O_IQ1_WUI_TREE", model);

                // add dashboard object if doesnt exists - Start
                var dashboardData = existingUDODataList.Where(i => i.Object == "O_IQ1_WUI_DASHBOARD").FirstOrDefault();

                if (!isUpdate || (object.ReferenceEquals(null, dashboardData)))
                {
                    await AddData("O_IQ1_WUI_DASHBOARD", model);
                }

                var b_SAP_SEL = existingUDODataList.Where(i => i.Object == "U_U_IQ1_B_SAP_SEL").FirstOrDefault();

                if (!isUpdate || (object.ReferenceEquals(null, b_SAP_SEL)))
                {
                    await AddData("U_U_IQ1_B_SAP_SEL", model);
                }

                var dOCTYPES = existingUDODataList.Where(i => i.Object == "U_IAG_DOCTYPES").FirstOrDefault();

                if (!isUpdate || (object.ReferenceEquals(null, b_SAP_SEL)))
                {
                    await AddData("U_IAG_DOCTYPES", model);
                }
                // add dashboard object if doesnt exists  - End

                await AddData("O_IQ1_WUI_DASHCARD", model);
                await AddData("O_IQ1_WUI_DASHCHART", model);
                await AddData("O_IQ1_WUI_VALIDATS", model);
                //await AddData("O_U_IQ1_B_B1W_BRND", model);
                //await AddData("O_U_IQ1_B_B1W_MDL", model);
                //await AddData("O_U_IQ1_B_DATENTYP", model);
                //await AddData("O_U_IQ1_B_SAP_SEL", model);
                await AddData("O_IQ1_WUI_GRDLAYOUT", model);
                await AddData("O_IQ1_WUI_NOTTYPE", model);
                //await AddData("O_IQ1_WUI_NOTIFY", model);
                await AddData("O_IQ1_WUI_OBJECTS", model);
                await AddData("O_IQ1_B_SELECT", model);
                //await AddData("O_IQ1_WUI_UPDATES", model);

                return true;
            }
            catch (Exception ex)
            {
                var errorString = string.Format("Method:{0} \n Error : {1} ", "AddUserDefinedData", ex.Message);

                Helper.LogMessage(errorString);
                return false;
                //throw;
            }
        }

        public async Task<bool> AddData(string udoName, SBOUserModel model)
        {

            var uri = Constants.SERVICEURL.BATCH;//string.Format("{0}{1}", Constants.SERVICEURL.SERVICE_LAYER, udoName);

            try
            {
                var source = LocalizationPath(udoName, model);

                var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);


                var loginResult = SendLoginRequest(model);
                var isSuccess = loginResult.State;

                if (isSuccess)
                {
                    var sessionId = loginResult.Message;


                    var udtUri = string.Empty;
                    var reqString = "--batch_Installer_AddData";
                    foreach (var uo in userDefinedTableResult)
                    {
                        var obj = JsonConvert.DeserializeObject<dynamic>(uo);

                        dynamic existingObject = null;//existingUDODataList.Where(f => f.Code == obj.Code && f.Object == obj.Object).FirstOrDefault();

                        if (string.IsNullOrEmpty(Convert.ToString(obj.Code)))
                        {
                            existingObject = existingUDODataList.Where(f => f.DocNum == obj.DocNum && f.Object == obj.Object).FirstOrDefault();
                        }
                        else
                        {
                            existingObject = existingUDODataList.Where(f => f.Code == obj.Code && f.Object == obj.Object).FirstOrDefault();
                        }


                        if (existingObject == null)
                        {
                            udtUri = string.Format("{0} /b1s/v1/{1}", "POST", udoName);
                            reqString += PrepareBatchStatementRequest(udtUri, uo, "batch_Installer_AddData");
                        }
                        else
                        {
                            var udoId = string.IsNullOrEmpty(Convert.ToString(existingObject.Code)) ? udoName + "(" + existingObject.DocNum + ")" : udoName + "('" + existingObject.Code + "')";
                            udtUri = string.Format("{0} /b1s/v1/{1}", "PATCH", udoId);
                            reqString += PrepareBatchStatementRequest(udtUri, uo, "batch_Installer_AddData");

                        }
                    }

                    reqString += "--batch_Installer_AddData--";

                    if (reqString.Contains("POST") || reqString.Contains("PATCH"))
                    {
                        var messsage = string.Format("Posting User Data: Name: {0}\n Data: {1}",udoName, reqString);
                        Helper.LogMessage(messsage);

                        var result = await Task.Run(() => Interact(uri, "POST", reqString, sessionId, null, no_of_retries, true, "multipart/mixed;boundary=batch_Installer_AddData"));

                        if (result.ContainsKey(true))
                        {
                            var successMessage = result[true];
                            Helper.LogMessage(successMessage);
                            return true;

                            //TryPostingUserDefinedTableAgain(model, udt, sessionId);
                        }
                        else
                        {
                            //var errorString = PrepareErrorString(result, "PostUserDefinedTable", udt, uri, model, tryNo);
                            var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);

                            string errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", "AddUserDefinedFields()", resultObject.error, reqString, uri, model.database, 1);

                            Helper.LogMessage(errorString);

                            return false;
                        }
                    }

                    return true;
                   
                }
                else
                {
                    var errorString = string.Format("Method:{0} \n Error : {1} \n ServiceUri : {2}", "AddData", Constants.SystemMessages.LOGIN_FAILED, uri);
                    Helper.LogMessage(errorString);
                    return false;
                }
            }
            catch (Exception ex)
            {
                var errorString = string.Format("Method:{0} \n Error : {1} \n ServiceUri : {2}", "AddData", ex.Message, uri);
                Helper.LogMessage(errorString);
                return false;
            }
        }

        private string LocalizationPath(string udoName, SBOUserModel model) {

            var source = string.Empty;

            if (model.localization == Constants.Localization.US)
            {
                source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDOs_data_US", udoName + ".json");
            }
            else if (model.localization == Constants.Localization.DE)
            {
                source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDOs_data_DE", udoName + ".json");
            }

            return source;
        }
        private async Task<bool> PostUserData(string uo, string udoName, SBOUserModel model, string sessionId, int tryNo = 1)
        {

            var uri = string.Format("{0}{1}", Constants.SERVICEURL.SERVICE_LAYER, udoName);

            try
            {

                var obj = JsonConvert.DeserializeObject<dynamic>(uo);
                var result = await Task.Run(() => Interact(uri, "POST", JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented }), sessionId));
                //var result = await Task.Run(() => Interact(uri, "POST", uo, sessionId));

                if (result.ContainsKey(true))
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);
                    return true;
                }
                else
                {
                    var errorString = PrepareErrorString(result, "PostUserData", uo, uri, model, tryNo);

                    if (errorString.Contains(Constants.SystemMessages.ENTRY_ALREADY_EXISTS))
                    {
                        // update if already exists
                        uri = uri + "('" + obj.Code + "')";
                        result = await Task.Run(() => Interact(uri, "PATCH", JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented }), sessionId));
                        if (result.ContainsKey(true))
                        {
                            string updateString = string.Format("Method:{0} \n Message : {1} \n Entry: {2} \n ServiceUri : {3} \n Database:{4}\n Try No {5}", "PostUserData", " Updated Successfully ", uo, uri, model.database, tryNo);

                            Helper.LogMessage(updateString);

                            return true;
                        }
                    }

                    if (tryNo == TOTAL_TRIES)
                    {
                        errorString = PrepareErrorString(result, "PostUserData", uo, uri, model, tryNo);
                        Helper.LogMessage(errorString);
                    }
                    return false;

                }
            }
            catch (Exception ex)
            {
                //throw;
                if (tryNo == TOTAL_TRIES)
                {
                    var errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2} \n ServiceUri : {3}\n Database:{4}\n Try No {5}", "PostUserData", ex.Message, uo, uri, model.database, tryNo);

                    Helper.LogMessage(errorString);
                }

                return false;
            }

        }

     
        public static Dictionary<bool, string> Interact(string url, string method, string body = "", string sessionId = "", List<KeyValuePair<string, string>> additionalHeaders = null, int retryCount = 0, bool B1SReplaceCollectionsOnPatch = false,string contentType = "application/json")
        {
            Thread.Sleep(1000);

            var result = new Dictionary<bool, string>();

            var httpRequest = (HttpWebRequest)WebRequest.Create(url);
            // improve speed by setting proxy to null, as it is not needed
            //httpRequest.Proxy = null;

            // default is 2, maybe a higher values increases speed
            //ServicePointManager.DefaultConnectionLimit = connectionLimit;

            httpRequest.Method = method.ToUpper();
            httpRequest.ContentType = contentType;
            httpRequest.ServicePoint.Expect100Continue = false;
            ServicePointManager.ServerCertificateValidationCallback += RemoteSSLTLSCertificateValidate;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                     | SecurityProtocolType.Tls11
                                     | SecurityProtocolType.Tls12;

            if (!string.IsNullOrEmpty(sessionId))
            {
                if (httpRequest.CookieContainer == null)
                {
                    httpRequest.CookieContainer = new CookieContainer();
                }
                httpRequest.CookieContainer.Add(new Uri(url), new Cookie("B1SESSION", sessionId));
            }

            // Default Case-insensitive Suche: B1S-CaseInsensitive: true (finde haus und Haus)
            httpRequest.Headers.Add("B1S-CaseInsensitive", "true");

            if(B1SReplaceCollectionsOnPatch)
                httpRequest.Headers.Add("B1S-ReplaceCollectionsOnPatch", "true");

            httpRequest.Headers.Add("Prefer", "odata.continue-on-error");

            httpRequest.KeepAlive = false;

            if (additionalHeaders != null)
            {
                foreach (var item in additionalHeaders)
                {
                    httpRequest.Headers.Add(item.Key, item.Value);
                }
            }

            /*foreach (Cookie cookie in cookieData)
            {
                httpRequest.CookieContainer.Add(new Uri(url), new Cookie(cookie.Name, cookie.Value));
            }*/

            if (!string.IsNullOrEmpty(body))
            {
                using (var requestStream = httpRequest.GetRequestStream())
                {
                    var writer = new StreamWriter(requestStream);
                    writer.Write(body);
                    writer.Close();
                }
            }

            try
            {
                var webResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (var response = new StreamReader(webResponse.GetResponseStream()))
                {
                    result.Add(true, response.ReadToEnd());
                }
            }
            catch (WebException e)
            {
                var message = e.Message;
                if (e.Response != null)
                {
                    using (var reader = new StreamReader(e.Response.GetResponseStream()))
                    {
                        var content = reader.ReadToEnd();

                        if ((content.Contains("502 Proxy Error") || content.Contains("SSL/TLS")) && retryCount < 5)
                        {
                            return Interact(url, method, body, sessionId, additionalHeaders, ++retryCount);
                        }
                        else
                        {
                            result.Add(false, content);
                            //log.Error($"ServiceLayer: {content}");
                        }
                    }
                }
                else if (message.Contains("SSL/TLS") && retryCount < 5)
                {
                    return Interact(url, method, body, sessionId, additionalHeaders, ++retryCount);
                }
                else
                {
                    result.Add(false, message);
                    // log.Error($"ServiceLayer: {message}");
                }
            }
            catch (Exception ex)
            {
                result.Add(false, ex.ToString());
                //log.Error($"ServiceLayer: {ex.ToString()}");
            }

            return result;
        }

        private static bool RemoteSSLTLSCertificateValidate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors ssl)
        {
            // accept
            return true;
        }

        public string GetLicenseKey(string Name, string Company, string Email, string InstallationNumber)
        {

            try
            {
                var ValidTo = DateTime.Now.AddDays(30).ToString("yyyyMMdd");
                var uri = Constants.SystemConstants.LICENSE_API_ENDPOINT;
                APIKeyModel model = new APIKeyModel() { AnzUser = "5", APIKEY = Constants.SystemConstants.LICENSE_API_ENDPOINT_API_KEY, Company = Company, Email = Email, InstallationNumber = InstallationNumber, Name = Name, ValidTo = "ValidTo" };

                var result = Interact(uri, "POST", JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented }), "");

                if (result.ContainsKey(true))
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);
                    return ((string)resultObject["Key"]);

                }
                else
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);

                    var errorString = string.Format("Method:{0} \n Error : {1} \n Entry: {2}", "GetLicenseKey", resultObject.error, model);

                    Helper.LogMessage(errorString);
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                //    throw;
                var errorString = string.Format("Method:{0} \n Error : {1}", "GetLicenseKey", ex.Message);
                Helper.LogMessage(errorString);
                return string.Empty;
            }


        }

        public void RestartServiceLayer(string hostName, int port, string userName, string password)
        {

            try
            {
                hostName = hostName.Replace("http://", "").Replace("https://", "");

                Helper.LogMessage("Restarting  Service Layer");

                SshClient sshClient;
                KeyboardInteractiveAuthenticationMethod kauth = new KeyboardInteractiveAuthenticationMethod(userName);
                PasswordAuthenticationMethod pauth = new PasswordAuthenticationMethod(userName, password);

                kauth.AuthenticationPrompt += (sender, e) => HandleKeyEvent(sender, e, password);

                ConnectionInfo connectionInfo = new ConnectionInfo(hostName, port, userName, pauth, kauth);

                sshClient = new SshClient(connectionInfo);
                sshClient.KeepAliveInterval = TimeSpan.FromSeconds(60);

                Helper.LogMessage("Before Connection");

                sshClient.Connect();

                Helper.LogMessage("Connected");


                Helper.LogMessage("Stopping Service");

                sshClient.RunCommand("/etc/init.d/b1s stop");

                Thread.Sleep(1000);

                Helper.LogMessage("Service Stopped");

                Helper.LogMessage("Starting Service");

                sshClient.RunCommand("/etc/init.d/b1s start");

                Thread.Sleep(1000);

                Helper.LogMessage("Service Started");
            }
            catch (Exception ex)
            {
                Helper.LogMessage("Error in Restarting Service:  Error: " + ex.Message);
            }
        }

        public ConnectionStatus CheckSSHConnection(string hostName, int port, string userName, string password)
        {
            ConnectionStatus connectionState = new ConnectionStatus();
            connectionState.Type = Constants.ConnectionType.SSH;
            //IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " SSH Connection Failed. ";

            try
            {
                hostName = hostName.Replace("http://", "").Replace("https://", "");

                SshClient sshClient;
                KeyboardInteractiveAuthenticationMethod kauth = new KeyboardInteractiveAuthenticationMethod(userName);
                PasswordAuthenticationMethod pauth = new PasswordAuthenticationMethod(userName, password);

                kauth.AuthenticationPrompt += (sender, e) => HandleKeyEvent(sender, e, password);

                ConnectionInfo connectionInfo = new ConnectionInfo(hostName, port, userName, pauth, kauth);

                sshClient = new SshClient(connectionInfo);
                sshClient.KeepAliveInterval = TimeSpan.FromSeconds(60);

                Helper.LogMessage("Before Connection");


                sshClient.Connect();


                Helper.LogMessage("Connected");

                connectionState.State = true;
                connectionState.Message = " SSH Connection Successful. ";
                //result.Add(true, " SSH Connection Successful. ");
                return connectionState;

            }
            catch (Exception ex)
            {
                Helper.LogMessage("Error in SSH connection with " + hostName + ":  Error: " + ex.Message);
                //result.Add(false, requiredVersionMessage);
                //return result;
                connectionState.State = false;
                connectionState.Message = requiredVersionMessage;
                //result.Add(true, " SSH Connection Successful. ");
                return connectionState;
            }
        }
        void HandleKeyEvent(Object sender, Renci.SshNet.Common.AuthenticationPromptEventArgs e, string password)
        {
            foreach (Renci.SshNet.Common.AuthenticationPrompt prompt in e.Prompts)
            {
                if (prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    prompt.Response = password;
                }
            }
        }

        public void WriteMetaData(string sessionId, string url, string type = "", string filePath = "", string udoType = "")
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            var result = Interact(url, "GET", "", sessionId);
            if (result.ContainsKey(true))
            {
                var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);

                dynamic responseObject = new ExpandoObject();
                responseObject.Result = resultObject.value;
                responseObject.NextPage = ((string)resultObject["odata.nextLink"]);


                if (type == Constants.MetaDataType.USER_DEFINED_TABLES)
                {
                    UDTMetaData udtMetaData = JsonConvert.DeserializeObject<UDTMetaData>(JsonConvert.SerializeObject(responseObject));

                    if (udtMetaData.Result != null && udtMetaData.Result.Count > 0)
                    {
                        foreach (var udf in udtMetaData.Result)
                        {
                            var res = JsonConvert.SerializeObject(udf);

                            using (StreamWriter w = File.AppendText(filePath))
                            {
                                w.Write(res);
                                w.Write("$%&");
                            }
                        }
                    }
                }

                if (type == Constants.MetaDataType.USER_DEFINED_FIELDS)
                {
                    UDFMetaData udfMetaData = JsonConvert.DeserializeObject<UDFMetaData>(JsonConvert.SerializeObject(responseObject));

                    if (udfMetaData.Result != null && udfMetaData.Result.Count > 0)
                    {
                        foreach (var udf in udfMetaData.Result)
                        {
                            var res = JsonConvert.SerializeObject(udf);

                            using (StreamWriter w = File.AppendText(filePath))
                            {
                                w.Write(res);
                                w.Write("$%&");
                            }
                        }
                    }
                }

                else if (type == Constants.MetaDataType.USER_DEFINED_OBJECTS)
                {
                    UDOMetaData udoMetaData = JsonConvert.DeserializeObject<UDOMetaData>(JsonConvert.SerializeObject(responseObject));

                    if (udoMetaData.Result != null && udoMetaData.Result.Count > 0)
                    {
                        foreach (var udf in udoMetaData.Result)
                        {
                            var res = JsonConvert.SerializeObject(udf);

                            using (StreamWriter w = File.AppendText(filePath))
                            {
                                w.Write(res);
                                w.Write("$%&");
                            }
                        }
                    }
                }

                string nextPageUrl = responseObject.NextPage;
                if (!string.IsNullOrEmpty(nextPageUrl))
                {
                    var uri = string.Format("{0}?{1}", url.Split('?')[0], nextPageUrl.Split('?')[1]);
                    WriteMetaData(sessionId, url, type, filePath, udoType);
                }
                else
                {
                    FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
                    fs.SetLength(fs.Length - 3);
                    fs.Close();
                }

            }
        }
        public void WriteUDOData(string sessionId, string url, string udoType = "", string filePath = "")
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                //var filePath = string.Format(@"C:/{0}.Json", udoType); // string.Empty;

                var result = Interact(url, "GET", "", sessionId);
                if (result.ContainsKey(true))
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);

                    dynamic responseObject = new ExpandoObject();
                    responseObject.Result = resultObject.value;
                    responseObject.NextPage = ((string)resultObject["odata.nextLink"]);

                    //var t =JsonConvert.SerializeObject(responseObject.Result);
                    var udfMetaData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(responseObject));
                    string nextPageUrl = responseObject.NextPage;

                    //var last = udfMetaData.Result.Count;

                    foreach (var udf in udfMetaData.Result)
                    {
                        var res = JsonConvert.SerializeObject(udf);

                        using (StreamWriter w = File.AppendText(filePath))
                        {
                            w.Write(res);
                            w.Write("$%&");
                        }
                    }

                    if (!string.IsNullOrEmpty(nextPageUrl))
                    {
                        var uri = string.Format("{0}{1}?{2}", Constants.SERVICEURL.SERVICE_LAYER, udoType, nextPageUrl.Split('?')[1]);
                        WriteUDOData(sessionId, uri, udoType, filePath);
                    }
                    else
                    {
                        FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
                        fs.SetLength(fs.Length - 3);
                        fs.Close();
                    }

                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(ex.Message);
            }
        }
        public void GetMetaData(string sessionId, string url, string type = "", string udoType = "")
        {
            try
            {
                var result = Interact(url, "GET", "", sessionId);

                if (result != null && result.ContainsKey(true))
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);

                    dynamic responseObject = new ExpandoObject();
                    responseObject.Result = resultObject.value;
                    responseObject.NextPage = ((string)resultObject["odata.nextLink"]);

                    //var t =JsonConvert.SerializeObject(responseObject.Result);

                    if (type == Constants.MetaDataType.USER_DEFINED_TABLES)
                    {
                        UDTMetaData udtMetaData = JsonConvert.DeserializeObject<UDTMetaData>(JsonConvert.SerializeObject(responseObject));

                        if (udtMetaData.Result != null && udtMetaData.Result.Count > 0)
                        {
                            existingUDTList.AddRange(udtMetaData.Result);
                        }
                    }

                    if (type == Constants.MetaDataType.USER_DEFINED_FIELDS)
                    {
                        UDFMetaData udfMetaData = JsonConvert.DeserializeObject<UDFMetaData>(JsonConvert.SerializeObject(responseObject));

                        if (udfMetaData.Result != null && udfMetaData.Result.Count > 0)
                        {
                            foreach (var udfItem in udfMetaData.Result)
                            {
                                var isItemExisting = existingUDFList.Where(i => i.Name == udfItem.Name && i.TableName == udfItem.TableName).FirstOrDefault();

                                if (isItemExisting == null)
                                {
                                    existingUDFList.Add(udfItem);
                                }
                               
                            }
                        }
                    }

                    else if (type == Constants.MetaDataType.USER_DEFINED_OBJECTS)
                    {
                        UDOMetaData udoMetaData = JsonConvert.DeserializeObject<UDOMetaData>(JsonConvert.SerializeObject(responseObject));

                        if (udoMetaData.Result != null && udoMetaData.Result.Count > 0)
                        {
                            existingUDOList.AddRange(udoMetaData.Result);
                        }
                    }

                    string nextPageUrl = responseObject.NextPage;
                    if (!string.IsNullOrEmpty(nextPageUrl))
                    {
                        var uri = string.Format("{0}?{1}", url.Split('?')[0], nextPageUrl.Split('?')[1]);
                        GetMetaData(sessionId, uri, type);
                    }
                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(ex.Message);
            }
        }

        public void GetUDOData(string sessionId, string udoName,string paging = "")
        {
            //var url = string.Format("{0}{1}", Constants.SERVICEURL.SERVICE_LAYER, udoName);

            try
            {
                var result = Interact(string.Format("{0}{1}{2}", Constants.SERVICEURL.SERVICE_LAYER, udoName, paging), "GET", "", sessionId);
                if (result.ContainsKey(true))
                {
                    var resultObject = JsonConvert.DeserializeObject<dynamic>(result[true]);

                    dynamic responseObject = new ExpandoObject();
                    responseObject.Result = resultObject.value;
                    responseObject.NextPage = ((string)resultObject["odata.nextLink"]);

                    //var t =JsonConvert.SerializeObject(responseObject.Result);
                    var udfMetaData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(responseObject));
                    string nextPageUrl = responseObject.NextPage;

                    //var last = udfMetaData.Result.Count;
                  
                    foreach (var udf in udfMetaData.Result)
                    {
                        //var res = JsonConvert.SerializeObject(udf);
                        existingUDODataList.Add(udf);
                       
                    }

                    if (!string.IsNullOrEmpty(nextPageUrl))
                    {
                        //var uri = string.Format("{0}?{1}",udoName, nextPageUrl.Split('?')[1]);
                        paging = string.Format("?{0}", nextPageUrl.Split('?')[1]);
                        GetUDOData(sessionId,udoName, paging);
                    }

                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(ex.Message);
            }
        }

        public ConnectionStatus CheckHanaStudioConnection(string serviceLayerIP, string databaseEnginePort, string hanaUserName, string hanaPassword,bool isMultiContainer=false,string tenentDB = "")
        {
            //IDictionary<bool, string> result = new Dictionary<bool, string>();

            ConnectionStatus connectionState = new ConnectionStatus();
            connectionState.Type = Constants.ConnectionType.HANA;

            var requiredVersionMessage = " Hana Connection Failed. ";

            try
            {
                var serverUrl = "Server=" + string.Format("{0}:{1}", serviceLayerIP.Replace("http://", "").Replace("https://", ""), databaseEnginePort);
                var UserName = "UserName=" + hanaUserName;
                var Password = "Password=" + hanaPassword;

                //var connectionString = string.Format("{0};{1};{2}", serverUrl, UserName, Password);


                var connectionString = string.Empty;

                if (isMultiContainer)
                {
                    var url = string.Format("{0}:{1}", serviceLayerIP.Replace("http://", "").Replace("https://", ""), databaseEnginePort);
                    connectionString = string.Format("driver = HDBODBC; UID = {0}; PWD = {1}; serverNode = {2}; databaseName = {3}", hanaUserName, hanaPassword, url, tenentDB);
                }
                else
                {
                    //var serverUrl = "Server=" + string.Format("{0}:{1}", serviceLayerIP.Replace("http://", "").Replace("https://", ""), databaseEnginePort);
                    //var UserName = "UserName=" + hanaUserName;
                    //var Password = "Password=" + hanaPassword;

                    connectionString = string.Format("{0};{1};{2}", serverUrl, UserName, Password);
                    //connectionString = "Server=" + serviceLayerIP.Replace("http://", "").Replace("https://", "") + ":" + databaseEnginePort + "; Database=" + "SBODEMOUS" + ";" + "UID=" + hanaUserName + ";PWD=" + hanaPassword;
                }
                //databaseName=TDB1
                //var query = string.Format("select *  from \"SBODEMOUS\".\"OCRD\" ",);
                //driver={HDBODBC};UID=B1ADMIN;PWD=SAPB1Admin;serverNode=192.168.97.7:30013;databaseName=DB1
                Assembly hanaAssembly = Assembly.LoadFile(pathToDLL);

                typeHanaConnection = hanaAssembly.GetType("Sap.Data.Hana.HanaConnection");
                typeHanaDataAdapter = hanaAssembly.GetType("Sap.Data.Hana.HanaDataAdapter");
                typeHanaCommand = hanaAssembly.GetType("Sap.Data.Hana.HanaCommand");

                objHanaConnection = Activator.CreateInstance(typeHanaConnection);
               

                PropertyInfo propConnectionString = typeHanaConnection.GetProperty("ConnectionString");

                propConnectionString.SetValue(objHanaConnection, connectionString);

                var methodOpen = typeHanaConnection.GetMethod("Open");
                var methodClose = typeHanaConnection.GetMethod("Close");
                var methodChanageDatabase = typeHanaConnection.GetMethod("ChangeDatabase");


                if (methodOpen == null)
                {
                    // never throw generic Exception - replace this with some other exception type
                    throw new Exception("No such method exists.");
                }
                else
                {
                    methodOpen.Invoke(objHanaConnection, null);
                    //methodChanageDatabase.Invoke(objHanaConnection, new object[] { "NDB" });

                    var state = objHanaConnection.GetType().GetProperty("State").GetValue(objHanaConnection, null);

                    //DataTable dtHanaTables = new DataTable();

                    //var methodFill = typeHanaDataAdapter.GetMethod("Fill", new Type[] { typeof(DataTable) });
                    //methodFill.Invoke(objHanaAdapter, new object[] { dtHanaTables });

                    if (Convert.ToString(state) == "Open")
                    {
                        methodClose.Invoke(objHanaConnection, null);
                        //result.Add(true, " Hana Connection Successful. ");
                        connectionState.State = true;
                        connectionState.Message = " Hana Connection Successful. ";
                        return connectionState;
                    }


                }

                //result.Add(false, requiredVersionMessage);
                connectionState.State = false;
                connectionState.Message = requiredVersionMessage;
                return connectionState;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.SystemMessages.HANA_CONNECTION_FAILED);
                Helper.LogMessage(Constants.SystemMessages.HANA_CONNECTION_FAILED);
                connectionState.State = false;
                connectionState.Message = requiredVersionMessage;
                return connectionState;
            }

        }

        public ConnectionStatus CheckSQLStudioConnection(string sqlServerInstanceName, string database, string sqlUserName, string sqlPassword)
        {
            ConnectionStatus connectionState = new ConnectionStatus();
            connectionState.Type = Constants.ConnectionType.SQL;
            var requiredVersionMessage = " SQL Connection Failed. ";

            try
            {
                var connetionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", sqlServerInstanceName, database, sqlUserName, sqlPassword);

                using (SqlConnection connection = new SqlConnection(connetionString))
                {
                    connection.Open();

                    connectionState.State = true;
                    connectionState.Message = " SQL Connection Successful. ";

                    connection.Close();

                    return connectionState;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.SystemMessages.SQL_CONNECTION_FAILED);
                Helper.LogMessage(Constants.SystemMessages.SQL_CONNECTION_FAILED);

                connectionState.State = false;
                connectionState.Message = requiredVersionMessage;
                return connectionState;
            }

        }

        public bool CreateStoredProcedureForHana(object database)
        {

            string sql = string.Empty;

            try
            {

                var DatabaseName = Convert.ToString(database);

                Helper.LogMessage("-- Creating SPs for HANA --");

                string dropsql = "DROP PROCEDURE \"" + DatabaseName + "\"." + "\"IQ1KEYRESOLVER\"";
                sql = string.Format(Constants.StoredProcedures.KEYRESOLVER_HANA, DatabaseName);

                Helper.LogMessage(string.Format("sql string for SP : {0}", sql));

                PropertyInfo propConnectionString = typeHanaConnection.GetProperty("State");

                var state = (ConnectionState)propConnectionString.GetValue(objHanaConnection);
                if (state == ConnectionState.Closed || state == ConnectionState.Broken)
                {
                    //hanaConnection.Open();
                    var methodOpen = typeHanaConnection.GetMethod("Open");
                    methodOpen.Invoke(objHanaConnection, null);
                }

                var objDropHanaCommand = Activator.CreateInstance(typeHanaCommand, new object[] { dropsql, objHanaConnection });
                var objHanaCommandCreate = Activator.CreateInstance(typeHanaCommand, new object[] { sql, objHanaConnection });
                //var dropcmd = new HanaCommand(dropsql, hanaConnection);
                //var cmd = new HanaCommand(sql, hanaConnection);

                try
                {
                    var methodExecuteNonQueryDrop = typeHanaCommand.GetMethod("ExecuteNonQuery");
                    methodExecuteNonQueryDrop.Invoke(objDropHanaCommand, null);
                    Helper.LogMessage(" Removing SP if already exists. ");
                    //dropcmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Helper.LogMessage(ex.Message + " \n sql string for dropping SP : " + dropsql);
                    //return false;
                }

                var methodExecuteNonQueryCreate = typeHanaCommand.GetMethod("ExecuteNonQuery");
                methodExecuteNonQueryCreate.Invoke(objHanaCommandCreate, null);
                Helper.LogMessage(" SP Created Successfully. ");
                // int retval = cmd.ExecuteNonQuery();

                return true;

            }
            catch (Exception ex)
            {
                Helper.LogMessage(Constants.SystemMessages.KEYRESOLVER_SP_HANA_FAILED + " \n" + ex.Message + " \n sql string for SP : " + sql);
                return false;
            }
        }
        public bool CreateStoredProcedureForSQL(string sqlServerInstanceName, string database, string sqlUserName, string sqlPassword)
        {
            try
            {
                var connetionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", sqlServerInstanceName, database, sqlUserName, sqlPassword);
               

                using (SqlConnection connection = new SqlConnection(connetionString))
                {
                    connection.Open();

                    //SqlCommand commandDelete = new SqlCommand(Constants.StoredProcedures.KEYRESOLVER_SQL_DROP, connection);
                    //commandDelete.ExecuteNonQuery();

                    SqlCommand commandCreate = new SqlCommand(Constants.StoredProcedures.KEYRESOLVER_SQL, connection);
                    commandCreate.ExecuteNonQuery();
                    connection.Close();
                }

                return true;

            }
            catch (Exception ex)
            {
                Helper.LogMessage(Constants.SystemMessages.KEYRESOLVER_SP_SQL_FAILED + " \n" + ex.Message + " \n sql string for SP");
                return false;
            }
        }

        public bool DropStoredProcedureForSQL(string sqlServerInstanceName, string database, string sqlUserName, string sqlPassword) {

            try
            {
                var connetionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", sqlServerInstanceName, database, sqlUserName, sqlPassword);


                using (SqlConnection connection = new SqlConnection(connetionString))
                {
                    connection.Open();

                    //SqlCommand commandDelete = new SqlCommand(Constants.StoredProcedures.KEYRESOLVER_SQL_DROP, connection);
                    //commandDelete.ExecuteNonQuery();

                    SqlCommand commandCreate = new SqlCommand(Constants.StoredProcedures.KEYRESOLVER_SQL_DROP, connection);
                    commandCreate.ExecuteNonQuery();
                    connection.Close();
                }

                return true;

            }
            catch (Exception ex)
            {
                Helper.LogMessage(Constants.SystemMessages.KEYRESOLVER_SP_SQL_FAILED + " \n" + ex.Message + " \n sql string for SP");
                return false;
            }
            
        }
        public bool ExecuteViewQueryForSQL(string sqlServerInstanceName, string database, string sqlUserName, string sqlPassword, string query) {

            try
            {
                var connetionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", sqlServerInstanceName, database, sqlUserName, sqlPassword);


                using (SqlConnection connection = new SqlConnection(connetionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    connection.Close();
                }

                return true;

            }
            catch (Exception ex)
            {
                Helper.LogMessage(Constants.SystemMessages.KEYRESOLVER_SP_SQL_FAILED + " \n" + ex.Message + " \n sql string for SP : " + query);
                return false;
            }
        }
        public IDictionary<bool, string> CheckHanaClient()
        {

            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " Hana Client Installed ";

            try
            {
                Assembly hanaAssembly = Assembly.LoadFile(pathToDLL);
                result.Add(true, requiredVersionMessage);
                return result;
            }
            catch (Exception ex)
            {

                Helper.LogMessage(Constants.SystemMessages.HANA_CLIENT_NOT_FOUND + " " + ex.Message);
                result.Add(false, Constants.SystemMessages.HANA_CLIENT_NOT_FOUND);
                return result;
            }

        }

        public void SetInstallerInformationEntriesInRegistry(string registryKey, InstallerModel lstSBOModel)
        {

            try
            {
                Helper.LogMessage(" Entering installer information in registry. ");

                Microsoft.Win32.RegistryKey key;
                key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software\\B1WEB");
                var serializedData = JsonConvert.SerializeObject(lstSBOModel);
                key.SetValue(registryKey, serializedData);
                key.Close();

                Helper.LogMessage(" installer information entered in registry. ");
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" failed to enter installer information in registry. " + ex.Message);
            }
        }

        public string ReadRegistryInformation(string registryKey)
        {
            try
            {
                string subKey = @"SOFTWARE\B1WEB";
                RegistryKey key = Registry.LocalMachine;
                RegistryKey skey = key.OpenSubKey(subKey);

                var regKey = skey.GetValue(registryKey);

                if (regKey != null)
                {
                    string strRegistryInfo = skey.GetValue(registryKey).ToString();

                    return strRegistryInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" failed to read registry registry. " + ex.Message);
                return string.Empty;
            }
        }

        public IDictionary<bool, string> RestartSAPServiceBackend()
        {
            
            int timeoutMilliseconds = 100000;
            var serviceName = "SBOJobServiceBackEnd";
            var requiredVersionMessage = " Unable to Restart SBO Job Service BackEnd. ";
            IDictionary<bool, string> result = new Dictionary<bool, string>();

            ServiceController service = new ServiceController(serviceName);
            try
            {
                //var remoteMachine = "192.50.154.72";
                //var userName = "NB-AKHAN\\akhan";
                //var password = "ABC@123Ciit";
                //var serviceName2 = "SBOJobServiceBackEnd";

                //using (new NetworkConnection($"\\\\{remoteMachine}", new NetworkCredential(userName, password)))
                //{
                //    Process.Start("CMD.exe", $"/C sc \\\\{remoteMachine} stop \"{serviceName2}\"&sc \\\\{remoteMachine} start \"{serviceName2}\"");
                //}

                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                Helper.LogMessage("Stopping SBO Job Service BackEnd.");

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                Helper.LogMessage("Stopped SBO Job Service BackEnd.");

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                Helper.LogMessage("Starting SBO Job Service BackEnd.");

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                result.Add(true, " SBO Job Service BackEnd restarted successfully ");

                Helper.LogMessage("SBO Job Service BackEnd restarted successfully.");
                return result;
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" Unable to restart SBO Job Service BackEnd , Error: " + ex.Message);
                result.Add(false, requiredVersionMessage);
                return result;

            }
        }
        public void StopIIS(InstallerModel obj)
        {

            try
            {
                //ProcessStartInfo startInfo = new ProcessStartInfo("iisreset.exe", " /stop");
                //Process.Start(startInfo);
                ServerManager manager = new ServerManager();
                Site defaultSite = manager.Sites[obj.SiteInfo.SiteName];

                if (defaultSite != null)
                {
                    defaultSite.Stop();
                    Helper.LogMessage(" IIS Stopped. ");
                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(" failed to Stop IIS . " + ex.Message);
            }
        }

        public void StopAppPool(InstallerModel obj)
        {

            try
            {
                ServerManager manager = new ServerManager();
                ApplicationPool defaultSite = manager.ApplicationPools[obj.SiteInfo.ApplicationPoolName];

                if (defaultSite != null)
                {
                    defaultSite.Stop();
                    Helper.LogMessage(" App Pool Stopped. ");
                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(" failed to Stop App Pool . " + ex.Message);
            }
        }
        public void StartIIS(InstallerModel obj)
        {

            try
            {
                //ProcessStartInfo startInfo = new ProcessStartInfo("iisreset.exe", " /start");
                //Process.Start(startInfo);
                //Helper.LogMessage(" IIS Started. ");

                ServerManager manager = new ServerManager();
                Site defaultSite = manager.Sites[obj.SiteInfo.SiteName];

                if (defaultSite != null)
                {
                    defaultSite.Start();
                    Helper.LogMessage(" IIS Started. ");
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" failed to Start IIS . " + ex.Message);
            }
        }

        public void StartAppPool(InstallerModel obj)
        {

            try
            {
                ServerManager manager = new ServerManager();
                ApplicationPool defaultSite = manager.ApplicationPools[obj.SiteInfo.ApplicationPoolName];

                if (defaultSite != null)
                {
                    defaultSite.Start();
                    Helper.LogMessage(" App Pool Started. ");
                }
            }
            catch (Exception ex)
            {

                Helper.LogMessage(" failed to Start App Pool . " + ex.Message);
            }
        }

        public void SetUrlConstants(string serviceLayerIP, string serviceLayerIPPort)
        {
            Constants.SERVICEURL.USER_DEFINED_TABLE = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/UserTablesMD");
            Constants.SERVICEURL.USER_DEFINED_FIELDS = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/UserFieldsMD");
            Constants.SERVICEURL.USER_DEFINED_OBJECTS = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/UserObjectsMD");
            Constants.SERVICEURL.LOGIN = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/Login");
            Constants.SERVICEURL.SERVICE_LAYER = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/");
            Constants.SERVICEURL.USER_DEFINED_FIELDS_GET = string.Format("{0}", Constants.SERVICEURL.USER_DEFINED_FIELDS);//string.Format("{0}?$filter=startswith(TableName,'@{1}')", Constants.SERVICEURL.USER_DEFINED_FIELDS, "U_IQ1");
            Constants.SERVICEURL.USER_DEFINED_OBJECTS_GET = string.Format("{0}?$filter=startswith(TableName,'{1}')", Constants.SERVICEURL.USER_DEFINED_OBJECTS, "U_IQ1");
            Constants.SERVICEURL.BATCH = string.Format("{0}:{1}/{2}", serviceLayerIP, serviceLayerIPPort, "b1s/v1/$batch");

            //string.Format("{0}?$filter=startswith(TableName, '@{1}'");
        }

        public async Task<Dictionary<string, bool>> ClearUDOS(SBOUserModel model)
        {
            var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDO.json");
            var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);
            //var sessionId = SendLoginRequest(model);
            var uri = Constants.SERVICEURL.USER_DEFINED_OBJECTS;
            Dictionary<string, bool> retval = new Dictionary<string, bool>();

            var loginResult = SendLoginRequest(model);
            var isSuccess = loginResult.State;

            if (isSuccess)
            {
                var sessionId = loginResult.Message;
                foreach (var udo in userDefinedTableResult)
                {
                    var val = JsonConvert.DeserializeObject<dynamic>(udo);
                    var code = val.Code;
                    string newuri = uri + "(\'" + Convert.ToString(code) + "\')";
                    var result = await Task.Run(() => Interact(newuri, "DELETE", sessionId: sessionId));
                    if (result.ContainsKey(true))
                    {
                        retval.Add(Convert.ToString(code), true);
                    }
                    else
                    {
                        var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);
                        string error = Convert.ToString(resultObject.error);
                        if (error.Contains(Constants.SystemMessages.ENTRY_DOESNOT_EXIST))
                        {
                            retval.Add(Convert.ToString(code), true);
                        }
                        else
                        {
                            retval.Add(Convert.ToString(code), false);
                        }
                    }
                }

                return retval;
            }
            else
            {
                retval.Add(Constants.SystemMessages.LOGIN_FAILED, false);
                return retval;
            }
        }

        public async Task<Dictionary<string, bool>> ClearUDTS(SBOUserModel model)
        {

            var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDT.json");
            var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);
            //var sessionId = SendLoginRequest(model);
            var uri = Constants.SERVICEURL.USER_DEFINED_TABLE;
            Dictionary<string, bool> retval = new Dictionary<string, bool>();

            var loginResult = SendLoginRequest(model);
            var isSuccess = loginResult.State;

            if (isSuccess)
            {
                var sessionId = loginResult.Message;
                foreach (var udt in userDefinedTableResult)
                {
                    var val = JsonConvert.DeserializeObject<dynamic>(udt);
                    var tablename = val.TableName;
                    string newuri = uri + "(\'" + Convert.ToString(tablename) + "\')";
                    var result = await Task.Run(() => Interact(newuri, "DELETE", sessionId: sessionId));
                    if (result.ContainsKey(true))
                    {
                        retval.Add(Convert.ToString(tablename), true);
                    }
                    else
                    {
                        var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);
                        string error = Convert.ToString(resultObject.error);
                        if (error.Contains(Constants.SystemMessages.ENTRY_DOESNOT_EXIST))
                        {
                            retval.Add(Convert.ToString(tablename), true);
                        }
                        else
                        {
                            TryClearingUserDefinedTableAgain(Convert.ToString(tablename), newuri, sessionId, retval);
                        }
                    }
                }
                //bool retry = true;
                //while (retval.ContainsValue(false) && retry)
                //{

                //    var failed = retval.Where(x => !x.Value);//JsonConvert.DeserializeObject<Dictionary<string, bool>>(JsonConvert.SerializeObject(retval.Where(x => !x.Value)));
                //    int failedcount = failed.Count();
                //    foreach (var f in failed)
                //    {
                //        var tablename = f.Key;

                //        try
                //        {

                //            string newuri = uri + "(\'" + Convert.ToString(tablename) + "\')";
                //            var result = await Task.Run(() => Interact(newuri, "DELETE", sessionId: sessionId));
                //            if (result.ContainsKey(true))
                //            {
                //                retval[Convert.ToString(tablename)] = true;
                //            }
                //            else
                //            {
                //                var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);
                //                string error = Convert.ToString(resultObject.error);
                //                if (error.Contains(Constants.SystemMessages.ENTRY_DOESNOT_EXIST))
                //                {
                //                    retval[Convert.ToString(tablename)] = true;
                //                }
                //                else
                //                {
                //                    retval[Convert.ToString(tablename)] = false;
                //                }
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            Helper.LogMessage("Clearing UDTs failed, table name:" + tablename + "-" + ex.ToString());
                //            retval[Convert.ToString(tablename)] = false;
                //        }
                //    }
                //    if (retval.Count(x => !x.Value) >= failedcount)
                //    {
                //        retry = false;
                //    }

                //}

                return retval;
            }
            else
            {
                retval.Add(Constants.SystemMessages.LOGIN_FAILED, true);
                return retval;
            }

        }

        private async void TryClearingUserDefinedTableAgain(string tablename, string newuri, string sessionId, IDictionary<string, bool> retval)
        {
            for (var i = 0; i < no_of_retries; i++)
            {
                var result = await Task.Run(() => Interact(newuri, "DELETE", sessionId: sessionId));
                if (result.ContainsKey(true))
                {
                    retval[Convert.ToString(tablename)] = true;
                    break;
                }
                else
                {
                    retval[Convert.ToString(tablename)] = false;
                }
            }
        }

        public async Task<Dictionary<string, bool>> ClearUDFS(SBOUserModel model)
        {
            var source = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "UDs/UDF.json");
            var userDefinedTableResult = File.ReadAllText(source).Trim().Split(new string[] { "$%&" }, StringSplitOptions.None);
            // var sessionId = SendLoginRequest(model);
            var uri = Constants.SERVICEURL.USER_DEFINED_FIELDS;
            Dictionary<string, bool> retval = new Dictionary<string, bool>();

            var loginResult = SendLoginRequest(model);
            var isSuccess = loginResult.State;

            if (isSuccess)
            {
                var sessionId = loginResult.Message;
                foreach (var udo in userDefinedTableResult)
                {
                    var val = JsonConvert.DeserializeObject<dynamic>(udo);
                    var fieldid = val.FieldID;
                    string tablename = val.TableName;
                    if (!tablename.Contains("@"))
                    {
                        string newuri = uri + "(TableName=\'" + tablename + "\',FieldID=" + Convert.ToString(fieldid) + ")";
                        var result = await Task.Run(() => Interact(newuri, "DELETE", sessionId: sessionId));
                        if (result.ContainsKey(true))
                        {
                            retval.Add(Convert.ToString(tablename + "-" + fieldid), true);
                        }
                        else
                        {
                            var resultObject = JsonConvert.DeserializeObject<dynamic>(result[false]);
                            string error = Convert.ToString(resultObject.error);
                            if (error.Contains(Constants.SystemMessages.ENTRY_DOESNOT_EXIST))
                            {
                                retval.Add(Convert.ToString(tablename + "-" + fieldid), true);
                            }
                            else
                            {
                                retval.Add(Convert.ToString(tablename + "-" + fieldid), false);
                            }
                        }
                    }
                }

                return retval;
            }
            else
            {
                retval.Add(Constants.SystemMessages.LOGIN_FAILED, false);
                return retval;
            }
        }

        public IDictionary<bool, string> CreateSiteInIIS(InstallerModel obj)
        {

            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " IIS Site Created. ";

            try
            {
                string siteName = obj.SiteInfo.SiteName;
                string domainName = obj.SiteInfo.DomainName;
                string appPoolName = obj.SiteInfo.ApplicationPoolName;
                string protocolType = obj.SiteInfo.ProtocolType;
                string webFiles = obj.BackendPath;
                string bindingName = string.Format("{0}:{1}:{2}",obj.SiteInfo.IPAddress,obj.SiteInfo.Port,obj.SiteInfo.DomainName);



                if (!IsWebsiteExists(siteName))
                {
                    ServerManager iisManager = new ServerManager();
                    iisManager.Sites.Add(obj.SiteInfo.SiteName, protocolType, bindingName, webFiles);
                    iisManager.ApplicationDefaults.ApplicationPoolName = appPoolName;
                    iisManager.CommitChanges();


                    var virtualDirResult = CreateVirtualDirectory(obj);

                    var isVirtualDirCreated = virtualDirResult.Keys.FirstOrDefault();
                    var isVirtualDirMessage = virtualDirResult[isVirtualDirCreated];

                    if (isVirtualDirCreated)
                    {
                        result.Add(true, requiredVersionMessage);
                    }
                    else
                    {
                        result.Add(false, isVirtualDirMessage);
                    }

                    Helper.LogMessage(requiredVersionMessage);
                }
                else
                {
                    var message = " Site Already Exists . ";

                    Helper.LogMessage(message);
                    result.Add(true, message);
                }
            }
            catch (Exception ex)
            {
                var message = " Failed to create site in IIS . " + ex.Message;
                Helper.LogMessage(message);
                result.Add(false, message);
            }

            return result;
        }

        public IDictionary<bool, string> CreateAppPool(string poolname, string runtimeVersion = "v4.0")
        {
            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " Application pool created. ";

            try
            {
                if (!IsApplicationPoolExists(poolname))
                {
                    using (ServerManager serverManager = new ServerManager())
                    {
                        ApplicationPool newPool = serverManager.ApplicationPools.Add(poolname);
                        newPool.ManagedRuntimeVersion = runtimeVersion;
                        newPool.Enable32BitAppOnWin64 = true;
                        newPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                        serverManager.CommitChanges();
                    }

                   
                    Helper.LogMessage(requiredVersionMessage);
                    result.Add(true, requiredVersionMessage);
                }
                else
                {
                    var message = " Application pool already exists. ";
                    Helper.LogMessage(message);
                    result.Add(true, message);

                }
            }

            catch (Exception ex)
            {
                var message = " Failed to create Application pool in IIS. " + ex.Message;
                Helper.LogMessage(message);
                result.Add(false, message);
            }

            return result;
        }

        public IDictionary<bool, string> CreateVirtualDirectory(InstallerModel obj)
        {
            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " Virtual directory created. ";

            try
            {
                ServerManager manager = new ServerManager();
                Site defaultSite = manager.Sites[obj.SiteInfo.SiteName];
                var b1WebClientSite = defaultSite.Applications.FirstOrDefault();
                b1WebClientSite.VirtualDirectories.Add(
                    "/ui", obj.FrontEndPath);
                b1WebClientSite.VirtualDirectories.Add(
                    "/assets", Path.Combine(obj.FrontEndPath, "assets"));

                manager.CommitChanges();

                Helper.LogMessage(requiredVersionMessage);
                result.Add(true, requiredVersionMessage);
            }
            catch (Exception ex)
            {
                var message = " Failed to create virtual directory. " + ex.Message;
                Helper.LogMessage(message);
                result.Add(false, message);
            }

            return result;
        }
        public static bool IsWebsiteExists(string strWebsitename)
        {
            ServerManager serverMgr = new ServerManager();
            Boolean flagset = false;
            SiteCollection sitecollection = serverMgr.Sites;
            flagset = sitecollection.Any(x => x.Name == strWebsitename);
            return flagset;
        }

        public static bool IsApplicationPoolExists(string poolName)
        {
            ServerManager serverMgr = new ServerManager();
            Boolean flagset = false;
            flagset = serverMgr.ApplicationPools.Any(x => x.Name == poolName);
            return flagset;
        }

        public List<LocalizationModel> GetLocalizationList() {

            LocalizationModel localizationModelUS = new LocalizationModel() { LanguageFullName = "United States", LanguageShortName = "US" };
            LocalizationModel localizationModelDE = new LocalizationModel() { LanguageFullName = "Germany", LanguageShortName = "DE" };

            List<LocalizationModel> lst = new List<LocalizationModel>() { localizationModelUS, localizationModelDE };

            lst.Insert(0, new LocalizationModel { LanguageFullName = Constants.SystemConstants.PLEASE_SELECT, LanguageShortName = Constants.SystemConstants.PLEASE_SELECT });

            return lst;
        }
        public void RunModelGenerator(string commandLineArugment, SBOUserModel model, string serviceLayerUrl) {

            try
            {
                string xmlFile = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "ModelGenerator", "V360ModelGenerator.exe.config");
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(xmlFile);

                var nodesList = xmlDoc.SelectNodes("configuration/applicationSettings/V360ModelGenerator.Properties.Settings/setting");

                foreach (System.Xml.XmlNode node in nodesList)
                {
                    if (node.Attributes["name"].Value.Equals("ServiceLayerUrl"))
                    {
                        var serviceLayerUrlNode = node.SelectSingleNode("value");
                        serviceLayerUrlNode.InnerText = serviceLayerUrl;
                    }
                    else if (node.Attributes["name"].Value.Equals("UserName"))
                    {
                        var serviceLayerUrlNode = node.SelectSingleNode("value");
                        serviceLayerUrlNode.InnerText = model.sboUserName;
                    }
                    else if (node.Attributes["name"].Value.Equals("Password"))
                    {
                        var serviceLayerUrlNode = node.SelectSingleNode("value");
                        serviceLayerUrlNode.InnerText = model.sboPassword;
                    }
                    else if (node.Attributes["name"].Value.Equals("CompanyDB"))
                    {
                        var serviceLayerUrlNode = node.SelectSingleNode("value");
                        serviceLayerUrlNode.InnerText = model.database;
                    }
                }

                xmlDoc.Save(xmlFile);

                var modGenPath = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER, "ModelGenerator");
                

                //Setting an instance of ProcessStartInfo class under System.Diagnostic Assembly Reference
                // var processInfo = new ProcessStartInfo("cmd.exe", "/c systeminfo")
                ProcessStartInfo pro = new ProcessStartInfo("cmd.exe", "/c " + commandLineArugment);

             

                //Setting up the Working Directory
                pro.WorkingDirectory = modGenPath;
                //pro.Arguments = "V360ModelGenerator.exe --metadata";
                //Instead of using the above two line of codes, You can just use the code below:
                // ProcessStartInfo pro = new ProcessStartInfo("cmd.exe");

                //Creating an Instance of the Process Class which will help to execute our Process
                Process proStart = new Process();

                //Setting up the Process Name here which we are going to start from ProcessStartInfo
                proStart.StartInfo = pro;

                //Calling the Start Method of Process class to Invoke our Process viz 'cmd.exe'
                proStart.Start();

                proStart.WaitForExit();
            }
            catch (Exception ex)
            {
                Helper.LogMessage(string.Format(" Error while running Model Generator . {0}", commandLineArugment) + ex.Message);
            }
        }

        public void CreateCrystalReportFolders(InstallerModel model)
        {

            foreach (var db in model.SBOModelList)
            {
                try
                {
                    var crystalReportfolderPath = Path.Combine(model.BackendPath, "App_Data", Constants.SystemConstants.CRYSTAL_REPORT_FOLDER, db.database);
                    var crystalReportTemplateFolderPath = Path.Combine(Constants.SystemConstants.TEMP_PATH, Constants.SystemConstants.INSTALLATION_DATA_FOLDER,
                        Constants.SystemConstants.CRYSTAL_REPORT_TEMPLATE_FOLDER);


                    var direct = Directory.CreateDirectory(crystalReportfolderPath);

                    foreach (string newPath in Directory.GetFiles(crystalReportTemplateFolderPath, "*.*", SearchOption.AllDirectories))
                        File.Copy(newPath, newPath.Replace(crystalReportTemplateFolderPath, crystalReportfolderPath), true);

                }
                catch (Exception ex)
                {
                    Helper.LogMessage("CreateCrystalReportFolders():" + ex.Message);
                }
            }
        }

        public IDictionary<bool, string> DoesCrytalReportRuntimeExists() {

            IDictionary<bool, string> result = new Dictionary<bool, string>();
            var requiredVersionMessage = " Crystal Report Runtime is required ";

            try
            {
                bool doescrystalReportDirExists = Directory.Exists(crytalReportRunTimePath);
                bool doescrystalReportDirExists64 = Directory.Exists(crytalReportRunTimePath_64Bit);

                if (doescrystalReportDirExists || doescrystalReportDirExists64)
                {
                    var files = doescrystalReportDirExists == true ? Directory.GetFiles(crytalReportRunTimePath, "*.dll", SearchOption.AllDirectories).ToList(): Directory.GetFiles(crytalReportRunTimePath_64Bit, "*.dll", SearchOption.AllDirectories).ToList(); ;
                    //var files64 = 

                    var crReportEngine = files.Where(f => f.Contains("CrystalDecisions.CrystalReports.Engine.dll"));
                    //var crReportEngine64 = files64.Where(f => f.Contains("CrystalDecisions.CrystalReports.Engine.dll"));

                    if (crReportEngine != null)
                    {
                        result.Add(true, "Crystal Report Runtime");
                    }
                    else
                    {
                        result.Add(false, requiredVersionMessage);
                    }

                }
                else
                {
                    result.Add(false, requiredVersionMessage);
                  
                }

                return result;

            }
            catch (Exception ex)
            {
                Helper.LogMessage("DoesCrytalReportRuntimeExists() " + ex.Message);
                result.Add(false,  requiredVersionMessage);
                return result;
                
            }

        }

        
        private string PrepareBatchStatementRequest(string uri, string obj,string batchName = "")
        {
            string changeSetNumber = string.Format("Changeset_{0}", Guid.NewGuid().ToString());
            StringBuilder req = new StringBuilder();

            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(string.Format("Content-Type:multipart/mixed; boundary ={0}",changeSetNumber));
            req.Append(Environment.NewLine);
            req.Append(string.Format("--{0}", changeSetNumber));
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append("Content-Type:application/http");
            req.Append(Environment.NewLine);
            req.Append("Content-Transfer-Encoding:binary");
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(string.Format("Content-ID:{0}",obj));
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(uri);
            req.Append(Environment.NewLine);
            req.Append("Content-Type: application/json");
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(obj);
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(string.Format("--{0}--", changeSetNumber));
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);
            req.Append(string.Format("--{0}", batchName));
            req.Append(Environment.NewLine);
            req.Append(Environment.NewLine);


            return req.ToString();
        }
        private bool IsTableObjectSame(UDT existingObject, UDT newObject)
        {
            var comparer = new ObjectsComparer.Comparer<UDT>();
            //Compare objects
            IEnumerable<Difference> differences;
            var isEqual = comparer.Compare(existingObject, newObject, out differences);
            return isEqual;
        }
        private bool IsFieldObjectSame(UDF existingObject, UDF newObject)
        {
            var b1 =JsonConvert.SerializeObject(existingObject);
            var b2 = JsonConvert.SerializeObject(newObject);
            var ise =  b1.SequenceEqual(b2);

            var comparer = new ObjectsComparer.Comparer<UDF>();
            //Compare objects
            IEnumerable<Difference> differences;
            var isEqual = comparer.Compare(existingObject, newObject, out differences);
            return isEqual;
        }

        private void prepareResponseData(string message) {

            var responseString = message.Split('\n').Where(i => i.StartsWith("--changesetresponse_")).FirstOrDefault();
            var udtResponseArray = message.Split(new string[] { responseString }, StringSplitOptions.None);
        }

    }
}
