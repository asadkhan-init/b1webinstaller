﻿namespace B1WebInstaller
{
    partial class frmB1Setup
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmB1Setup));
            this.opd = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProgressText = new System.Windows.Forms.Label();
            this.PanNavigation = new System.Windows.Forms.Panel();
            this.btnBack = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnNext = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnFinish = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCancel = new MaterialSkin.Controls.MaterialRaisedButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.tabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.wizardTab = new MaterialSkin.Controls.MaterialTabControl();
            this.pnlStep1 = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.txtInstructions = new System.Windows.Forms.TextBox();
            this.pnlStep2 = new System.Windows.Forms.TabPage();
            this.lblSystemCompatibilityError = new System.Windows.Forms.Label();
            this.pnlSystemCompatibility = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbDatabaseTypeHana = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbDatabaseTypeSQL = new MaterialSkin.Controls.MaterialRadioButton();
            this.pnlStep3 = new System.Windows.Forms.TabPage();
            this.btnBrowseBackendPath = new System.Windows.Forms.Button();
            this.btnBrowseFrontendPath = new System.Windows.Forms.Button();
            this.txtB1BackendFolderPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtB1FrontendFolderPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlStep4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label38 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInstallationNumber = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtB1License = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label55 = new System.Windows.Forms.Label();
            this.txtPhoneNo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtUserNameEditable = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtEmailEditable = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCompanyNameEditable = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.btnGenerateLicenseKey = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlStep5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtConfirmUserManagementPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtUserManagementPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label15 = new System.Windows.Forms.Label();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlStep6 = new System.Windows.Forms.TabPage();
            this.gbHanaSingleMultiContainer = new System.Windows.Forms.GroupBox();
            this.txtTenantDB = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.rbMultiContainer = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbSingleContainer = new MaterialSkin.Controls.MaterialRadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtXSEnginePort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtHANAUserPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label53 = new System.Windows.Forms.Label();
            this.txtLicenseServerPort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label57 = new System.Windows.Forms.Label();
            this.txtHANAUser = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label58 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDatabaseEnginePort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label23 = new System.Windows.Forms.Label();
            this.txtServiceLayerIPPort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label45 = new System.Windows.Forms.Label();
            this.txtSQLHanaServerInstanceName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label51 = new System.Windows.Forms.Label();
            this.txtServiceLayerIP = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblSQLHanaServerInstanceName = new System.Windows.Forms.Label();
            this.ddlDatabaseType = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlStep7 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label52 = new System.Windows.Forms.Label();
            this.txtSSHRootPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label46 = new System.Windows.Forms.Label();
            this.txtSSHRootUserName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnTestSSHConnection = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pnlStep8 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.ddlIPList = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.ddlProtocolType = new System.Windows.Forms.ComboBox();
            this.txtPortNumber = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label43 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSiteName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label41 = new System.Windows.Forms.Label();
            this.txtApplicationPool = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtDomainName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.pnlStep9 = new System.Windows.Forms.TabPage();
            this.txtConfirmCertificatePassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtCertificatePassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSSLCertificatePath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnSSLCertificatePath = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlStep11 = new System.Windows.Forms.TabPage();
            this.gvDatabaseInformation = new System.Windows.Forms.DataGridView();
            this.label37 = new System.Windows.Forms.Label();
            this.btnAddUCDatabaseInformation = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.pnlStep12 = new System.Windows.Forms.TabPage();
            this.cbCleanUDs = new System.Windows.Forms.CheckBox();
            this.cbUpdateConfiguration = new System.Windows.Forms.CheckBox();
            this.cbUpdateIIS = new System.Windows.Forms.CheckBox();
            this.cbUpdateSP = new System.Windows.Forms.CheckBox();
            this.cbUpdateDatabase = new System.Windows.Forms.CheckBox();
            this.cbCopyRelease = new System.Windows.Forms.CheckBox();
            this.cblIncludedDatabases = new System.Windows.Forms.CheckedListBox();
            this.lblDatabasesToBeUpdated = new System.Windows.Forms.Label();
            this.pnlParent = new System.Windows.Forms.Panel();
            this.picLoader = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pg = new MaterialSkin.Controls.MaterialProgressBar();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.ucDatabaseInformation1 = new B1WebInstaller.ucDatabaseInformation();
            this.PanNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.wizardTab.SuspendLayout();
            this.pnlStep1.SuspendLayout();
            this.pnlStep2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlStep4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.pnlStep5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnlStep6.SuspendLayout();
            this.gbHanaSingleMultiContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnlStep7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.pnlStep8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.pnlStep9.SuspendLayout();
            this.pnlStep11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDatabaseInformation)).BeginInit();
            this.pnlStep12.SuspendLayout();
            this.pnlParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // opd
            // 
            this.opd.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(46, 324);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 76);
            this.label1.TabIndex = 3;
            this.label1.Text = "B1 Web Installer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProgressText
            // 
            this.lblProgressText.AutoSize = true;
            this.lblProgressText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblProgressText.Location = new System.Drawing.Point(651, 8);
            this.lblProgressText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProgressText.Name = "lblProgressText";
            this.lblProgressText.Size = new System.Drawing.Size(161, 39);
            this.lblProgressText.TabIndex = 31;
            this.lblProgressText.Text = "Progress";
            this.lblProgressText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblProgressText.Visible = false;
            // 
            // PanNavigation
            // 
            this.PanNavigation.BackColor = System.Drawing.SystemColors.ControlDark;
            this.PanNavigation.Controls.Add(this.lblProgressText);
            this.PanNavigation.Controls.Add(this.btnBack);
            this.PanNavigation.Controls.Add(this.btnNext);
            this.PanNavigation.Controls.Add(this.btnFinish);
            this.PanNavigation.Controls.Add(this.btnCancel);
            this.PanNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanNavigation.Location = new System.Drawing.Point(0, 1379);
            this.PanNavigation.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.PanNavigation.Name = "PanNavigation";
            this.PanNavigation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PanNavigation.Size = new System.Drawing.Size(2457, 70);
            this.PanNavigation.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnBack.Depth = 0;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBack.Icon = null;
            this.btnBack.Location = new System.Drawing.Point(1980, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnBack.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnBack.Name = "btnBack";
            this.btnBack.Primary = true;
            this.btnBack.Size = new System.Drawing.Size(106, 70);
            this.btnBack.TabIndex = 52;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.AutoSize = true;
            this.btnNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNext.Depth = 0;
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNext.Icon = null;
            this.btnNext.Location = new System.Drawing.Point(2086, 0);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnNext.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNext.Name = "btnNext";
            this.btnNext.Primary = true;
            this.btnNext.Size = new System.Drawing.Size(104, 70);
            this.btnNext.TabIndex = 60;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.AutoSize = true;
            this.btnFinish.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnFinish.Depth = 0;
            this.btnFinish.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFinish.Icon = null;
            this.btnFinish.Location = new System.Drawing.Point(2190, 0);
            this.btnFinish.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnFinish.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Primary = true;
            this.btnFinish.Size = new System.Drawing.Size(123, 70);
            this.btnFinish.TabIndex = 52;
            this.btnFinish.Text = "Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Visible = false;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancel.Depth = 0;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Icon = null;
            this.btnCancel.Location = new System.Drawing.Point(2313, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnCancel.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Primary = true;
            this.btnCancel.Size = new System.Drawing.Size(144, 70);
            this.btnCancel.TabIndex = 51;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(53, 146);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(352, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label50.Location = new System.Drawing.Point(60, 649);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(101, 32);
            this.label50.TabIndex = 47;
            this.label50.Text = "Email:";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label49.Location = new System.Drawing.Point(59, 549);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(239, 32);
            this.label49.TabIndex = 46;
            this.label49.Text = "Company Name:";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label48.Location = new System.Drawing.Point(59, 451);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(103, 32);
            this.label48.TabIndex = 45;
            this.label48.Text = "Name:";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabSelector
            // 
            this.tabSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSelector.BaseTabControl = this.wizardTab;
            this.tabSelector.Depth = 0;
            this.tabSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSelector.Location = new System.Drawing.Point(473, 110);
            this.tabSelector.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.tabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabSelector.Name = "tabSelector";
            this.tabSelector.Size = new System.Drawing.Size(1772, 115);
            this.tabSelector.TabIndex = 47;
            this.tabSelector.Text = "materialTabSelector1";
            // 
            // wizardTab
            // 
            this.wizardTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardTab.Controls.Add(this.pnlStep1);
            this.wizardTab.Controls.Add(this.pnlStep2);
            this.wizardTab.Controls.Add(this.pnlStep3);
            this.wizardTab.Controls.Add(this.pnlStep4);
            this.wizardTab.Controls.Add(this.pnlStep5);
            this.wizardTab.Controls.Add(this.pnlStep6);
            this.wizardTab.Controls.Add(this.pnlStep7);
            this.wizardTab.Controls.Add(this.pnlStep8);
            this.wizardTab.Controls.Add(this.pnlStep9);
            this.wizardTab.Controls.Add(this.pnlStep11);
            this.wizardTab.Controls.Add(this.pnlStep12);
            this.wizardTab.Depth = 0;
            this.wizardTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardTab.Location = new System.Drawing.Point(473, 239);
            this.wizardTab.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.wizardTab.MouseState = MaterialSkin.MouseState.HOVER;
            this.wizardTab.Name = "wizardTab";
            this.wizardTab.SelectedIndex = 0;
            this.wizardTab.Size = new System.Drawing.Size(1785, 1037);
            this.wizardTab.TabIndex = 48;
            this.wizardTab.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.wizardTab_Selecting);
            // 
            // pnlStep1
            // 
            this.pnlStep1.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlStep1.Controls.Add(this.label21);
            this.pnlStep1.Controls.Add(this.txtInstructions);
            this.pnlStep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlStep1.Location = new System.Drawing.Point(10, 48);
            this.pnlStep1.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep1.Name = "pnlStep1";
            this.pnlStep1.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep1.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep1.TabIndex = 0;
            this.pnlStep1.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoEllipsis = true;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label21.Location = new System.Drawing.Point(105, 31);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(925, 69);
            this.label21.TabIndex = 30;
            this.label21.Text = "Please read instructions carefully.";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.UseWaitCursor = true;
            // 
            // txtInstructions
            // 
            this.txtInstructions.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtInstructions.Location = new System.Drawing.Point(105, 118);
            this.txtInstructions.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtInstructions.Multiline = true;
            this.txtInstructions.Name = "txtInstructions";
            this.txtInstructions.ReadOnly = true;
            this.txtInstructions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInstructions.Size = new System.Drawing.Size(1604, 836);
            this.txtInstructions.TabIndex = 29;
            // 
            // pnlStep2
            // 
            this.pnlStep2.BackColor = System.Drawing.Color.White;
            this.pnlStep2.Controls.Add(this.lblSystemCompatibilityError);
            this.pnlStep2.Controls.Add(this.pnlSystemCompatibility);
            this.pnlStep2.Controls.Add(this.groupBox1);
            this.pnlStep2.Location = new System.Drawing.Point(10, 48);
            this.pnlStep2.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep2.Name = "pnlStep2";
            this.pnlStep2.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep2.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep2.TabIndex = 1;
            this.pnlStep2.Text = "2";
            // 
            // lblSystemCompatibilityError
            // 
            this.lblSystemCompatibilityError.AutoSize = true;
            this.lblSystemCompatibilityError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemCompatibilityError.ForeColor = System.Drawing.Color.Red;
            this.lblSystemCompatibilityError.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSystemCompatibilityError.Location = new System.Drawing.Point(92, 930);
            this.lblSystemCompatibilityError.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblSystemCompatibilityError.Name = "lblSystemCompatibilityError";
            this.lblSystemCompatibilityError.Size = new System.Drawing.Size(876, 31);
            this.lblSystemCompatibilityError.TabIndex = 37;
            this.lblSystemCompatibilityError.Text = "Please fix system compatibility issues to continue with installation.";
            this.lblSystemCompatibilityError.Visible = false;
            // 
            // pnlSystemCompatibility
            // 
            this.pnlSystemCompatibility.Location = new System.Drawing.Point(100, 225);
            this.pnlSystemCompatibility.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlSystemCompatibility.Name = "pnlSystemCompatibility";
            this.pnlSystemCompatibility.Padding = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlSystemCompatibility.Size = new System.Drawing.Size(1630, 612);
            this.pnlSystemCompatibility.TabIndex = 36;
            this.pnlSystemCompatibility.TabStop = false;
            this.pnlSystemCompatibility.Text = "Checking System Compatibility";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbDatabaseTypeHana);
            this.groupBox1.Controls.Add(this.rbDatabaseTypeSQL);
            this.groupBox1.Location = new System.Drawing.Point(105, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.groupBox1.Size = new System.Drawing.Size(1625, 150);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Please select installation  database:";
            // 
            // rbDatabaseTypeHana
            // 
            this.rbDatabaseTypeHana.AutoSize = true;
            this.rbDatabaseTypeHana.Depth = 0;
            this.rbDatabaseTypeHana.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.rbDatabaseTypeHana.Location = new System.Drawing.Point(64, 60);
            this.rbDatabaseTypeHana.Margin = new System.Windows.Forms.Padding(0);
            this.rbDatabaseTypeHana.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbDatabaseTypeHana.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbDatabaseTypeHana.Name = "rbDatabaseTypeHana";
            this.rbDatabaseTypeHana.Ripple = true;
            this.rbDatabaseTypeHana.Size = new System.Drawing.Size(120, 30);
            this.rbDatabaseTypeHana.TabIndex = 33;
            this.rbDatabaseTypeHana.TabStop = true;
            this.rbDatabaseTypeHana.Tag = "DatabaseType";
            this.rbDatabaseTypeHana.Text = "HANA";
            this.rbDatabaseTypeHana.UseVisualStyleBackColor = true;
            this.rbDatabaseTypeHana.CheckedChanged += new System.EventHandler(this.rbDatabaseType_CheckedChanged);
            // 
            // rbDatabaseTypeSQL
            // 
            this.rbDatabaseTypeSQL.AutoSize = true;
            this.rbDatabaseTypeSQL.Depth = 0;
            this.rbDatabaseTypeSQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.rbDatabaseTypeSQL.Location = new System.Drawing.Point(297, 60);
            this.rbDatabaseTypeSQL.Margin = new System.Windows.Forms.Padding(0);
            this.rbDatabaseTypeSQL.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbDatabaseTypeSQL.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbDatabaseTypeSQL.Name = "rbDatabaseTypeSQL";
            this.rbDatabaseTypeSQL.Ripple = true;
            this.rbDatabaseTypeSQL.Size = new System.Drawing.Size(91, 30);
            this.rbDatabaseTypeSQL.TabIndex = 34;
            this.rbDatabaseTypeSQL.TabStop = true;
            this.rbDatabaseTypeSQL.Tag = "DatabaseType";
            this.rbDatabaseTypeSQL.Text = "SQL";
            this.rbDatabaseTypeSQL.UseVisualStyleBackColor = true;
            this.rbDatabaseTypeSQL.CheckedChanged += new System.EventHandler(this.rbDatabaseType_CheckedChanged);
            // 
            // pnlStep3
            // 
            this.pnlStep3.BackColor = System.Drawing.Color.White;
            this.pnlStep3.Controls.Add(this.btnBrowseBackendPath);
            this.pnlStep3.Controls.Add(this.btnBrowseFrontendPath);
            this.pnlStep3.Controls.Add(this.txtB1BackendFolderPath);
            this.pnlStep3.Controls.Add(this.txtB1FrontendFolderPath);
            this.pnlStep3.Controls.Add(this.label40);
            this.pnlStep3.Controls.Add(this.label39);
            this.pnlStep3.Controls.Add(this.label5);
            this.pnlStep3.Controls.Add(this.pictureBox2);
            this.pnlStep3.Controls.Add(this.label6);
            this.pnlStep3.Controls.Add(this.label7);
            this.pnlStep3.Controls.Add(this.label8);
            this.pnlStep3.Location = new System.Drawing.Point(10, 48);
            this.pnlStep3.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep3.Name = "pnlStep3";
            this.pnlStep3.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep3.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep3.TabIndex = 2;
            this.pnlStep3.Text = "3";
            // 
            // btnBrowseBackendPath
            // 
            this.btnBrowseBackendPath.FlatAppearance.BorderSize = 0;
            this.btnBrowseBackendPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseBackendPath.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBrowseBackendPath.Location = new System.Drawing.Point(580, 744);
            this.btnBrowseBackendPath.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnBrowseBackendPath.Name = "btnBrowseBackendPath";
            this.btnBrowseBackendPath.Size = new System.Drawing.Size(160, 62);
            this.btnBrowseBackendPath.TabIndex = 64;
            this.btnBrowseBackendPath.Text = "Browse...";
            this.btnBrowseBackendPath.UseVisualStyleBackColor = true;
            this.btnBrowseBackendPath.Click += new System.EventHandler(this.btnBrowseBackendPath_Click);
            // 
            // btnBrowseFrontendPath
            // 
            this.btnBrowseFrontendPath.FlatAppearance.BorderSize = 0;
            this.btnBrowseFrontendPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseFrontendPath.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBrowseFrontendPath.Location = new System.Drawing.Point(580, 504);
            this.btnBrowseFrontendPath.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnBrowseFrontendPath.Name = "btnBrowseFrontendPath";
            this.btnBrowseFrontendPath.Size = new System.Drawing.Size(160, 62);
            this.btnBrowseFrontendPath.TabIndex = 63;
            this.btnBrowseFrontendPath.Text = "Browse...";
            this.btnBrowseFrontendPath.UseVisualStyleBackColor = true;
            this.btnBrowseFrontendPath.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtB1BackendFolderPath
            // 
            this.txtB1BackendFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtB1BackendFolderPath.Depth = 0;
            this.txtB1BackendFolderPath.Hint = "This is a hint";
            this.txtB1BackendFolderPath.Location = new System.Drawing.Point(105, 843);
            this.txtB1BackendFolderPath.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.txtB1BackendFolderPath.MaxLength = 32767;
            this.txtB1BackendFolderPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtB1BackendFolderPath.Name = "txtB1BackendFolderPath";
            this.txtB1BackendFolderPath.PasswordChar = '\0';
            this.txtB1BackendFolderPath.SelectedText = "";
            this.txtB1BackendFolderPath.SelectionLength = 0;
            this.txtB1BackendFolderPath.SelectionStart = 0;
            this.txtB1BackendFolderPath.Size = new System.Drawing.Size(0, 46);
            this.txtB1BackendFolderPath.TabIndex = 60;
            this.txtB1BackendFolderPath.TabStop = false;
            this.txtB1BackendFolderPath.Tag = "Folder Path is required.";
            this.txtB1BackendFolderPath.Text = "C:\\B1Web_Installer\\B1 Web\\Backend";
            this.txtB1BackendFolderPath.UseSystemPasswordChar = false;
            // 
            // txtB1FrontendFolderPath
            // 
            this.txtB1FrontendFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtB1FrontendFolderPath.Depth = 0;
            this.txtB1FrontendFolderPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB1FrontendFolderPath.Hint = "This is a hint";
            this.txtB1FrontendFolderPath.Location = new System.Drawing.Point(105, 603);
            this.txtB1FrontendFolderPath.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.txtB1FrontendFolderPath.MaxLength = 32767;
            this.txtB1FrontendFolderPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtB1FrontendFolderPath.Name = "txtB1FrontendFolderPath";
            this.txtB1FrontendFolderPath.PasswordChar = '\0';
            this.txtB1FrontendFolderPath.SelectedText = "";
            this.txtB1FrontendFolderPath.SelectionLength = 0;
            this.txtB1FrontendFolderPath.SelectionStart = 0;
            this.txtB1FrontendFolderPath.Size = new System.Drawing.Size(0, 46);
            this.txtB1FrontendFolderPath.TabIndex = 59;
            this.txtB1FrontendFolderPath.TabStop = false;
            this.txtB1FrontendFolderPath.Tag = "Folder Path is required.";
            this.txtB1FrontendFolderPath.Text = "C:\\B1Web_Installer\\B1 Web\\Frontend";
            this.txtB1FrontendFolderPath.UseSystemPasswordChar = false;
            // 
            // label40
            // 
            this.label40.AutoEllipsis = true;
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label40.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label40.Location = new System.Drawing.Point(105, 759);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(414, 31);
            this.label40.TabIndex = 58;
            this.label40.Text = "Please enter Backend folder path";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label39
            // 
            this.label39.AutoEllipsis = true;
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label39.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label39.Location = new System.Drawing.Point(105, 519);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(417, 31);
            this.label39.TabIndex = 57;
            this.label39.Text = "Please enter Frontend folder path";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoEllipsis = true;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(105, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(792, 69);
            this.label5.TabIndex = 50;
            this.label5.Text = "Select Desitination Location";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(105, 239);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(92, 85);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoEllipsis = true;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label6.Location = new System.Drawing.Point(105, 121);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(653, 46);
            this.label6.TabIndex = 51;
            this.label6.Text = "Where should B1 Web be installed?";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label7.Location = new System.Drawing.Point(105, 397);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(992, 31);
            this.label7.TabIndex = 52;
            this.label7.Text = "To continue, click Next, If you would like to select a different folder, click Br" +
    "owser.";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoEllipsis = true;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label8.Location = new System.Drawing.Point(254, 263);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(668, 36);
            this.label8.TabIndex = 53;
            this.label8.Text = "Setup will install B1 Web into the following folder.";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep4
            // 
            this.pnlStep4.AutoScroll = true;
            this.pnlStep4.BackColor = System.Drawing.Color.White;
            this.pnlStep4.Controls.Add(this.tableLayoutPanel4);
            this.pnlStep4.Controls.Add(this.tableLayoutPanel3);
            this.pnlStep4.Controls.Add(this.btnGenerateLicenseKey);
            this.pnlStep4.Controls.Add(this.label3);
            this.pnlStep4.Controls.Add(this.label4);
            this.pnlStep4.Controls.Add(this.label10);
            this.pnlStep4.Location = new System.Drawing.Point(10, 48);
            this.pnlStep4.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep4.Name = "pnlStep4";
            this.pnlStep4.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep4.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep4.TabIndex = 3;
            this.pnlStep4.Text = "4";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.50801F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.492F));
            this.tableLayoutPanel4.Controls.Add(this.label38, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtInstallationNumber, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtB1License, 1, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(135, 285);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1374, 193);
            this.tableLayoutPanel4.TabIndex = 59;
            // 
            // label38
            // 
            this.label38.AutoEllipsis = true;
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label38.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label38.Location = new System.Drawing.Point(5, 0);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(248, 31);
            this.label38.TabIndex = 52;
            this.label38.Text = "Installation Number";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoEllipsis = true;
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label9.Location = new System.Drawing.Point(5, 96);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(272, 31);
            this.label9.TabIndex = 49;
            this.label9.Text = "B1 Web License Key:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInstallationNumber
            // 
            this.txtInstallationNumber.Depth = 0;
            this.txtInstallationNumber.Hint = "10 digit installaton number (e.g 0520807842)";
            this.txtInstallationNumber.Location = new System.Drawing.Point(327, 2);
            this.txtInstallationNumber.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtInstallationNumber.MaxLength = 32767;
            this.txtInstallationNumber.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtInstallationNumber.Name = "txtInstallationNumber";
            this.txtInstallationNumber.PasswordChar = '\0';
            this.txtInstallationNumber.SelectedText = "";
            this.txtInstallationNumber.SelectionLength = 0;
            this.txtInstallationNumber.SelectionStart = 0;
            this.txtInstallationNumber.Size = new System.Drawing.Size(1043, 46);
            this.txtInstallationNumber.TabIndex = 56;
            this.txtInstallationNumber.TabStop = false;
            this.txtInstallationNumber.Tag = "Installation number is required.";
            this.txtInstallationNumber.UseSystemPasswordChar = false;
            // 
            // txtB1License
            // 
            this.txtB1License.Depth = 0;
            this.txtB1License.Hint = "";
            this.txtB1License.Location = new System.Drawing.Point(327, 98);
            this.txtB1License.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtB1License.MaxLength = 32767;
            this.txtB1License.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtB1License.Name = "txtB1License";
            this.txtB1License.PasswordChar = '\0';
            this.txtB1License.SelectedText = "";
            this.txtB1License.SelectionLength = 0;
            this.txtB1License.SelectionStart = 0;
            this.txtB1License.Size = new System.Drawing.Size(1043, 46);
            this.txtB1License.TabIndex = 57;
            this.txtB1License.TabStop = false;
            this.txtB1License.Tag = "LIcense key is required.";
            this.txtB1License.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label55, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtPhoneNo, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtUserNameEditable, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtEmailEditable, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtCompanyNameEditable, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label54, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label56, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(135, 528);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.43925F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.56075F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1374, 342);
            this.tableLayoutPanel3.TabIndex = 58;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label55.Location = new System.Drawing.Point(4, 0);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(98, 32);
            this.label55.TabIndex = 46;
            this.label55.Text = "Name:";
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Depth = 0;
            this.txtPhoneNo.Hint = "+4924627448942";
            this.txtPhoneNo.Location = new System.Drawing.Point(697, 236);
            this.txtPhoneNo.Margin = new System.Windows.Forms.Padding(10);
            this.txtPhoneNo.MaxLength = 32767;
            this.txtPhoneNo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.PasswordChar = '\0';
            this.txtPhoneNo.SelectedText = "";
            this.txtPhoneNo.SelectionLength = 0;
            this.txtPhoneNo.SelectionStart = 0;
            this.txtPhoneNo.Size = new System.Drawing.Size(667, 46);
            this.txtPhoneNo.TabIndex = 61;
            this.txtPhoneNo.TabStop = false;
            this.txtPhoneNo.Tag = "Phone is required.";
            this.txtPhoneNo.UseSystemPasswordChar = false;
            // 
            // txtUserNameEditable
            // 
            this.txtUserNameEditable.Depth = 0;
            this.txtUserNameEditable.Hint = "";
            this.txtUserNameEditable.Location = new System.Drawing.Point(10, 62);
            this.txtUserNameEditable.Margin = new System.Windows.Forms.Padding(10);
            this.txtUserNameEditable.MaxLength = 32767;
            this.txtUserNameEditable.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtUserNameEditable.Name = "txtUserNameEditable";
            this.txtUserNameEditable.PasswordChar = '\0';
            this.txtUserNameEditable.SelectedText = "";
            this.txtUserNameEditable.SelectionLength = 0;
            this.txtUserNameEditable.SelectionStart = 0;
            this.txtUserNameEditable.Size = new System.Drawing.Size(667, 46);
            this.txtUserNameEditable.TabIndex = 58;
            this.txtUserNameEditable.TabStop = false;
            this.txtUserNameEditable.Tag = "Name is required.";
            this.txtUserNameEditable.UseSystemPasswordChar = false;
            // 
            // txtEmailEditable
            // 
            this.txtEmailEditable.Depth = 0;
            this.txtEmailEditable.Hint = "example@domain.com";
            this.txtEmailEditable.Location = new System.Drawing.Point(10, 236);
            this.txtEmailEditable.Margin = new System.Windows.Forms.Padding(10);
            this.txtEmailEditable.MaxLength = 32767;
            this.txtEmailEditable.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtEmailEditable.Name = "txtEmailEditable";
            this.txtEmailEditable.PasswordChar = '\0';
            this.txtEmailEditable.SelectedText = "";
            this.txtEmailEditable.SelectionLength = 0;
            this.txtEmailEditable.SelectionStart = 0;
            this.txtEmailEditable.Size = new System.Drawing.Size(667, 46);
            this.txtEmailEditable.TabIndex = 60;
            this.txtEmailEditable.TabStop = false;
            this.txtEmailEditable.Tag = "Email is required.";
            this.txtEmailEditable.UseSystemPasswordChar = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(691, 178);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(149, 32);
            this.label14.TabIndex = 53;
            this.label14.Text = "Phone No:";
            // 
            // txtCompanyNameEditable
            // 
            this.txtCompanyNameEditable.Depth = 0;
            this.txtCompanyNameEditable.Hint = "";
            this.txtCompanyNameEditable.Location = new System.Drawing.Point(697, 62);
            this.txtCompanyNameEditable.Margin = new System.Windows.Forms.Padding(10);
            this.txtCompanyNameEditable.MaxLength = 32767;
            this.txtCompanyNameEditable.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCompanyNameEditable.Name = "txtCompanyNameEditable";
            this.txtCompanyNameEditable.PasswordChar = '\0';
            this.txtCompanyNameEditable.SelectedText = "";
            this.txtCompanyNameEditable.SelectionLength = 0;
            this.txtCompanyNameEditable.SelectionStart = 0;
            this.txtCompanyNameEditable.Size = new System.Drawing.Size(667, 46);
            this.txtCompanyNameEditable.TabIndex = 59;
            this.txtCompanyNameEditable.TabStop = false;
            this.txtCompanyNameEditable.Tag = "Company Name is required.";
            this.txtCompanyNameEditable.UseSystemPasswordChar = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label54.Location = new System.Drawing.Point(691, 0);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(226, 32);
            this.label54.TabIndex = 47;
            this.label54.Text = "Company Name:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label56.Location = new System.Drawing.Point(4, 178);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(95, 32);
            this.label56.TabIndex = 48;
            this.label56.Text = "Email:";
            // 
            // btnGenerateLicenseKey
            // 
            this.btnGenerateLicenseKey.FlatAppearance.BorderSize = 0;
            this.btnGenerateLicenseKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateLicenseKey.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnGenerateLicenseKey.Location = new System.Drawing.Point(1546, 275);
            this.btnGenerateLicenseKey.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnGenerateLicenseKey.Name = "btnGenerateLicenseKey";
            this.btnGenerateLicenseKey.Size = new System.Drawing.Size(160, 62);
            this.btnGenerateLicenseKey.TabIndex = 54;
            this.btnGenerateLicenseKey.Text = "Generate";
            this.btnGenerateLicenseKey.UseVisualStyleBackColor = true;
            this.btnGenerateLicenseKey.Click += new System.EventHandler(this.btnGenerateLicenseKey_Click);
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(105, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(397, 69);
            this.label3.TabIndex = 47;
            this.label3.Text = "Configuration";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label4.Location = new System.Drawing.Point(127, 121);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(311, 46);
            this.label4.TabIndex = 48;
            this.label4.Text = "B1 Web License";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoEllipsis = true;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label10.Location = new System.Drawing.Point(132, 181);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(1373, 36);
            this.label10.TabIndex = 50;
            this.label10.Text = "Please enter the installation number and press Generate to receive a 30-days tria" +
    "l license key via email:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep5
            // 
            this.pnlStep5.BackColor = System.Drawing.Color.White;
            this.pnlStep5.Controls.Add(this.tableLayoutPanel2);
            this.pnlStep5.Controls.Add(this.materialRaisedButton2);
            this.pnlStep5.Controls.Add(this.label11);
            this.pnlStep5.Controls.Add(this.label12);
            this.pnlStep5.Location = new System.Drawing.Point(10, 48);
            this.pnlStep5.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep5.Name = "pnlStep5";
            this.pnlStep5.Padding = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep5.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep5.TabIndex = 4;
            this.pnlStep5.Text = "5";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtConfirmUserManagementPassword, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtUserManagementPassword, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label15, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(117, 218);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.55556F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 69.44444F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1494, 144);
            this.tableLayoutPanel2.TabIndex = 61;
            // 
            // label13
            // 
            this.label13.AutoEllipsis = true;
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label13.Location = new System.Drawing.Point(5, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(558, 31);
            this.label13.TabIndex = 33;
            this.label13.Text = "Please set a password for user management:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtConfirmUserManagementPassword
            // 
            this.txtConfirmUserManagementPassword.Depth = 0;
            this.txtConfirmUserManagementPassword.Hint = "Please Confirm Password";
            this.txtConfirmUserManagementPassword.Location = new System.Drawing.Point(751, 46);
            this.txtConfirmUserManagementPassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtConfirmUserManagementPassword.MaxLength = 32767;
            this.txtConfirmUserManagementPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConfirmUserManagementPassword.Name = "txtConfirmUserManagementPassword";
            this.txtConfirmUserManagementPassword.PasswordChar = '*';
            this.txtConfirmUserManagementPassword.SelectedText = "";
            this.txtConfirmUserManagementPassword.SelectionLength = 0;
            this.txtConfirmUserManagementPassword.SelectionStart = 0;
            this.txtConfirmUserManagementPassword.Size = new System.Drawing.Size(739, 46);
            this.txtConfirmUserManagementPassword.TabIndex = 60;
            this.txtConfirmUserManagementPassword.TabStop = false;
            this.txtConfirmUserManagementPassword.Tag = "Confirm User Management Password.";
            this.txtConfirmUserManagementPassword.UseSystemPasswordChar = false;
            // 
            // txtUserManagementPassword
            // 
            this.txtUserManagementPassword.Depth = 0;
            this.txtUserManagementPassword.Hint = "B1 Web Client User Managment Password";
            this.txtUserManagementPassword.Location = new System.Drawing.Point(4, 46);
            this.txtUserManagementPassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtUserManagementPassword.MaxLength = 32767;
            this.txtUserManagementPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtUserManagementPassword.Name = "txtUserManagementPassword";
            this.txtUserManagementPassword.PasswordChar = '*';
            this.txtUserManagementPassword.SelectedText = "";
            this.txtUserManagementPassword.SelectionLength = 0;
            this.txtUserManagementPassword.SelectionStart = 0;
            this.txtUserManagementPassword.Size = new System.Drawing.Size(739, 46);
            this.txtUserManagementPassword.TabIndex = 59;
            this.txtUserManagementPassword.TabStop = false;
            this.txtUserManagementPassword.Tag = "User Management Password is required.";
            this.txtUserManagementPassword.UseSystemPasswordChar = false;
            // 
            // label15
            // 
            this.label15.AutoEllipsis = true;
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label15.Location = new System.Drawing.Point(752, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(244, 31);
            this.label15.TabIndex = 35;
            this.label15.Text = "Confirm Password:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.materialRaisedButton2.AutoSize = true;
            this.materialRaisedButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Icon = null;
            this.materialRaisedButton2.Location = new System.Drawing.Point(-32768, 226);
            this.materialRaisedButton2.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(87, 36);
            this.materialRaisedButton2.TabIndex = 1;
            this.materialRaisedButton2.Text = "Add";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoEllipsis = true;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(105, 31);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(397, 69);
            this.label11.TabIndex = 31;
            this.label11.Text = "Configuration";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.AutoEllipsis = true;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label12.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label12.Location = new System.Drawing.Point(105, 121);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(357, 46);
            this.label12.TabIndex = 32;
            this.label12.Text = "User Management ";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep6
            // 
            this.pnlStep6.AutoScroll = true;
            this.pnlStep6.BackColor = System.Drawing.Color.White;
            this.pnlStep6.Controls.Add(this.gbHanaSingleMultiContainer);
            this.pnlStep6.Controls.Add(this.tableLayoutPanel1);
            this.pnlStep6.Controls.Add(this.ddlDatabaseType);
            this.pnlStep6.Controls.Add(this.label47);
            this.pnlStep6.Controls.Add(this.label17);
            this.pnlStep6.Controls.Add(this.label18);
            this.pnlStep6.Location = new System.Drawing.Point(10, 48);
            this.pnlStep6.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlStep6.Name = "pnlStep6";
            this.pnlStep6.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep6.TabIndex = 5;
            this.pnlStep6.Text = "6";
            // 
            // gbHanaSingleMultiContainer
            // 
            this.gbHanaSingleMultiContainer.Controls.Add(this.txtTenantDB);
            this.gbHanaSingleMultiContainer.Controls.Add(this.rbMultiContainer);
            this.gbHanaSingleMultiContainer.Controls.Add(this.rbSingleContainer);
            this.gbHanaSingleMultiContainer.Location = new System.Drawing.Point(119, 317);
            this.gbHanaSingleMultiContainer.Name = "gbHanaSingleMultiContainer";
            this.gbHanaSingleMultiContainer.Size = new System.Drawing.Size(1269, 120);
            this.gbHanaSingleMultiContainer.TabIndex = 68;
            this.gbHanaSingleMultiContainer.TabStop = false;
            // 
            // txtTenantDB
            // 
            this.txtTenantDB.Depth = 0;
            this.txtTenantDB.Hint = "DB1";
            this.txtTenantDB.Location = new System.Drawing.Point(889, 37);
            this.txtTenantDB.MaxLength = 32767;
            this.txtTenantDB.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtTenantDB.Name = "txtTenantDB";
            this.txtTenantDB.PasswordChar = '\0';
            this.txtTenantDB.SelectedText = "";
            this.txtTenantDB.SelectionLength = 0;
            this.txtTenantDB.SelectionStart = 0;
            this.txtTenantDB.Size = new System.Drawing.Size(314, 46);
            this.txtTenantDB.TabIndex = 2;
            this.txtTenantDB.TabStop = false;
            this.txtTenantDB.UseSystemPasswordChar = false;
            // 
            // rbMultiContainer
            // 
            this.rbMultiContainer.AutoSize = true;
            this.rbMultiContainer.Depth = 0;
            this.rbMultiContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.rbMultiContainer.Location = new System.Drawing.Point(463, 49);
            this.rbMultiContainer.Margin = new System.Windows.Forms.Padding(0);
            this.rbMultiContainer.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbMultiContainer.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbMultiContainer.Name = "rbMultiContainer";
            this.rbMultiContainer.Ripple = true;
            this.rbMultiContainer.Size = new System.Drawing.Size(289, 30);
            this.rbMultiContainer.TabIndex = 1;
            this.rbMultiContainer.TabStop = true;
            this.rbMultiContainer.Text = "Multiple Container";
            this.rbMultiContainer.UseVisualStyleBackColor = true;
            this.rbMultiContainer.CheckedChanged += new System.EventHandler(this.rbHanaContainerType_CheckedChanged);
            // 
            // rbSingleContainer
            // 
            this.rbSingleContainer.AutoSize = true;
            this.rbSingleContainer.Depth = 0;
            this.rbSingleContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.rbSingleContainer.Location = new System.Drawing.Point(56, 49);
            this.rbSingleContainer.Margin = new System.Windows.Forms.Padding(0);
            this.rbSingleContainer.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbSingleContainer.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbSingleContainer.Name = "rbSingleContainer";
            this.rbSingleContainer.Ripple = true;
            this.rbSingleContainer.Size = new System.Drawing.Size(262, 30);
            this.rbSingleContainer.TabIndex = 0;
            this.rbSingleContainer.TabStop = true;
            this.rbSingleContainer.Text = "Single Container";
            this.rbSingleContainer.UseVisualStyleBackColor = true;
            this.rbSingleContainer.CheckedChanged += new System.EventHandler(this.rbHanaContainerType_CheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtXSEnginePort, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtHANAUserPassword, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label53, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtLicenseServerPort, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label57, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtHANAUser, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label58, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label16, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDatabaseEnginePort, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label23, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtServiceLayerIPPort, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label45, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtSQLHanaServerInstanceName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label51, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtServiceLayerIP, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblSQLHanaServerInstanceName, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(118, 459);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.935508F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.87176F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.986474F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.57739F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.280847F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.2538F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.60444F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.48979F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1252, 669);
            this.tableLayoutPanel1.TabIndex = 67;
            // 
            // txtXSEnginePort
            // 
            this.txtXSEnginePort.Depth = 0;
            this.txtXSEnginePort.Hint = "";
            this.txtXSEnginePort.Location = new System.Drawing.Point(636, 584);
            this.txtXSEnginePort.Margin = new System.Windows.Forms.Padding(10);
            this.txtXSEnginePort.MaxLength = 32767;
            this.txtXSEnginePort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtXSEnginePort.Name = "txtXSEnginePort";
            this.txtXSEnginePort.PasswordChar = '\0';
            this.txtXSEnginePort.SelectedText = "";
            this.txtXSEnginePort.SelectionLength = 0;
            this.txtXSEnginePort.SelectionStart = 0;
            this.txtXSEnginePort.Size = new System.Drawing.Size(250, 46);
            this.txtXSEnginePort.TabIndex = 66;
            this.txtXSEnginePort.TabStop = false;
            this.txtXSEnginePort.Tag = "XS Engine Port is required.";
            this.txtXSEnginePort.Text = "4300";
            this.txtXSEnginePort.UseSystemPasswordChar = false;
            // 
            // txtHANAUserPassword
            // 
            this.txtHANAUserPassword.Depth = 0;
            this.txtHANAUserPassword.Hint = "HANA or SQL Studio Password";
            this.txtHANAUserPassword.Location = new System.Drawing.Point(636, 228);
            this.txtHANAUserPassword.Margin = new System.Windows.Forms.Padding(10);
            this.txtHANAUserPassword.MaxLength = 32767;
            this.txtHANAUserPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtHANAUserPassword.Name = "txtHANAUserPassword";
            this.txtHANAUserPassword.PasswordChar = '*';
            this.txtHANAUserPassword.SelectedText = "";
            this.txtHANAUserPassword.SelectionLength = 0;
            this.txtHANAUserPassword.SelectionStart = 0;
            this.txtHANAUserPassword.Size = new System.Drawing.Size(567, 46);
            this.txtHANAUserPassword.TabIndex = 62;
            this.txtHANAUserPassword.TabStop = false;
            this.txtHANAUserPassword.Tag = "Password is required.";
            this.txtHANAUserPassword.UseSystemPasswordChar = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(626, 524);
            this.label53.Margin = new System.Windows.Forms.Padding(0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(198, 31);
            this.label53.TabIndex = 69;
            this.label53.Text = "XS Engine Port";
            // 
            // txtLicenseServerPort
            // 
            this.txtLicenseServerPort.Depth = 0;
            this.txtLicenseServerPort.Hint = "";
            this.txtLicenseServerPort.Location = new System.Drawing.Point(636, 406);
            this.txtLicenseServerPort.Margin = new System.Windows.Forms.Padding(10);
            this.txtLicenseServerPort.MaxLength = 32767;
            this.txtLicenseServerPort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtLicenseServerPort.Name = "txtLicenseServerPort";
            this.txtLicenseServerPort.PasswordChar = '\0';
            this.txtLicenseServerPort.SelectedText = "";
            this.txtLicenseServerPort.SelectionLength = 0;
            this.txtLicenseServerPort.SelectionStart = 0;
            this.txtLicenseServerPort.Size = new System.Drawing.Size(239, 46);
            this.txtLicenseServerPort.TabIndex = 65;
            this.txtLicenseServerPort.TabStop = false;
            this.txtLicenseServerPort.Tag = "License Server Port is required.";
            this.txtLicenseServerPort.Text = "40000";
            this.txtLicenseServerPort.UseSystemPasswordChar = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label57.Location = new System.Drawing.Point(626, 348);
            this.label57.Margin = new System.Windows.Forms.Padding(0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(271, 32);
            this.label57.TabIndex = 48;
            this.label57.Text = "License Server Port:";
            this.label57.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txtHANAUser
            // 
            this.txtHANAUser.Depth = 0;
            this.txtHANAUser.Hint = "(e.g sa)";
            this.txtHANAUser.Location = new System.Drawing.Point(10, 228);
            this.txtHANAUser.Margin = new System.Windows.Forms.Padding(10);
            this.txtHANAUser.MaxLength = 32767;
            this.txtHANAUser.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtHANAUser.Name = "txtHANAUser";
            this.txtHANAUser.PasswordChar = '\0';
            this.txtHANAUser.SelectedText = "";
            this.txtHANAUser.SelectionLength = 0;
            this.txtHANAUser.SelectionStart = 0;
            this.txtHANAUser.Size = new System.Drawing.Size(545, 46);
            this.txtHANAUser.TabIndex = 61;
            this.txtHANAUser.TabStop = false;
            this.txtHANAUser.Tag = "User name is required.";
            this.txtHANAUser.UseSystemPasswordChar = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label58.Location = new System.Drawing.Point(0, 524);
            this.label58.Margin = new System.Windows.Forms.Padding(0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(301, 32);
            this.label58.TabIndex = 47;
            this.label58.Text = "Database Engine Port:";
            // 
            // label16
            // 
            this.label16.AutoEllipsis = true;
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label16.Location = new System.Drawing.Point(626, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(391, 31);
            this.label16.TabIndex = 50;
            this.label16.Text = "Service Layer IP (HANA/SQL ):";
            this.label16.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txtDatabaseEnginePort
            // 
            this.txtDatabaseEnginePort.Depth = 0;
            this.txtDatabaseEnginePort.Hint = "";
            this.txtDatabaseEnginePort.Location = new System.Drawing.Point(10, 584);
            this.txtDatabaseEnginePort.Margin = new System.Windows.Forms.Padding(10);
            this.txtDatabaseEnginePort.MaxLength = 32767;
            this.txtDatabaseEnginePort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDatabaseEnginePort.Name = "txtDatabaseEnginePort";
            this.txtDatabaseEnginePort.PasswordChar = '\0';
            this.txtDatabaseEnginePort.SelectedText = "";
            this.txtDatabaseEnginePort.SelectionLength = 0;
            this.txtDatabaseEnginePort.SelectionStart = 0;
            this.txtDatabaseEnginePort.Size = new System.Drawing.Size(239, 46);
            this.txtDatabaseEnginePort.TabIndex = 64;
            this.txtDatabaseEnginePort.TabStop = false;
            this.txtDatabaseEnginePort.Tag = "Database Server Port is required.";
            this.txtDatabaseEnginePort.Text = "30015";
            this.txtDatabaseEnginePort.UseSystemPasswordChar = false;
            // 
            // label23
            // 
            this.label23.AutoEllipsis = true;
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label23.Location = new System.Drawing.Point(626, 178);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(287, 31);
            this.label23.TabIndex = 52;
            this.label23.Text = "HANA/SQL Password:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServiceLayerIPPort
            // 
            this.txtServiceLayerIPPort.Depth = 0;
            this.txtServiceLayerIPPort.Hint = "";
            this.txtServiceLayerIPPort.Location = new System.Drawing.Point(10, 406);
            this.txtServiceLayerIPPort.Margin = new System.Windows.Forms.Padding(10);
            this.txtServiceLayerIPPort.MaxLength = 32767;
            this.txtServiceLayerIPPort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServiceLayerIPPort.Name = "txtServiceLayerIPPort";
            this.txtServiceLayerIPPort.PasswordChar = '\0';
            this.txtServiceLayerIPPort.SelectedText = "";
            this.txtServiceLayerIPPort.SelectionLength = 0;
            this.txtServiceLayerIPPort.SelectionStart = 0;
            this.txtServiceLayerIPPort.Size = new System.Drawing.Size(250, 46);
            this.txtServiceLayerIPPort.TabIndex = 68;
            this.txtServiceLayerIPPort.TabStop = false;
            this.txtServiceLayerIPPort.Text = "50000";
            this.txtServiceLayerIPPort.UseSystemPasswordChar = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(0, 348);
            this.label45.Margin = new System.Windows.Forms.Padding(0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(237, 31);
            this.label45.TabIndex = 67;
            this.label45.Text = "Service Layer Port";
            this.label45.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txtSQLHanaServerInstanceName
            // 
            this.txtSQLHanaServerInstanceName.Depth = 0;
            this.txtSQLHanaServerInstanceName.Hint = "https://192.168.9.72 | MY-PC";
            this.txtSQLHanaServerInstanceName.Location = new System.Drawing.Point(10, 49);
            this.txtSQLHanaServerInstanceName.Margin = new System.Windows.Forms.Padding(10);
            this.txtSQLHanaServerInstanceName.MaxLength = 32767;
            this.txtSQLHanaServerInstanceName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSQLHanaServerInstanceName.Name = "txtSQLHanaServerInstanceName";
            this.txtSQLHanaServerInstanceName.PasswordChar = '\0';
            this.txtSQLHanaServerInstanceName.SelectedText = "";
            this.txtSQLHanaServerInstanceName.SelectionLength = 0;
            this.txtSQLHanaServerInstanceName.SelectionStart = 0;
            this.txtSQLHanaServerInstanceName.Size = new System.Drawing.Size(545, 46);
            this.txtSQLHanaServerInstanceName.TabIndex = 66;
            this.txtSQLHanaServerInstanceName.TabStop = false;
            this.txtSQLHanaServerInstanceName.Tag = "Service Layer IP is required.";
            this.txtSQLHanaServerInstanceName.UseSystemPasswordChar = false;
            // 
            // label51
            // 
            this.label51.AutoEllipsis = true;
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label51.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label51.Location = new System.Drawing.Point(5, 178);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(225, 31);
            this.label51.TabIndex = 56;
            this.label51.Text = "HANA/SQL User:";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServiceLayerIP
            // 
            this.txtServiceLayerIP.Depth = 0;
            this.txtServiceLayerIP.Hint = "https://Server-Name";
            this.txtServiceLayerIP.Location = new System.Drawing.Point(636, 49);
            this.txtServiceLayerIP.Margin = new System.Windows.Forms.Padding(10);
            this.txtServiceLayerIP.MaxLength = 32767;
            this.txtServiceLayerIP.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServiceLayerIP.Name = "txtServiceLayerIP";
            this.txtServiceLayerIP.PasswordChar = '\0';
            this.txtServiceLayerIP.SelectedText = "";
            this.txtServiceLayerIP.SelectionLength = 0;
            this.txtServiceLayerIP.SelectionStart = 0;
            this.txtServiceLayerIP.Size = new System.Drawing.Size(545, 46);
            this.txtServiceLayerIP.TabIndex = 60;
            this.txtServiceLayerIP.TabStop = false;
            this.txtServiceLayerIP.Tag = "Service Layer IP is required.";
            this.txtServiceLayerIP.UseSystemPasswordChar = false;
            // 
            // lblSQLHanaServerInstanceName
            // 
            this.lblSQLHanaServerInstanceName.AutoEllipsis = true;
            this.lblSQLHanaServerInstanceName.AutoSize = true;
            this.lblSQLHanaServerInstanceName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSQLHanaServerInstanceName.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblSQLHanaServerInstanceName.Location = new System.Drawing.Point(0, 0);
            this.lblSQLHanaServerInstanceName.Margin = new System.Windows.Forms.Padding(0);
            this.lblSQLHanaServerInstanceName.Name = "lblSQLHanaServerInstanceName";
            this.lblSQLHanaServerInstanceName.Size = new System.Drawing.Size(353, 31);
            this.lblSQLHanaServerInstanceName.TabIndex = 63;
            this.lblSQLHanaServerInstanceName.Text = "SQL Server Instance Name:";
            this.lblSQLHanaServerInstanceName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // ddlDatabaseType
            // 
            this.ddlDatabaseType.FormattingEnabled = true;
            this.ddlDatabaseType.Items.AddRange(new object[] {
            "Please Select"});
            this.ddlDatabaseType.Location = new System.Drawing.Point(381, 258);
            this.ddlDatabaseType.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.ddlDatabaseType.Name = "ddlDatabaseType";
            this.ddlDatabaseType.Size = new System.Drawing.Size(990, 39);
            this.ddlDatabaseType.TabIndex = 55;
            this.ddlDatabaseType.Tag = "Please select Database Type:";
            this.ddlDatabaseType.Text = "Please-Select";
            this.ddlDatabaseType.SelectedIndexChanged += new System.EventHandler(this.ddlDatabaseType_SelectedIndexChanged);
            // 
            // label47
            // 
            this.label47.AutoEllipsis = true;
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label47.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label47.Location = new System.Drawing.Point(111, 261);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(207, 31);
            this.label47.TabIndex = 54;
            this.label47.Text = "Database Type:";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoEllipsis = true;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(99, 104);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(397, 69);
            this.label17.TabIndex = 48;
            this.label17.Text = "Configuration";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label18
            // 
            this.label18.AutoEllipsis = true;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label18.Location = new System.Drawing.Point(109, 176);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(1031, 46);
            this.label18.TabIndex = 49;
            this.label18.Text = "Database Type, Service Layer IP, SQ /HANA credentials.";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep7
            // 
            this.pnlStep7.BackColor = System.Drawing.Color.White;
            this.pnlStep7.Controls.Add(this.tableLayoutPanel5);
            this.pnlStep7.Controls.Add(this.label20);
            this.pnlStep7.Controls.Add(this.label19);
            this.pnlStep7.Controls.Add(this.btnTestSSHConnection);
            this.pnlStep7.Controls.Add(this.label2);
            this.pnlStep7.Controls.Add(this.label22);
            this.pnlStep7.Location = new System.Drawing.Point(10, 48);
            this.pnlStep7.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlStep7.Name = "pnlStep7";
            this.pnlStep7.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep7.TabIndex = 6;
            this.pnlStep7.Text = "7";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.90329F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.09671F));
            this.tableLayoutPanel5.Controls.Add(this.label52, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtSSHRootPassword, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label46, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtSSHRootUserName, 0, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(135, 224);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.8607F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.13931F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1235, 170);
            this.tableLayoutPanel5.TabIndex = 63;
            // 
            // label52
            // 
            this.label52.AutoEllipsis = true;
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label52.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label52.Location = new System.Drawing.Point(5, 0);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(212, 31);
            this.label52.TabIndex = 48;
            this.label52.Text = "Root Username:";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSSHRootPassword
            // 
            this.txtSSHRootPassword.Depth = 0;
            this.txtSSHRootPassword.Hint = "";
            this.txtSSHRootPassword.Location = new System.Drawing.Point(626, 57);
            this.txtSSHRootPassword.Margin = new System.Windows.Forms.Padding(10);
            this.txtSSHRootPassword.MaxLength = 32767;
            this.txtSSHRootPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSSHRootPassword.Name = "txtSSHRootPassword";
            this.txtSSHRootPassword.PasswordChar = '*';
            this.txtSSHRootPassword.SelectedText = "";
            this.txtSSHRootPassword.SelectionLength = 0;
            this.txtSSHRootPassword.SelectionStart = 0;
            this.txtSSHRootPassword.Size = new System.Drawing.Size(599, 46);
            this.txtSSHRootPassword.TabIndex = 62;
            this.txtSSHRootPassword.TabStop = false;
            this.txtSSHRootPassword.Tag = "Password is required.";
            this.txtSSHRootPassword.UseSystemPasswordChar = false;
            // 
            // label46
            // 
            this.label46.AutoEllipsis = true;
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label46.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label46.Location = new System.Drawing.Point(621, 0);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(207, 31);
            this.label46.TabIndex = 50;
            this.label46.Text = "Root Password:";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSSHRootUserName
            // 
            this.txtSSHRootUserName.Depth = 0;
            this.txtSSHRootUserName.Hint = "";
            this.txtSSHRootUserName.Location = new System.Drawing.Point(10, 57);
            this.txtSSHRootUserName.Margin = new System.Windows.Forms.Padding(10);
            this.txtSSHRootUserName.MaxLength = 32767;
            this.txtSSHRootUserName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSSHRootUserName.Name = "txtSSHRootUserName";
            this.txtSSHRootUserName.PasswordChar = '\0';
            this.txtSSHRootUserName.SelectedText = "";
            this.txtSSHRootUserName.SelectionLength = 0;
            this.txtSSHRootUserName.SelectionStart = 0;
            this.txtSSHRootUserName.Size = new System.Drawing.Size(596, 46);
            this.txtSSHRootUserName.TabIndex = 61;
            this.txtSSHRootUserName.TabStop = false;
            this.txtSSHRootUserName.Tag = "Username is required.";
            this.txtSSHRootUserName.UseSystemPasswordChar = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(252, 623);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(1107, 31);
            this.label20.TabIndex = 54;
            this.label20.Text = "if SSH Connection is not established user will have to restart restart service la" +
    "yer manually.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(266, 575);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(1085, 31);
            this.label19.TabIndex = 53;
            this.label19.Text = "In case of HANA SSH connection is required to restart service layer at different " +
    "intervals, ";
            this.label19.UseWaitCursor = true;
            // 
            // btnTestSSHConnection
            // 
            this.btnTestSSHConnection.Location = new System.Drawing.Point(625, 435);
            this.btnTestSSHConnection.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnTestSSHConnection.Name = "btnTestSSHConnection";
            this.btnTestSSHConnection.Size = new System.Drawing.Size(251, 76);
            this.btnTestSSHConnection.TabIndex = 52;
            this.btnTestSSHConnection.Text = "Test Connection";
            this.btnTestSSHConnection.UseVisualStyleBackColor = true;
            this.btnTestSSHConnection.Click += new System.EventHandler(this.btnTestSSHConnection_Click);
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(105, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(397, 69);
            this.label2.TabIndex = 46;
            this.label2.Text = "Configuration";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label22
            // 
            this.label22.AutoEllipsis = true;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label22.Location = new System.Drawing.Point(127, 118);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(315, 46);
            this.label22.TabIndex = 47;
            this.label22.Text = "SSH Credentials";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep8
            // 
            this.pnlStep8.BackColor = System.Drawing.Color.White;
            this.pnlStep8.Controls.Add(this.tableLayoutPanel7);
            this.pnlStep8.Controls.Add(this.tableLayoutPanel6);
            this.pnlStep8.Controls.Add(this.txtDomainName);
            this.pnlStep8.Controls.Add(this.label28);
            this.pnlStep8.Controls.Add(this.label29);
            this.pnlStep8.Controls.Add(this.label30);
            this.pnlStep8.Location = new System.Drawing.Point(10, 48);
            this.pnlStep8.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlStep8.Name = "pnlStep8";
            this.pnlStep8.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep8.TabIndex = 7;
            this.pnlStep8.Text = "8";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.15028F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.18305F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel7.Controls.Add(this.ddlIPList, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label44, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.ddlProtocolType, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtPortNumber, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.label43, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label31, 2, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(135, 350);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1166, 135);
            this.tableLayoutPanel7.TabIndex = 74;
            // 
            // ddlIPList
            // 
            this.ddlIPList.FormattingEnabled = true;
            this.ddlIPList.Location = new System.Drawing.Point(244, 77);
            this.ddlIPList.Margin = new System.Windows.Forms.Padding(10);
            this.ddlIPList.Name = "ddlIPList";
            this.ddlIPList.Size = new System.Drawing.Size(546, 39);
            this.ddlIPList.TabIndex = 75;
            this.ddlIPList.Tag = "Please select IP Address";
            // 
            // label44
            // 
            this.label44.AutoEllipsis = true;
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label44.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label44.Location = new System.Drawing.Point(5, 0);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(83, 31);
            this.label44.TabIndex = 71;
            this.label44.Text = "Type:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label44.UseWaitCursor = true;
            // 
            // ddlProtocolType
            // 
            this.ddlProtocolType.FormattingEnabled = true;
            this.ddlProtocolType.Items.AddRange(new object[] {
            "http",
            "https"});
            this.ddlProtocolType.Location = new System.Drawing.Point(10, 77);
            this.ddlProtocolType.Margin = new System.Windows.Forms.Padding(10);
            this.ddlProtocolType.Name = "ddlProtocolType";
            this.ddlProtocolType.Size = new System.Drawing.Size(190, 39);
            this.ddlProtocolType.TabIndex = 72;
            this.ddlProtocolType.Tag = "Protocol Type is required.";
            // 
            // txtPortNumber
            // 
            this.txtPortNumber.Depth = 0;
            this.txtPortNumber.Hint = "";
            this.txtPortNumber.Location = new System.Drawing.Point(980, 77);
            this.txtPortNumber.Margin = new System.Windows.Forms.Padding(10);
            this.txtPortNumber.MaxLength = 32767;
            this.txtPortNumber.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPortNumber.Name = "txtPortNumber";
            this.txtPortNumber.PasswordChar = '\0';
            this.txtPortNumber.SelectedText = "";
            this.txtPortNumber.SelectionLength = 0;
            this.txtPortNumber.SelectionStart = 0;
            this.txtPortNumber.Size = new System.Drawing.Size(155, 46);
            this.txtPortNumber.TabIndex = 66;
            this.txtPortNumber.TabStop = false;
            this.txtPortNumber.Tag = "Port number is required.";
            this.txtPortNumber.Text = "80";
            this.txtPortNumber.UseSystemPasswordChar = false;
            // 
            // label43
            // 
            this.label43.AutoEllipsis = true;
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label43.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label43.Location = new System.Drawing.Point(239, 0);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(155, 31);
            this.label43.TabIndex = 69;
            this.label43.Text = "IP Address:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label43.UseWaitCursor = true;
            // 
            // label31
            // 
            this.label31.AutoEllipsis = true;
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label31.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label31.Location = new System.Drawing.Point(975, 0);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(175, 31);
            this.label31.TabIndex = 65;
            this.label31.Text = "Port Number:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label31.UseWaitCursor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel6.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtSiteName, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label41, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtApplicationPool, 1, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(135, 191);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.44118F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.55882F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1163, 136);
            this.tableLayoutPanel6.TabIndex = 73;
            // 
            // label27
            // 
            this.label27.AutoEllipsis = true;
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label27.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label27.Location = new System.Drawing.Point(5, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(148, 31);
            this.label27.TabIndex = 63;
            this.label27.Text = "Site Name:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label27.UseWaitCursor = true;
            // 
            // txtSiteName
            // 
            this.txtSiteName.Depth = 0;
            this.txtSiteName.Hint = "";
            this.txtSiteName.Location = new System.Drawing.Point(10, 65);
            this.txtSiteName.Margin = new System.Windows.Forms.Padding(10);
            this.txtSiteName.MaxLength = 32767;
            this.txtSiteName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSiteName.Name = "txtSiteName";
            this.txtSiteName.PasswordChar = '\0';
            this.txtSiteName.SelectedText = "";
            this.txtSiteName.SelectionLength = 0;
            this.txtSiteName.SelectionStart = 0;
            this.txtSiteName.Size = new System.Drawing.Size(561, 46);
            this.txtSiteName.TabIndex = 64;
            this.txtSiteName.TabStop = false;
            this.txtSiteName.Tag = "Site name is required.";
            this.txtSiteName.Text = "B1WebClient";
            this.txtSiteName.UseSystemPasswordChar = false;
            // 
            // label41
            // 
            this.label41.AutoEllipsis = true;
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label41.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label41.Location = new System.Drawing.Point(586, 0);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(295, 31);
            this.label41.TabIndex = 67;
            this.label41.Text = "Application Pool Name:";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label41.UseWaitCursor = true;
            // 
            // txtApplicationPool
            // 
            this.txtApplicationPool.Depth = 0;
            this.txtApplicationPool.Hint = "";
            this.txtApplicationPool.Location = new System.Drawing.Point(591, 65);
            this.txtApplicationPool.Margin = new System.Windows.Forms.Padding(10);
            this.txtApplicationPool.MaxLength = 32767;
            this.txtApplicationPool.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtApplicationPool.Name = "txtApplicationPool";
            this.txtApplicationPool.PasswordChar = '\0';
            this.txtApplicationPool.SelectedText = "";
            this.txtApplicationPool.SelectionLength = 0;
            this.txtApplicationPool.SelectionStart = 0;
            this.txtApplicationPool.Size = new System.Drawing.Size(562, 46);
            this.txtApplicationPool.TabIndex = 68;
            this.txtApplicationPool.TabStop = false;
            this.txtApplicationPool.Tag = "Application pool is required.";
            this.txtApplicationPool.Text = "B1WebClient";
            this.txtApplicationPool.UseSystemPasswordChar = false;
            // 
            // txtDomainName
            // 
            this.txtDomainName.Depth = 0;
            this.txtDomainName.Hint = "Example: myhost.example.com or localhost";
            this.txtDomainName.Location = new System.Drawing.Point(146, 567);
            this.txtDomainName.Margin = new System.Windows.Forms.Padding(10);
            this.txtDomainName.MaxLength = 32767;
            this.txtDomainName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDomainName.Name = "txtDomainName";
            this.txtDomainName.PasswordChar = '\0';
            this.txtDomainName.SelectedText = "";
            this.txtDomainName.SelectionLength = 0;
            this.txtDomainName.SelectionStart = 0;
            this.txtDomainName.Size = new System.Drawing.Size(912, 46);
            this.txtDomainName.TabIndex = 62;
            this.txtDomainName.TabStop = false;
            this.txtDomainName.Tag = "Domain name is required.";
            this.txtDomainName.UseSystemPasswordChar = false;
            // 
            // label28
            // 
            this.label28.AutoEllipsis = true;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(105, 31);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(397, 69);
            this.label28.TabIndex = 38;
            this.label28.Text = "Configuration";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label28.UseWaitCursor = true;
            // 
            // label29
            // 
            this.label29.AutoEllipsis = true;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label29.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label29.Location = new System.Drawing.Point(127, 121);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(366, 46);
            this.label29.TabIndex = 39;
            this.label29.Text = "IIS Settings Details.";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label29.UseWaitCursor = true;
            // 
            // label30
            // 
            this.label30.AutoEllipsis = true;
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label30.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label30.Location = new System.Drawing.Point(140, 526);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(726, 31);
            this.label30.TabIndex = 40;
            this.label30.Text = "Fully qualified domain name, please type without http/https:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label30.UseWaitCursor = true;
            // 
            // pnlStep9
            // 
            this.pnlStep9.BackColor = System.Drawing.Color.White;
            this.pnlStep9.Controls.Add(this.txtConfirmCertificatePassword);
            this.pnlStep9.Controls.Add(this.txtCertificatePassword);
            this.pnlStep9.Controls.Add(this.label36);
            this.pnlStep9.Controls.Add(this.label34);
            this.pnlStep9.Controls.Add(this.txtSSLCertificatePath);
            this.pnlStep9.Controls.Add(this.btnSSLCertificatePath);
            this.pnlStep9.Controls.Add(this.label24);
            this.pnlStep9.Controls.Add(this.label26);
            this.pnlStep9.Location = new System.Drawing.Point(10, 48);
            this.pnlStep9.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlStep9.Name = "pnlStep9";
            this.pnlStep9.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep9.TabIndex = 8;
            this.pnlStep9.Text = "9";
            // 
            // txtConfirmCertificatePassword
            // 
            this.txtConfirmCertificatePassword.Depth = 0;
            this.txtConfirmCertificatePassword.Hint = "";
            this.txtConfirmCertificatePassword.Location = new System.Drawing.Point(906, 404);
            this.txtConfirmCertificatePassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtConfirmCertificatePassword.MaxLength = 32767;
            this.txtConfirmCertificatePassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConfirmCertificatePassword.Name = "txtConfirmCertificatePassword";
            this.txtConfirmCertificatePassword.PasswordChar = '*';
            this.txtConfirmCertificatePassword.SelectedText = "";
            this.txtConfirmCertificatePassword.SelectionLength = 0;
            this.txtConfirmCertificatePassword.SelectionStart = 0;
            this.txtConfirmCertificatePassword.Size = new System.Drawing.Size(711, 46);
            this.txtConfirmCertificatePassword.TabIndex = 69;
            this.txtConfirmCertificatePassword.TabStop = false;
            this.txtConfirmCertificatePassword.Tag = "Confirm SSL Certficate Password  is required.";
            this.txtConfirmCertificatePassword.UseSystemPasswordChar = false;
            // 
            // txtCertificatePassword
            // 
            this.txtCertificatePassword.Depth = 0;
            this.txtCertificatePassword.Hint = "";
            this.txtCertificatePassword.Location = new System.Drawing.Point(111, 408);
            this.txtCertificatePassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtCertificatePassword.MaxLength = 32767;
            this.txtCertificatePassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCertificatePassword.Name = "txtCertificatePassword";
            this.txtCertificatePassword.PasswordChar = '*';
            this.txtCertificatePassword.SelectedText = "";
            this.txtCertificatePassword.SelectionLength = 0;
            this.txtCertificatePassword.SelectionStart = 0;
            this.txtCertificatePassword.Size = new System.Drawing.Size(711, 46);
            this.txtCertificatePassword.TabIndex = 68;
            this.txtCertificatePassword.TabStop = false;
            this.txtCertificatePassword.Tag = "SSL Certficate Password is required.";
            this.txtCertificatePassword.UseSystemPasswordChar = false;
            // 
            // label36
            // 
            this.label36.AutoEllipsis = true;
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label36.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label36.Location = new System.Drawing.Point(906, 362);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(244, 31);
            this.label36.TabIndex = 67;
            this.label36.Text = "Confirm Password:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label36.UseWaitCursor = true;
            // 
            // label34
            // 
            this.label34.AutoEllipsis = true;
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label34.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label34.Location = new System.Drawing.Point(111, 362);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(331, 31);
            this.label34.TabIndex = 66;
            this.label34.Text = "SSL Certificate Password:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label34.UseWaitCursor = true;
            // 
            // txtSSLCertificatePath
            // 
            this.txtSSLCertificatePath.Depth = 0;
            this.txtSSLCertificatePath.Hint = "";
            this.txtSSLCertificatePath.Location = new System.Drawing.Point(105, 229);
            this.txtSSLCertificatePath.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtSSLCertificatePath.MaxLength = 32767;
            this.txtSSLCertificatePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSSLCertificatePath.Name = "txtSSLCertificatePath";
            this.txtSSLCertificatePath.PasswordChar = '\0';
            this.txtSSLCertificatePath.SelectedText = "";
            this.txtSSLCertificatePath.SelectionLength = 0;
            this.txtSSLCertificatePath.SelectionStart = 0;
            this.txtSSLCertificatePath.Size = new System.Drawing.Size(878, 46);
            this.txtSSLCertificatePath.TabIndex = 63;
            this.txtSSLCertificatePath.TabStop = false;
            this.txtSSLCertificatePath.UseSystemPasswordChar = false;
            // 
            // btnSSLCertificatePath
            // 
            this.btnSSLCertificatePath.Location = new System.Drawing.Point(1195, 225);
            this.btnSSLCertificatePath.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnSSLCertificatePath.Name = "btnSSLCertificatePath";
            this.btnSSLCertificatePath.Size = new System.Drawing.Size(188, 54);
            this.btnSSLCertificatePath.TabIndex = 42;
            this.btnSSLCertificatePath.Text = "Browse...";
            this.btnSSLCertificatePath.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoEllipsis = true;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(105, 31);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(397, 69);
            this.label24.TabIndex = 39;
            this.label24.Text = "Configuration";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label26
            // 
            this.label26.AutoEllipsis = true;
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label26.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label26.Location = new System.Drawing.Point(105, 172);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(393, 31);
            this.label26.TabIndex = 40;
            this.label26.Text = "SSL Certificate Path (Optional):";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep11
            // 
            this.pnlStep11.AutoScroll = true;
            this.pnlStep11.BackColor = System.Drawing.Color.White;
            this.pnlStep11.Controls.Add(this.ucDatabaseInformation1);
            this.pnlStep11.Controls.Add(this.gvDatabaseInformation);
            this.pnlStep11.Controls.Add(this.label37);
            this.pnlStep11.Controls.Add(this.btnAddUCDatabaseInformation);
            this.pnlStep11.Controls.Add(this.label42);
            this.pnlStep11.Location = new System.Drawing.Point(10, 48);
            this.pnlStep11.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.pnlStep11.Name = "pnlStep11";
            this.pnlStep11.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep11.TabIndex = 10;
            this.pnlStep11.Text = "10";
            // 
            // gvDatabaseInformation
            // 
            this.gvDatabaseInformation.AllowUserToAddRows = false;
            this.gvDatabaseInformation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvDatabaseInformation.Location = new System.Drawing.Point(123, 689);
            this.gvDatabaseInformation.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.gvDatabaseInformation.Name = "gvDatabaseInformation";
            this.gvDatabaseInformation.RowHeadersVisible = false;
            this.gvDatabaseInformation.RowHeadersWidth = 102;
            this.gvDatabaseInformation.RowTemplate.Height = 40;
            this.gvDatabaseInformation.Size = new System.Drawing.Size(1241, 250);
            this.gvDatabaseInformation.TabIndex = 29;
            // 
            // label37
            // 
            this.label37.AutoEllipsis = true;
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(105, 31);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(397, 69);
            this.label37.TabIndex = 45;
            this.label37.Text = "Configuration";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnAddUCDatabaseInformation
            // 
            this.btnAddUCDatabaseInformation.Location = new System.Drawing.Point(632, 576);
            this.btnAddUCDatabaseInformation.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnAddUCDatabaseInformation.Name = "btnAddUCDatabaseInformation";
            this.btnAddUCDatabaseInformation.Size = new System.Drawing.Size(181, 85);
            this.btnAddUCDatabaseInformation.TabIndex = 47;
            this.btnAddUCDatabaseInformation.Text = "Add";
            this.btnAddUCDatabaseInformation.UseVisualStyleBackColor = true;
            this.btnAddUCDatabaseInformation.Click += new System.EventHandler(this.btnAddUCDatabaseInformation_Click);
            // 
            // label42
            // 
            this.label42.AutoEllipsis = true;
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label42.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label42.Location = new System.Drawing.Point(115, 148);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(317, 46);
            this.label42.TabIndex = 46;
            this.label42.Text = "SBO Credentials";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlStep12
            // 
            this.pnlStep12.Controls.Add(this.cbCleanUDs);
            this.pnlStep12.Controls.Add(this.cbUpdateConfiguration);
            this.pnlStep12.Controls.Add(this.cbUpdateIIS);
            this.pnlStep12.Controls.Add(this.cbUpdateSP);
            this.pnlStep12.Controls.Add(this.cbUpdateDatabase);
            this.pnlStep12.Controls.Add(this.cbCopyRelease);
            this.pnlStep12.Controls.Add(this.cblIncludedDatabases);
            this.pnlStep12.Controls.Add(this.lblDatabasesToBeUpdated);
            this.pnlStep12.Location = new System.Drawing.Point(10, 48);
            this.pnlStep12.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlStep12.Name = "pnlStep12";
            this.pnlStep12.Size = new System.Drawing.Size(1765, 979);
            this.pnlStep12.TabIndex = 11;
            this.pnlStep12.Text = "11";
            this.pnlStep12.UseVisualStyleBackColor = true;
            // 
            // cbCleanUDs
            // 
            this.cbCleanUDs.AutoSize = true;
            this.cbCleanUDs.Location = new System.Drawing.Point(120, 571);
            this.cbCleanUDs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbCleanUDs.Name = "cbCleanUDs";
            this.cbCleanUDs.Size = new System.Drawing.Size(184, 35);
            this.cbCleanUDs.TabIndex = 55;
            this.cbCleanUDs.Text = "Clean UDs";
            this.cbCleanUDs.UseVisualStyleBackColor = true;
            // 
            // cbUpdateConfiguration
            // 
            this.cbUpdateConfiguration.AutoSize = true;
            this.cbUpdateConfiguration.Checked = true;
            this.cbUpdateConfiguration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUpdateConfiguration.Location = new System.Drawing.Point(120, 863);
            this.cbUpdateConfiguration.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbUpdateConfiguration.Name = "cbUpdateConfiguration";
            this.cbUpdateConfiguration.Size = new System.Drawing.Size(309, 35);
            this.cbUpdateConfiguration.TabIndex = 54;
            this.cbUpdateConfiguration.Text = "Update Configuration";
            this.cbUpdateConfiguration.UseVisualStyleBackColor = true;
            // 
            // cbUpdateIIS
            // 
            this.cbUpdateIIS.AutoSize = true;
            this.cbUpdateIIS.Checked = true;
            this.cbUpdateIIS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUpdateIIS.Location = new System.Drawing.Point(123, 803);
            this.cbUpdateIIS.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbUpdateIIS.Name = "cbUpdateIIS";
            this.cbUpdateIIS.Size = new System.Drawing.Size(287, 35);
            this.cbUpdateIIS.TabIndex = 53;
            this.cbUpdateIIS.Text = "Update IIS Settings";
            this.cbUpdateIIS.UseVisualStyleBackColor = true;
            // 
            // cbUpdateSP
            // 
            this.cbUpdateSP.AutoSize = true;
            this.cbUpdateSP.Checked = true;
            this.cbUpdateSP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUpdateSP.Location = new System.Drawing.Point(123, 744);
            this.cbUpdateSP.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbUpdateSP.Name = "cbUpdateSP";
            this.cbUpdateSP.Size = new System.Drawing.Size(373, 35);
            this.cbUpdateSP.TabIndex = 52;
            this.cbUpdateSP.Text = "Update Stored Procedures";
            this.cbUpdateSP.UseVisualStyleBackColor = true;
            // 
            // cbUpdateDatabase
            // 
            this.cbUpdateDatabase.AutoSize = true;
            this.cbUpdateDatabase.Checked = true;
            this.cbUpdateDatabase.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUpdateDatabase.Location = new System.Drawing.Point(123, 685);
            this.cbUpdateDatabase.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbUpdateDatabase.Name = "cbUpdateDatabase";
            this.cbUpdateDatabase.Size = new System.Drawing.Size(296, 35);
            this.cbUpdateDatabase.TabIndex = 51;
            this.cbUpdateDatabase.Text = "Update Database(s)";
            this.cbUpdateDatabase.UseVisualStyleBackColor = true;
            // 
            // cbCopyRelease
            // 
            this.cbCopyRelease.AutoSize = true;
            this.cbCopyRelease.Checked = true;
            this.cbCopyRelease.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCopyRelease.Location = new System.Drawing.Point(123, 626);
            this.cbCopyRelease.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbCopyRelease.Name = "cbCopyRelease";
            this.cbCopyRelease.Size = new System.Drawing.Size(223, 35);
            this.cbCopyRelease.TabIndex = 50;
            this.cbCopyRelease.Text = "Copy Release";
            this.cbCopyRelease.UseVisualStyleBackColor = true;
            // 
            // cblIncludedDatabases
            // 
            this.cblIncludedDatabases.FormattingEnabled = true;
            this.cblIncludedDatabases.Location = new System.Drawing.Point(120, 123);
            this.cblIncludedDatabases.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.cblIncludedDatabases.Name = "cblIncludedDatabases";
            this.cblIncludedDatabases.Size = new System.Drawing.Size(459, 389);
            this.cblIncludedDatabases.TabIndex = 49;
            // 
            // lblDatabasesToBeUpdated
            // 
            this.lblDatabasesToBeUpdated.AutoEllipsis = true;
            this.lblDatabasesToBeUpdated.AutoSize = true;
            this.lblDatabasesToBeUpdated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabasesToBeUpdated.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDatabasesToBeUpdated.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblDatabasesToBeUpdated.Location = new System.Drawing.Point(117, 37);
            this.lblDatabasesToBeUpdated.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDatabasesToBeUpdated.Name = "lblDatabasesToBeUpdated";
            this.lblDatabasesToBeUpdated.Size = new System.Drawing.Size(509, 31);
            this.lblDatabasesToBeUpdated.TabIndex = 47;
            this.lblDatabasesToBeUpdated.Text = "Following Database(s) will be updated";
            this.lblDatabasesToBeUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlParent
            // 
            this.pnlParent.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlParent.Controls.Add(this.picLoader);
            this.pnlParent.Controls.Add(this.label50);
            this.pnlParent.Controls.Add(this.label25);
            this.pnlParent.Controls.Add(this.label49);
            this.pnlParent.Controls.Add(this.tabSelector);
            this.pnlParent.Controls.Add(this.label48);
            this.pnlParent.Controls.Add(this.wizardTab);
            this.pnlParent.Controls.Add(this.panel1);
            this.pnlParent.Controls.Add(this.pictureBox1);
            this.pnlParent.Controls.Add(this.label1);
            this.pnlParent.Location = new System.Drawing.Point(0, 70);
            this.pnlParent.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.pnlParent.Name = "pnlParent";
            this.pnlParent.Size = new System.Drawing.Size(3271, 1283);
            this.pnlParent.TabIndex = 1;
            // 
            // picLoader
            // 
            this.picLoader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picLoader.Image = global::B1WebInstaller.Properties.Resources.Spinner;
            this.picLoader.Location = new System.Drawing.Point(66, 849);
            this.picLoader.Margin = new System.Windows.Forms.Padding(21, 17, 21, 17);
            this.picLoader.Name = "picLoader";
            this.picLoader.Size = new System.Drawing.Size(348, 291);
            this.picLoader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picLoader.TabIndex = 50;
            this.picLoader.TabStop = false;
            this.picLoader.Visible = false;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(23, 1214);
            this.label25.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(436, 56);
            this.label25.TabIndex = 49;
            this.label25.Text = "© 2021 Init Consulting AG ";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.pg);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.lblCompanyName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(3271, 101);
            this.panel1.TabIndex = 43;
            // 
            // pg
            // 
            this.pg.BackColor = System.Drawing.Color.GreenYellow;
            this.pg.Depth = 0;
            this.pg.ForeColor = System.Drawing.Color.Red;
            this.pg.Location = new System.Drawing.Point(11, 93);
            this.pg.Margin = new System.Windows.Forms.Padding(5);
            this.pg.MouseState = MaterialSkin.MouseState.HOVER;
            this.pg.Name = "pg";
            this.pg.Size = new System.Drawing.Size(3248, 5);
            this.pg.TabIndex = 61;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEmail.Location = new System.Drawing.Point(1735, 36);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(0, 32);
            this.lblEmail.TabIndex = 44;
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblName.Location = new System.Drawing.Point(572, 36);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 32);
            this.lblName.TabIndex = 43;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCompanyName.Location = new System.Drawing.Point(1243, 36);
            this.lblCompanyName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(0, 32);
            this.lblCompanyName.TabIndex = 42;
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucDatabaseInformation1
            // 
            this.ucDatabaseInformation1.Location = new System.Drawing.Point(123, 230);
            this.ucDatabaseInformation1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ucDatabaseInformation1.Name = "ucDatabaseInformation1";
            this.ucDatabaseInformation1.showLocalization = true;
            this.ucDatabaseInformation1.Size = new System.Drawing.Size(1269, 306);
            this.ucDatabaseInformation1.TabIndex = 48;
            // 
            // frmB1Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2457, 1449);
            this.Controls.Add(this.pnlParent);
            this.Controls.Add(this.PanNavigation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.Name = "frmB1Setup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "B1Web Setup";
            this.TopMost = true;
            this.PanNavigation.ResumeLayout(false);
            this.PanNavigation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.wizardTab.ResumeLayout(false);
            this.pnlStep1.ResumeLayout(false);
            this.pnlStep1.PerformLayout();
            this.pnlStep2.ResumeLayout(false);
            this.pnlStep2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlStep3.ResumeLayout(false);
            this.pnlStep3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlStep4.ResumeLayout(false);
            this.pnlStep4.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.pnlStep5.ResumeLayout(false);
            this.pnlStep5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.pnlStep6.ResumeLayout(false);
            this.pnlStep6.PerformLayout();
            this.gbHanaSingleMultiContainer.ResumeLayout(false);
            this.gbHanaSingleMultiContainer.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pnlStep7.ResumeLayout(false);
            this.pnlStep7.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.pnlStep8.ResumeLayout(false);
            this.pnlStep8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.pnlStep9.ResumeLayout(false);
            this.pnlStep9.PerformLayout();
            this.pnlStep11.ResumeLayout(false);
            this.pnlStep11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDatabaseInformation)).EndInit();
            this.pnlStep12.ResumeLayout(false);
            this.pnlStep12.PerformLayout();
            this.pnlParent.ResumeLayout(false);
            this.pnlParent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoader)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog opd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblProgressText;
        private System.Windows.Forms.Panel PanNavigation;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private MaterialSkin.Controls.MaterialTabSelector tabSelector;
        private System.Windows.Forms.Panel pnlParent;
        private MaterialSkin.Controls.MaterialTabControl wizardTab;
        private System.Windows.Forms.TabPage pnlStep1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtInstructions;
        private System.Windows.Forms.TabPage pnlStep2;
        private System.Windows.Forms.TabPage pnlStep3;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtB1BackendFolderPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtB1FrontendFolderPath;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage pnlStep4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnGenerateLicenseKey;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage pnlStep5;
        private System.Windows.Forms.Label label15;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage pnlStep6;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox ddlDatabaseType;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage pnlStep7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnTestSSHConnection;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage pnlStep8;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage pnlStep9;
        private System.Windows.Forms.Button btnSSLCertificatePath;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage pnlStep11;
        private System.Windows.Forms.DataGridView gvDatabaseInformation;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnAddUCDatabaseInformation;
        private System.Windows.Forms.Label label42;
        private MaterialSkin.Controls.MaterialRaisedButton btnBack;
        //private ucDatabaseInformation ucDatabaseInformation1;
        private MaterialSkin.Controls.MaterialRaisedButton btnFinish;
        private MaterialSkin.Controls.MaterialRaisedButton btnCancel;
        private MaterialSkin.Controls.MaterialRaisedButton btnNext;
        private System.Windows.Forms.Button btnBrowseFrontendPath;
        private System.Windows.Forms.Button btnBrowseBackendPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtInstallationNumber;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtB1License;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtUserNameEditable;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPhoneNo;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtEmailEditable;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCompanyNameEditable;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtUserManagementPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtConfirmUserManagementPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtHANAUserPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtHANAUser;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSSHRootUserName;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSSHRootPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDomainName;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSSLCertificatePath;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtLicenseServerPort;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDatabaseEnginePort;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtXSEnginePort;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage pnlStep12;
        private System.Windows.Forms.Label lblSQLHanaServerInstanceName;
        private MaterialSkin.Controls.MaterialRadioButton rbDatabaseTypeHana;
        private System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialRadioButton rbDatabaseTypeSQL;
        private System.Windows.Forms.GroupBox pnlSystemCompatibility;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSQLHanaServerInstanceName;
        private System.Windows.Forms.Label lblSystemCompatibilityError;
        private System.Windows.Forms.PictureBox picLoader;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtApplicationPool;
        private System.Windows.Forms.Label label41;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPortNumber;
        private System.Windows.Forms.Label label31;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSiteName;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckedListBox cblIncludedDatabases;
        private System.Windows.Forms.Label lblDatabasesToBeUpdated;
        private System.Windows.Forms.ComboBox ddlProtocolType;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox cbCopyRelease;
        private System.Windows.Forms.CheckBox cbUpdateDatabase;
        private System.Windows.Forms.CheckBox cbUpdateSP;
        private System.Windows.Forms.CheckBox cbUpdateIIS;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialProgressBar pg;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label45;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtServiceLayerIPPort;
        private System.Windows.Forms.Label label53;
        private ucDatabaseInformation ucDatabaseInformation1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtConfirmCertificatePassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCertificatePassword;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox ddlIPList;
        private System.Windows.Forms.CheckBox cbUpdateConfiguration;
        private System.Windows.Forms.CheckBox cbCleanUDs;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtServiceLayerIP;
        private System.Windows.Forms.GroupBox gbHanaSingleMultiContainer;
        private MaterialSkin.Controls.MaterialRadioButton rbMultiContainer;
        private MaterialSkin.Controls.MaterialRadioButton rbSingleContainer;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtTenantDB;
    }
}

