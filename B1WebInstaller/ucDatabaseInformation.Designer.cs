﻿namespace B1WebInstaller
{
    partial class ucDatabaseInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lblLocalization = new System.Windows.Forms.Label();
            this.cbLocalization = new System.Windows.Forms.ComboBox();
            this.txtDatabaseNames = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSBOPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSBOUserName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label45
            // 
            this.label45.AutoEllipsis = true;
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label45.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label45.Location = new System.Drawing.Point(614, 0);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(214, 32);
            this.label45.TabIndex = 33;
            this.label45.Text = "SBO Password:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label43
            // 
            this.label43.AutoEllipsis = true;
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label43.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label43.Location = new System.Drawing.Point(5, 0);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(220, 32);
            this.label43.TabIndex = 31;
            this.label43.Text = "SBO Username:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.AutoEllipsis = true;
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label40.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label40.Location = new System.Drawing.Point(5, 136);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(219, 32);
            this.label40.TabIndex = 35;
            this.label40.Text = "Database Name";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocalization
            // 
            this.lblLocalization.AutoEllipsis = true;
            this.lblLocalization.AutoSize = true;
            this.lblLocalization.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLocalization.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblLocalization.Location = new System.Drawing.Point(614, 136);
            this.lblLocalization.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblLocalization.Name = "lblLocalization";
            this.lblLocalization.Size = new System.Drawing.Size(168, 32);
            this.lblLocalization.TabIndex = 37;
            this.lblLocalization.Text = "Localization";
            this.lblLocalization.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbLocalization
            // 
            this.cbLocalization.FormattingEnabled = true;
            this.cbLocalization.Location = new System.Drawing.Point(612, 183);
            this.cbLocalization.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbLocalization.Name = "cbLocalization";
            this.cbLocalization.Size = new System.Drawing.Size(556, 39);
            this.cbLocalization.TabIndex = 38;
            // 
            // txtDatabaseNames
            // 
            this.txtDatabaseNames.Depth = 0;
            this.txtDatabaseNames.Hint = "";
            this.txtDatabaseNames.Location = new System.Drawing.Point(3, 183);
            this.txtDatabaseNames.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDatabaseNames.MaxLength = 32767;
            this.txtDatabaseNames.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDatabaseNames.Name = "txtDatabaseNames";
            this.txtDatabaseNames.PasswordChar = '\0';
            this.txtDatabaseNames.SelectedText = "";
            this.txtDatabaseNames.SelectionLength = 0;
            this.txtDatabaseNames.SelectionStart = 0;
            this.txtDatabaseNames.Size = new System.Drawing.Size(603, 46);
            this.txtDatabaseNames.TabIndex = 41;
            this.txtDatabaseNames.TabStop = false;
            this.txtDatabaseNames.Tag = "Database Name is required.";
            this.txtDatabaseNames.UseSystemPasswordChar = false;
            // 
            // txtSBOPassword
            // 
            this.txtSBOPassword.Depth = 0;
            this.txtSBOPassword.Hint = "";
            this.txtSBOPassword.Location = new System.Drawing.Point(612, 46);
            this.txtSBOPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSBOPassword.MaxLength = 32767;
            this.txtSBOPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSBOPassword.Name = "txtSBOPassword";
            this.txtSBOPassword.PasswordChar = '*';
            this.txtSBOPassword.SelectedText = "";
            this.txtSBOPassword.SelectionLength = 0;
            this.txtSBOPassword.SelectionStart = 0;
            this.txtSBOPassword.Size = new System.Drawing.Size(556, 46);
            this.txtSBOPassword.TabIndex = 40;
            this.txtSBOPassword.TabStop = false;
            this.txtSBOPassword.Tag = "SBO Password is required.";
            this.txtSBOPassword.UseSystemPasswordChar = false;
            // 
            // txtSBOUserName
            // 
            this.txtSBOUserName.Depth = 0;
            this.txtSBOUserName.Hint = "";
            this.txtSBOUserName.Location = new System.Drawing.Point(3, 46);
            this.txtSBOUserName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSBOUserName.MaxLength = 32767;
            this.txtSBOUserName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSBOUserName.Name = "txtSBOUserName";
            this.txtSBOUserName.PasswordChar = '\0';
            this.txtSBOUserName.SelectedText = "";
            this.txtSBOUserName.SelectionLength = 0;
            this.txtSBOUserName.SelectionStart = 0;
            this.txtSBOUserName.Size = new System.Drawing.Size(556, 46);
            this.txtSBOUserName.TabIndex = 39;
            this.txtSBOUserName.TabStop = false;
            this.txtSBOUserName.Tag = "SBO Username is required.";
            this.txtSBOUserName.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label43, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbLocalization, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtDatabaseNames, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtSBOUserName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLocalization, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label45, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtSBOPassword, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label40, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(23, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.5109F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.94834F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.60517F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.71028F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1218, 271);
            this.tableLayoutPanel1.TabIndex = 43;
            // 
            // ucDatabaseInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ucDatabaseInformation";
            this.Size = new System.Drawing.Size(1278, 341);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblLocalization;
        private System.Windows.Forms.ComboBox cbLocalization;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSBOUserName;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSBOPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDatabaseNames;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
