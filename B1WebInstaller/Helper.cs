﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Resources;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.IO.Compression;
using System.Collections;

namespace B1WebInstaller
{
    public class Helper
    {
        public static string GetDotNetFrameworkVersion()
        {

            return string.Empty;
        }

        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            try
            {
                // Get the subdirectories for the specified directory.
                DirectoryInfo dir = new DirectoryInfo(sourceDirName);

                if (!dir.Exists)
                {
                    throw new DirectoryNotFoundException(
                        "Source directory does not exist or could not be found: "
                        + sourceDirName);
                }

                DirectoryInfo[] dirs = dir.GetDirectories();
                // If the destination directory doesn't exist, create it.
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, true);
                }

                // If copying subdirectories, copy them and their contents to new location.
                if (copySubDirs)
                {
                    foreach (DirectoryInfo subdir in dirs)
                    {
                        string temppath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage(" DirectoryCopy() " + ex.Message);
                throw;
            }
        }

        public static string GetProjectDirectoryPath()
        {

            string workingDirectory = Environment.CurrentDirectory;

            string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;

            return projectDirectory;
        }

        public static string GetExeDirectoryPath()
        {

            string startupPath = System.IO.Directory.GetCurrentDirectory();

            return startupPath;
        }

        public static void UpdateConfigurationFile(string path, string key, string valueToUpate)
        {
            try
            {

                var configFile = new FileInfo(path);
                var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
                var wcfm = new WebConfigurationFileMap();
                wcfm.VirtualDirectories.Add("/", vdm);
                var configuration = WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");

                configuration.AppSettings.Settings[key].Value = valueToUpate;
                configuration.Save();
            }
            catch (Exception ex)
            {
                var message = string.Format(" UpdateConfigurationFile() unable to update the key {0}, Error:{1}", key, ex.Message);
                LogMessage(message);
            }

        }

        public static string ReadConfigurationKey(string path,string key)
        {

            if (File.Exists(path))
            {
                ExeConfigurationFileMap map = new ExeConfigurationFileMap();
                map.ExeConfigFilename = path;

                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

                string val = config.AppSettings.Settings[key].Value;

                 return val;
            }

            return string.Empty;
        }

        public static string GenerateRandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ReadJSONFile(string path)
        {

            JObject o1 = JObject.Parse(File.ReadAllText(path));

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(path))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
            }

            return string.Empty;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public static void ExtractZip(string _zipPath,string _tempPath, string resouceFileName)
        {
            try
            {
                //write the resource zip file to the temp directory //
                using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resouceFileName))
                {
                    if (File.Exists(_zipPath))
                        File.Delete(_zipPath);

                    using (FileStream bw = new FileStream(_zipPath, FileMode.Create))
                    {
                        //read until we reach the end of the file
                        while (stream.Position < stream.Length)
                        {
                            //byte array to hold file bytes
                            byte[] bits = new byte[stream.Length];
                            //read in the bytes
                            stream.Read(bits, 0, (int)stream.Length);
                            //write out the bytes
                            bw.Write(bits, 0, (int)stream.Length);
                        }
                    }
                    stream.Close();
                }

                ZipFile.ExtractToDirectory(_zipPath, _tempPath);
                //extract the contents of the file we created
                //UnzipFile(_zipPath, _tempPath);
                //or
                //System.IO.Compression.ZipFile.ExtractToDirectory(_zipPath, _tempPath);

            }
            catch (Exception ex)
            {
                Helper.LogMessage(" ExtractZip() " + ex.Message);
                
                //handle the error
            }
        }

        public static void UnzipFile(string zipPath, string folderPath)
        {
            try
            {
                if (!File.Exists(zipPath))
                {
                    throw new FileNotFoundException();
                }
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                
                Shell32.Shell objShell = new Shell32.Shell();
                Shell32.Folder destinationFolder = objShell.NameSpace(folderPath);
                Shell32.Folder sourceFile = objShell.NameSpace(zipPath);
                foreach (var file in sourceFile.Items())
                {
                    destinationFolder.CopyHere(file, 4 | 16);
                }
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" UnzipFile() " + ex.Message);
                //handle error
            }
        }

        public static void RemoveFile(string path)
        {

            if (File.Exists(path))
                File.Delete(path);
        }

        public static bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool ValidatePhoneNumber(string phone, bool IsRequired)
        {
            if (string.IsNullOrEmpty(phone) & !IsRequired)
                return true;

            if (string.IsNullOrEmpty(phone) & IsRequired)
                return false;

            var cleaned = RemoveNonNumeric(phone);
            if (IsRequired)
            {
                if (cleaned.Length >= 10)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (cleaned.Length == 0)
                    return true;
                else if (cleaned.Length > 0 & cleaned.Length < 10)
                    return false;
                else if (cleaned.Length == 10)
                    return true;
                else
                    return false; // should never get here
            }
        }

        /// <summary>
        /// Removes all non numeric characters from a string
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string RemoveNonNumeric(string phone)
        {
            return Regex.Replace(phone, @"[^0-9]+", "");
        }
        public static bool SendEmail() {

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                var username = "asadmateenkhan@gmail.com";
                var password = "";

                mail.From = new MailAddress("Asad.Khan@init-consulting.de");
                mail.To.Add("Asad.Khan@init-consulting.de");
                mail.Subject = "Test Mail";
                mail.Body = "This is for testing SMTP mail from GMAIL";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(username, password);
                SmtpServer.EnableSsl = true;
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Send(mail);

                return true;
                
            }
            catch (Exception ex)
            {
                Helper.LogMessage(" SendEmail() " + ex.Message);
                return false;
            }
        }
        public static void EmptyFolder(string path)
        {
            string strpath = path;
            if (Directory.Exists(strpath))
            {
                RemoveDirectories(strpath);
            }
        }
        public static string EncryptStringAES(string plainText)
        {
            var keybytes = Encoding.UTF8.GetBytes("02CE4A849C034F99");
            var iv = Encoding.UTF8.GetBytes("BDBA9D50AC5694B8");

            var encryptedBytes = EncryptStringToBytes(plainText, keybytes, iv);
            return Convert.ToBase64String(encryptedBytes);
        }

        public static string DecryptStringAES(string cipherText)
        {
            var keybytes = Encoding.UTF8.GetBytes("02CE4A849C034F99");
            var iv = Encoding.UTF8.GetBytes("BDBA9D50AC5694B8");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }

        public static void EditJSONFile(string filePath, string key, string value)
        {

            string json = File.ReadAllText(filePath);
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            jsonObj[key] = value;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(filePath, output);
        }

        public static string ReadJSONKey(string filePath, string key)
        {

            string json = File.ReadAllText(filePath);
            dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            var str = Convert.ToString(jsonObj[key]);
            return str;
        }

        public static void LogMessage(string message)
        {

            var filePath = Path.Combine(Constants.SystemConstants.LOG_DIR_PATH, Constants.SystemConstants.LOG_FILE_NAME);

            if (!File.Exists(filePath))
            {
                DirectoryInfo di = Directory.CreateDirectory(Constants.SystemConstants.LOG_DIR_PATH);
            }

            using (StreamWriter w = File.AppendText(filePath))
            {
                Log(message, w);
            }

        }
        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.  
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.  
            return encrypted;
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }
        private static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.Write("\n");
            w.WriteLine($"{logMessage}");
            w.WriteLine("-------------------------------");
        }
      
        private static void RemoveDirectories(string strpath)
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
            {
                File.Delete(file);
            }
            //This condition is used to check all child Directories and delete files
            foreach (string subfolder in Directory.GetDirectories(strpath))
            {
                RemoveDirectories(subfolder);
            }
            Directory.Delete(strpath);
        }

        public static IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        public static bool CheckSMTPConnection()
        {
            bool isValid = false;

            try
            {
                using (var client = new TcpClient())
                {
                    var server = "smtp.gmail.com";
                    var port = 465;
                    client.Connect(server, port);
                    // As GMail requires SSL we should use SslStream
                    // If your SMTP server doesn't support SSL you can
                    // work directly with the underlying stream
                    using (var stream = client.GetStream())
                    using (var sslStream = new SslStream(stream))
                    {
                        sslStream.AuthenticateAsClient(server);
                        using (var writer = new StreamWriter(sslStream))
                        using (var reader = new StreamReader(sslStream))
                        {
                            writer.WriteLine("EHLO " + server);
                            writer.Flush();
                            Console.WriteLine(reader.ReadLine());
                            return true;
                            // GMail responds with: 220 mx.google.com ESMTP
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return isValid;
            }
        }

        public static void GiveFolderPermission(string accountName, string folderPath)
        {
            try
            {

                FileSystemRights Rights;

                //What rights are we setting? Here accountName is == "IIS_IUSRS"

                Rights = FileSystemRights.FullControl;
                bool modified;
                var none = new InheritanceFlags();
                none = InheritanceFlags.None;

                //set on dir itself
                var accessRule = new FileSystemAccessRule(accountName, Rights, none, PropagationFlags.NoPropagateInherit, AccessControlType.Allow);
                var dInfo = new DirectoryInfo(folderPath);
                var dSecurity = dInfo.GetAccessControl();
                dSecurity.ModifyAccessRule(AccessControlModification.Set, accessRule, out modified);

                //Always allow objects to inherit on a directory 
                var iFlags = new InheritanceFlags();
                iFlags = InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit;

                //Add Access rule for the inheritance
                var accessRule2 = new FileSystemAccessRule(accountName, Rights, iFlags, PropagationFlags.InheritOnly, AccessControlType.Allow);
                dSecurity.ModifyAccessRule(AccessControlModification.Add, accessRule2, out modified);

                dInfo.SetAccessControl(dSecurity);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:GiveFolderPermission()" + ex.Message);
            }
        }


        public static bool AreObjectsEqual(object objectA, object objectB, params string[] ignoreList)
        {
            try
            {
                bool result;

                if (objectA != null && objectB != null)
                {
                    Type objectType;

                    objectType = objectA.GetType();

                    result = true; // assume by default they are equal

                    foreach (PropertyInfo propertyInfo in objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && !ignoreList.Contains(p.Name)))
                    {
                        object valueA;
                        object valueB;

                        valueA = propertyInfo.GetValue(objectA, null);
                        valueB = propertyInfo.GetValue(objectB, null);

                        // if it is a primitive type, value type or implements IComparable, just directly try and compare the value
                        if (CanDirectlyCompare(propertyInfo.PropertyType))
                        {
                            if (!AreValuesEqual(valueA, valueB))
                            {
                                Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                                result = false;
                            }
                        }
                        // if it implements IEnumerable, then scan any items
                        else if (typeof(IEnumerable).IsAssignableFrom(propertyInfo.PropertyType))
                        {
                            IEnumerable<object> collectionItems1;
                            IEnumerable<object> collectionItems2;
                            int collectionItemsCount1;
                            int collectionItemsCount2;

                            // null check
                            if (valueA == null && valueB != null || valueA != null && valueB == null)
                            {
                                Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                                result = false;
                            }
                            else if (valueA != null && valueB != null)
                            {
                                collectionItems1 = ((IEnumerable)valueA).Cast<object>();
                                collectionItems2 = ((IEnumerable)valueB).Cast<object>();
                                collectionItemsCount1 = collectionItems1.Count();
                                collectionItemsCount2 = collectionItems2.Count();

                                // check the counts to ensure they match
                                if (collectionItemsCount1 != collectionItemsCount2)
                                {
                                    Console.WriteLine("Collection counts for property '{0}.{1}' do not match.", objectType.FullName, propertyInfo.Name);
                                    result = false;
                                }
                                // and if they do, compare each item... this assumes both collections have the same order
                                else
                                {
                                    for (int i = 0; i < collectionItemsCount1; i++)
                                    {
                                        object collectionItem1;
                                        object collectionItem2;
                                        Type collectionItemType;

                                        collectionItem1 = collectionItems1.ElementAt(i);
                                        collectionItem2 = collectionItems2.ElementAt(i);
                                        collectionItemType = collectionItem1.GetType();

                                        if (CanDirectlyCompare(collectionItemType))
                                        {
                                            if (!AreValuesEqual(collectionItem1, collectionItem2))
                                            {
                                                Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.", i, objectType.FullName, propertyInfo.Name);
                                                result = false;
                                            }
                                        }
                                        else if (!AreObjectsEqual(collectionItem1, collectionItem2, ignoreList))
                                        {
                                            Console.WriteLine("Item {0} in property collection '{1}.{2}' does not match.", i, objectType.FullName, propertyInfo.Name);
                                            result = false;
                                        }
                                    }
                                }
                            }
                        }
                        else if (propertyInfo.PropertyType.IsClass)
                        {
                            if (!AreObjectsEqual(propertyInfo.GetValue(objectA, null), propertyInfo.GetValue(objectB, null), ignoreList))
                            {
                                Console.WriteLine("Mismatch with property '{0}.{1}' found.", objectType.FullName, propertyInfo.Name);
                                result = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Cannot compare property '{0}.{1}'.", objectType.FullName, propertyInfo.Name);
                            result = false;
                        }
                    }
                }
                else
                    result = object.Equals(objectA, objectB);

                return result;
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Determines whether value instances of the specified type can be directly compared.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if this value instances of the specified type can be directly compared; otherwise, <c>false</c>.
        /// </returns>
        private static bool CanDirectlyCompare(Type type)
        {
            return typeof(IComparable).IsAssignableFrom(type) || type.IsPrimitive || type.IsValueType;
        }

        /// <summary>
        /// Compares two values and returns if they are the same.
        /// </summary>
        /// <param name="valueA">The first value to compare.</param>
        /// <param name="valueB">The second value to compare.</param>
        /// <returns><c>true</c> if both values match, otherwise <c>false</c>.</returns>
        private static bool AreValuesEqual(object valueA, object valueB)
        {
            bool result;
            IComparable selfValueComparer;

            selfValueComparer = valueA as IComparable;

            if (valueA == null && valueB != null || valueA != null && valueB == null)
                result = false; // one of the values is null
            else if (selfValueComparer != null && selfValueComparer.CompareTo(valueB) != 0)
                result = false; // the comparison using IComparable failed
            else if (!object.Equals(valueA, valueB))
                result = false; // the comparison using Equals failed
            else
                result = true; // match

            return result;
        }

    }

}

