
Prerequisites for Installation
	1. Windows Server Version 2012 R2 or higher required or Windows 10 or higher
	2.  IIS Version 8.5 or higher
	3.  IIS URL Rewrite Version 2.1 or higher
	4.  DOT NET Framework version 4.6.2 or higher
	5.  Secure Shell (SSH) connection should be enabled on server where 
		Service Layer is installed and client machine should be able to access 
		server using SSH.
	6. For Windows Server: 
		1. Open „Server-Manager“
		2. Click „Manage“ —> „Add Roles and Features“ —> „Server Roles“
		3. Under „Web Server (IIS)“ check all checkboxes
	7. For Windows 10:
		1. Open „Programs and Features“
		2. Click „Turn Windows features on or off“
		3. Under „Internet Information Services“ check all checkboxes


—————————————————————————————


Installation Manual
	1. Enter your name, company name, Phone number as well as a correct and 		valid eMail address as License 	Key will be send to said address.
	
	2. Select Destination Location
		- You can use the given folder paths
		- Or choose your own
		—> But be aware that frontend and backend need to be installed in 
		different folders!

	Configuration
	3. Installation Number: You find this number in your SAP Business One Fat Client  		—> 	Help 	—> 	About SAP Business One

	4. The License Key will be then send to the eMail address you entered before
		- Copy & Paste said License Key from your eMail account into the Input 
		field provided below

	5. User management password: You can choose which password to set
	(This password will be of use when adding Licenses for the Web client)
	
	6. Service Layer/Database configuration
	- Database Type: HANA/SQL
	- Service Layer (= HANA Server | SQL Server) IP: Server-IP where Service Layer 
		is running (ex. https://192.50.154.115)
	- HANA/SQL User: Username for HANA Studio/SQL Studio
	- HANA/SQL Password: Password for HANA Studio/SQL Studio
	
	Advanced settings:
	- Service Layer Port: (ex. 50000)
	- XSEngine Port: (ex. 4300)
	- Database Engine Port: (ex. 30015)
	- License Server Port: (ex. 40000)

	Info: Note that these are default ports. If they are different on your running 
	System you can adapt them here accordingly

	7.  Enter SSH credentials
			—> default: 	Root Username: root
					Root Password: (System password for HANA)
	
	8. Fully qualified domain name: URL for the Web client

	Info: You can skip this step but note that you need to enter the domain name in 
	the web.config 	afterwards manually!

	9. SSL Certificate Path: Not mandatory
		—> If available enter password accordingly	
	10. SAP Business One credentials
	- Input SAP Business One Username, Password and the Database you want to 
	use
	- Localisation: Uses taxation and other settings for said country


—————————————————————————————

Post Installation Manual
	1. Open (IIS)-Manager
	2. Right click on „Sites“
		—> Add Website
	3. Site name is chosen freely
	4. Physical Path: Enter the path for the Backend (ex.: C:\WEBUI\Backend)
	5. Choose Type: https/http
		Note: You can only use https if you provided the necessary information in the Installation 		process!
	6. Enter IP address of running machine
	7. Enter the Hostname URL in which the Web client will be running
		Scenarios
		1. Ideal: 
			—> Enter same URL as in the Installation process
		2. No URL was provided in Installation process: 
			—> Input a fully qualified domain name
			Note: Enter same URL in web.config 
			default path: C:\B1Web_Installer\B1 Web\Backend\web.config
		3. A different URL was provided in Installation process:
			—> Input a fully qualified domain name
			Note: Enter same URL in web.config 
			default path: C:\B1Web_Installer\B1 Web\Backend\web.config
	8. Choose SSL certificate if available
	9. Press OK button
	10. Right click on created Website and choose: Add virtual directory…
		- Alias: assets
		- Physical path (default path): C:\WEBUI\Frontend\assets
		- Press OK button

	11. Right click on created Website and choose: Add virtual directory…
		- Alias: ui
		- Physical path (default path): C:\WEBUI\Frontend 
		- Press OK button

			









