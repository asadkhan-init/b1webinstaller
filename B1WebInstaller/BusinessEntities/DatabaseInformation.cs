﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class DatabaseInformation
    {
        public string SBOUserName { get; set; }
        public string SBOPassword { get; set; }
        public string DatabaseName { get; set; }

        public string Localization { get; set; }
    }
}
