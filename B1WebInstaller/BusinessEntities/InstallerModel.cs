﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class InstallerModel
    {

        public string BackendPath { get; set; }
        public string FrontEndPath { get; set; }
        public string InstallationNumber { get; set; }
        public string LicenseKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCompany { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhoneNo { get; set; }
        public string UserManagementPassword { get; set; }
        public string DatabaseType { get; set; }
        public string ServiceLayerIP { get; set; }
        public string HanaSQLUserName { get; set; }
        public string HanaSQLPassword { get; set; }
        public string ServiceLayerPort { get; set; }
        public string DatabaseEnginePort { get; set; }
        public string LicenseServerPort { get; set; }
        public string XSEnginePort { get; set; }
        public bool isMultiContainer { get; set; }
        public string TenantDB { get; set; }
        public string SSHUserName { get; set; }
        public string SSHPassword { get; set; }
        public string SSLPath { get; set; }
        public string SSLPassword { get; set; }
        public string ServiceLayerUrl { get; set; }
        public string InstallationType { get; set; }
        public string SQLInstanceName { get; set; }
        public string B1WebClientVersion { get; set; }
        public List<SBOUserModel> SBOModelList { get; set; }

        public SiteInformation SiteInfo { get; set; }

        public InstallerModel()
        {

            SBOModelList = new List<SBOUserModel>();
            SiteInfo = new SiteInformation();
        }

    }
}
