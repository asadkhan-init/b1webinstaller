﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class ConnectionStatus
    {

        public string Type { get; set; }
        public string Message { get; set; }
        public bool State { get; set; }
    }
}
