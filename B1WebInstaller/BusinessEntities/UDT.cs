﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class UDT
    {

        public string TableName { get; set; }
        public string TableDescription { get; set; }
        public string TableType { get; set; }
        public string Archivable { get; set; }
        public string ArchiveDateField { get; set; }

    }
}
