﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class SiteInformation
    {
        public string SiteName { get; set; }
        public string Port { get; set; }
        public string ApplicationPoolName { get; set; }
        public string IPAddress { get; set; }
        public string DomainName { get; set; }
        public string ProtocolType { get;set; }
    }
}
