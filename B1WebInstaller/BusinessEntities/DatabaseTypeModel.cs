﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class DatabaseTypeModel
    {
        public string serverName { get; set; }
        public string serverKey { get; set; }
        public string serverInstanceName { get; set; }
        public string modelKey
        {
            get
            {
                return serverKey + "$" + serverInstanceName;
            }
        }
    }
}
