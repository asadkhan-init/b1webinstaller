﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class LocalizationModel
    {
        public string LanguageShortName { get; set; }
        public string LanguageFullName { get; set; }
       
    }
}
