﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class ValidValuesMD
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }

    public class UDF
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int? Size { get; set; }
        public string Description { get; set; }
        public string SubType { get; set; }
        public object LinkedTable { get; set; }
        public object DefaultValue { get; set; }
        public string TableName { get; set; }
        public int? FieldID { get; set; }
        public int? EditSize { get; set; }
        public string Mandatory { get; set; }
        public object LinkedUDO { get; set; }
        public object LinkedSystemObject { get; set; }
        public List<ValidValuesMD> ValidValuesMD { get; set; }
    }
}
