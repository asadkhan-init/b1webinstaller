﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class UDTMetaData
    {
        public List<UDT> Result { get; set; }
        public string NextPage { get; set; }
    }
}
