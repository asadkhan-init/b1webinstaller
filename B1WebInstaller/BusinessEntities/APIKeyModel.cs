﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class APIKeyModel
    {
        public string APIKEY { get; set; }
        public string InstallationNumber { get; set; }
        public string ValidTo { get; set; }
        public string AnzUser { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }


    }
}
