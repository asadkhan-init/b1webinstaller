﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller.BusinessEntities
{
    public class SBOUserModel
    {
       // public string serviceLayerUrl { get; set; }
        //public string serviceLayerPort { get; set; }
        public string sboUserName { get; set; }
        public string sboPassword { get; set; }
        public string database { get; set; }
        public string localization { get; set; }
       
    }
}
