﻿namespace B1WebInstaller
{
    partial class frmMetaData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlDatabaseType = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ucDatabaseInformation1 = new B1WebInstaller.ucDatabaseInformation();
            this.txtServiceLayerIP = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtServiceLayerIPPort = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnLoadTables = new System.Windows.Forms.Button();
            this.dgTablesList = new System.Windows.Forms.DataGridView();
            this.cbSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TableJson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dgFieldsList = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgObjectList = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLoadFields = new System.Windows.Forms.Button();
            this.btnLoadObjects = new System.Windows.Forms.Button();
            this.btnExportObjectMetaData = new System.Windows.Forms.Button();
            this.btnExportFieldMetaData = new System.Windows.Forms.Button();
            this.btnExportTableMetaData = new System.Windows.Forms.Button();
            this.btnExportObjectData = new System.Windows.Forms.Button();
            this.btnUDODataBrowsePath = new System.Windows.Forms.Button();
            this.txtMetaDataPath = new System.Windows.Forms.TextBox();
            this.cbTableSelectAll = new System.Windows.Forms.CheckBox();
            this.cbFieldsSelectAll = new System.Windows.Forms.CheckBox();
            this.cbObjectsSelectAll = new System.Windows.Forms.CheckBox();
            this.txtFilterTable = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFilterFields = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFilterObjects = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTotalTableCount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblTotalFieldsCount = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTotalObjectsCount = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablesList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgObjectList)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlDatabaseType
            // 
            this.ddlDatabaseType.FormattingEnabled = true;
            this.ddlDatabaseType.Items.AddRange(new object[] {
            "dst_HANADB",
            "dst_MSSQL2005",
            "dst_MSSQL2008",
            "dst_MSSQL2012",
            "dst_MSSQL2014",
            "dst_MSSQL2016",
            "dst_MSSQL2017"});
            this.ddlDatabaseType.Location = new System.Drawing.Point(59, 64);
            this.ddlDatabaseType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ddlDatabaseType.Name = "ddlDatabaseType";
            this.ddlDatabaseType.Size = new System.Drawing.Size(900, 39);
            this.ddlDatabaseType.TabIndex = 63;
            this.ddlDatabaseType.Tag = "Please select Database Type:";
            this.ddlDatabaseType.Text = "Please-Select";
            // 
            // label47
            // 
            this.label47.AutoEllipsis = true;
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label47.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label47.Location = new System.Drawing.Point(53, 29);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(229, 32);
            this.label47.TabIndex = 62;
            this.label47.Text = "Database Type:";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoEllipsis = true;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label16.Location = new System.Drawing.Point(53, 136);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(651, 32);
            this.label16.TabIndex = 61;
            this.label16.Text = "Service Layer(= HANA Server | SQL Server) IP:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label1.Location = new System.Drawing.Point(53, 250);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 32);
            this.label1.TabIndex = 66;
            this.label1.Text = "Service Layer Port";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.AutoEllipsis = true;
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label42.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label42.Location = new System.Drawing.Point(77, 389);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(242, 32);
            this.label42.TabIndex = 67;
            this.label42.Text = "SBO Credentials";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ucDatabaseInformation1);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.ddlDatabaseType);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtServiceLayerIP);
            this.panel1.Controls.Add(this.txtServiceLayerIPPort);
            this.panel1.Location = new System.Drawing.Point(120, 55);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1317, 1185);
            this.panel1.TabIndex = 69;
            // 
            // ucDatabaseInformation1
            // 
            this.ucDatabaseInformation1.Location = new System.Drawing.Point(83, 475);
            this.ucDatabaseInformation1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ucDatabaseInformation1.Name = "ucDatabaseInformation1";
            this.ucDatabaseInformation1.showLocalization = true;
            this.ucDatabaseInformation1.Size = new System.Drawing.Size(984, 546);
            this.ucDatabaseInformation1.TabIndex = 68;
            // 
            // txtServiceLayerIP
            // 
            this.txtServiceLayerIP.Depth = 0;
            this.txtServiceLayerIP.Hint = "";
            this.txtServiceLayerIP.Location = new System.Drawing.Point(61, 172);
            this.txtServiceLayerIP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtServiceLayerIP.MaxLength = 32767;
            this.txtServiceLayerIP.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServiceLayerIP.Name = "txtServiceLayerIP";
            this.txtServiceLayerIP.PasswordChar = '\0';
            this.txtServiceLayerIP.SelectedText = "";
            this.txtServiceLayerIP.SelectionLength = 0;
            this.txtServiceLayerIP.SelectionStart = 0;
            this.txtServiceLayerIP.Size = new System.Drawing.Size(912, 46);
            this.txtServiceLayerIP.TabIndex = 64;
            this.txtServiceLayerIP.TabStop = false;
            this.txtServiceLayerIP.Tag = "Service Layer IP is required.";
            this.txtServiceLayerIP.Text = "https://172.21.13.115";
            this.txtServiceLayerIP.UseSystemPasswordChar = false;
            // 
            // txtServiceLayerIPPort
            // 
            this.txtServiceLayerIPPort.Depth = 0;
            this.txtServiceLayerIPPort.Hint = "";
            this.txtServiceLayerIPPort.Location = new System.Drawing.Point(61, 298);
            this.txtServiceLayerIPPort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtServiceLayerIPPort.MaxLength = 32767;
            this.txtServiceLayerIPPort.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtServiceLayerIPPort.Name = "txtServiceLayerIPPort";
            this.txtServiceLayerIPPort.PasswordChar = '\0';
            this.txtServiceLayerIPPort.SelectedText = "";
            this.txtServiceLayerIPPort.SelectionLength = 0;
            this.txtServiceLayerIPPort.SelectionStart = 0;
            this.txtServiceLayerIPPort.Size = new System.Drawing.Size(912, 46);
            this.txtServiceLayerIPPort.TabIndex = 65;
            this.txtServiceLayerIPPort.TabStop = false;
            this.txtServiceLayerIPPort.Tag = "Service Layer Port is required.";
            this.txtServiceLayerIPPort.Text = "50000";
            this.txtServiceLayerIPPort.UseSystemPasswordChar = false;
            // 
            // btnLoadTables
            // 
            this.btnLoadTables.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLoadTables.FlatAppearance.BorderSize = 0;
            this.btnLoadTables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadTables.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadTables.Location = new System.Drawing.Point(2438, 104);
            this.btnLoadTables.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoadTables.Name = "btnLoadTables";
            this.btnLoadTables.Size = new System.Drawing.Size(271, 62);
            this.btnLoadTables.TabIndex = 70;
            this.btnLoadTables.Text = "Load Tables";
            this.btnLoadTables.UseVisualStyleBackColor = false;
            this.btnLoadTables.Click += new System.EventHandler(this.btnLoadTables_Click);
            // 
            // dgTablesList
            // 
            this.dgTablesList.AllowUserToAddRows = false;
            this.dgTablesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTablesList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbSelect,
            this.TableName,
            this.TableJson});
            this.dgTablesList.Location = new System.Drawing.Point(1509, 193);
            this.dgTablesList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgTablesList.Name = "dgTablesList";
            this.dgTablesList.RowHeadersVisible = false;
            this.dgTablesList.RowHeadersWidth = 102;
            this.dgTablesList.RowTemplate.Height = 40;
            this.dgTablesList.Size = new System.Drawing.Size(1200, 708);
            this.dgTablesList.TabIndex = 71;
            // 
            // cbSelect
            // 
            this.cbSelect.HeaderText = "Select";
            this.cbSelect.MinimumWidth = 12;
            this.cbSelect.Name = "cbSelect";
            this.cbSelect.Width = 50;
            // 
            // TableName
            // 
            this.TableName.HeaderText = "Table Name";
            this.TableName.MinimumWidth = 12;
            this.TableName.Name = "TableName";
            this.TableName.Width = 300;
            // 
            // TableJson
            // 
            this.TableJson.HeaderText = "";
            this.TableJson.MinimumWidth = 12;
            this.TableJson.Name = "TableJson";
            this.TableJson.Visible = false;
            this.TableJson.Width = 250;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2016, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 32);
            this.label2.TabIndex = 72;
            this.label2.Text = "Table List";
            // 
            // dgFieldsList
            // 
            this.dgFieldsList.AllowUserToAddRows = false;
            this.dgFieldsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFieldsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.FieldName,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgFieldsList.Location = new System.Drawing.Point(2835, 191);
            this.dgFieldsList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgFieldsList.Name = "dgFieldsList";
            this.dgFieldsList.RowHeadersVisible = false;
            this.dgFieldsList.RowHeadersWidth = 102;
            this.dgFieldsList.RowTemplate.Height = 40;
            this.dgFieldsList.Size = new System.Drawing.Size(1107, 708);
            this.dgFieldsList.TabIndex = 73;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Select";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 12;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // FieldName
            // 
            this.FieldName.HeaderText = "Field Name";
            this.FieldName.MinimumWidth = 12;
            this.FieldName.Name = "FieldName";
            this.FieldName.Width = 250;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Table Name";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 12;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 300;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 12;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 250;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3277, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 32);
            this.label3.TabIndex = 74;
            this.label3.Text = "Field List";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2075, 1061);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 32);
            this.label4.TabIndex = 76;
            this.label4.Text = "Object List";
            // 
            // dgObjectList
            // 
            this.dgObjectList.AllowUserToAddRows = false;
            this.dgObjectList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgObjectList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2,
            this.Code,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgObjectList.Location = new System.Drawing.Point(1509, 1207);
            this.dgObjectList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgObjectList.Name = "dgObjectList";
            this.dgObjectList.RowHeadersVisible = false;
            this.dgObjectList.RowHeadersWidth = 102;
            this.dgObjectList.RowTemplate.Height = 40;
            this.dgObjectList.Size = new System.Drawing.Size(1629, 572);
            this.dgObjectList.TabIndex = 75;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Select";
            this.dataGridViewCheckBoxColumn2.MinimumWidth = 12;
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 50;
            // 
            // Code
            // 
            this.Code.HeaderText = "Code";
            this.Code.MinimumWidth = 12;
            this.Code.Name = "Code";
            this.Code.Width = 250;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Table Name";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 12;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 300;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 12;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            this.dataGridViewTextBoxColumn4.Width = 250;
            // 
            // btnLoadFields
            // 
            this.btnLoadFields.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLoadFields.FlatAppearance.BorderSize = 0;
            this.btnLoadFields.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadFields.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadFields.Location = new System.Drawing.Point(3715, 95);
            this.btnLoadFields.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoadFields.Name = "btnLoadFields";
            this.btnLoadFields.Size = new System.Drawing.Size(227, 62);
            this.btnLoadFields.TabIndex = 77;
            this.btnLoadFields.Text = "Load Fields";
            this.btnLoadFields.UseVisualStyleBackColor = false;
            this.btnLoadFields.Click += new System.EventHandler(this.btnLoadFields_Click);
            // 
            // btnLoadObjects
            // 
            this.btnLoadObjects.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLoadObjects.FlatAppearance.BorderSize = 0;
            this.btnLoadObjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadObjects.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadObjects.Location = new System.Drawing.Point(2873, 1107);
            this.btnLoadObjects.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoadObjects.Name = "btnLoadObjects";
            this.btnLoadObjects.Size = new System.Drawing.Size(265, 62);
            this.btnLoadObjects.TabIndex = 78;
            this.btnLoadObjects.Text = "Load Objects";
            this.btnLoadObjects.UseVisualStyleBackColor = false;
            this.btnLoadObjects.Click += new System.EventHandler(this.btnLoadObjects_Click);
            // 
            // btnExportObjectMetaData
            // 
            this.btnExportObjectMetaData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExportObjectMetaData.FlatAppearance.BorderSize = 0;
            this.btnExportObjectMetaData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportObjectMetaData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExportObjectMetaData.Location = new System.Drawing.Point(2024, 1815);
            this.btnExportObjectMetaData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportObjectMetaData.Name = "btnExportObjectMetaData";
            this.btnExportObjectMetaData.Size = new System.Drawing.Size(344, 62);
            this.btnExportObjectMetaData.TabIndex = 79;
            this.btnExportObjectMetaData.Text = "Export Object Meta Data";
            this.btnExportObjectMetaData.UseVisualStyleBackColor = false;
            this.btnExportObjectMetaData.Click += new System.EventHandler(this.btnExportObjectMetaData_Click);
            // 
            // btnExportFieldMetaData
            // 
            this.btnExportFieldMetaData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExportFieldMetaData.FlatAppearance.BorderSize = 0;
            this.btnExportFieldMetaData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportFieldMetaData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExportFieldMetaData.Location = new System.Drawing.Point(3163, 918);
            this.btnExportFieldMetaData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportFieldMetaData.Name = "btnExportFieldMetaData";
            this.btnExportFieldMetaData.Size = new System.Drawing.Size(267, 62);
            this.btnExportFieldMetaData.TabIndex = 80;
            this.btnExportFieldMetaData.Text = "Export Feilds Meta Data";
            this.btnExportFieldMetaData.UseVisualStyleBackColor = false;
            this.btnExportFieldMetaData.Click += new System.EventHandler(this.btnExportFieldMetaData_Click);
            // 
            // btnExportTableMetaData
            // 
            this.btnExportTableMetaData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExportTableMetaData.FlatAppearance.BorderSize = 0;
            this.btnExportTableMetaData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportTableMetaData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExportTableMetaData.Location = new System.Drawing.Point(1883, 918);
            this.btnExportTableMetaData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportTableMetaData.Name = "btnExportTableMetaData";
            this.btnExportTableMetaData.Size = new System.Drawing.Size(344, 62);
            this.btnExportTableMetaData.TabIndex = 81;
            this.btnExportTableMetaData.Text = "Export Tables Meta Data";
            this.btnExportTableMetaData.UseVisualStyleBackColor = false;
            this.btnExportTableMetaData.Click += new System.EventHandler(this.btnExportTableMetaData_Click);
            // 
            // btnExportObjectData
            // 
            this.btnExportObjectData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnExportObjectData.FlatAppearance.BorderSize = 0;
            this.btnExportObjectData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportObjectData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExportObjectData.Location = new System.Drawing.Point(2480, 1815);
            this.btnExportObjectData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportObjectData.Name = "btnExportObjectData";
            this.btnExportObjectData.Size = new System.Drawing.Size(344, 62);
            this.btnExportObjectData.TabIndex = 82;
            this.btnExportObjectData.Text = "Export Object Data";
            this.btnExportObjectData.UseVisualStyleBackColor = false;
            this.btnExportObjectData.Click += new System.EventHandler(this.btnExportObjectData_Click);
            // 
            // btnUDODataBrowsePath
            // 
            this.btnUDODataBrowsePath.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnUDODataBrowsePath.FlatAppearance.BorderSize = 0;
            this.btnUDODataBrowsePath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUDODataBrowsePath.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUDODataBrowsePath.Location = new System.Drawing.Point(1266, 1353);
            this.btnUDODataBrowsePath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUDODataBrowsePath.Name = "btnUDODataBrowsePath";
            this.btnUDODataBrowsePath.Size = new System.Drawing.Size(160, 62);
            this.btnUDODataBrowsePath.TabIndex = 83;
            this.btnUDODataBrowsePath.Text = "Browse...";
            this.btnUDODataBrowsePath.UseVisualStyleBackColor = false;
            this.btnUDODataBrowsePath.Click += new System.EventHandler(this.btnUDODataBrowsePath_Click);
            // 
            // txtMetaDataPath
            // 
            this.txtMetaDataPath.Location = new System.Drawing.Point(110, 1377);
            this.txtMetaDataPath.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtMetaDataPath.Name = "txtMetaDataPath";
            this.txtMetaDataPath.Size = new System.Drawing.Size(1069, 38);
            this.txtMetaDataPath.TabIndex = 84;
            this.txtMetaDataPath.Text = "D:\\B1WebMetaData";
            // 
            // cbTableSelectAll
            // 
            this.cbTableSelectAll.AutoSize = true;
            this.cbTableSelectAll.Location = new System.Drawing.Point(1509, 124);
            this.cbTableSelectAll.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cbTableSelectAll.Name = "cbTableSelectAll";
            this.cbTableSelectAll.Size = new System.Drawing.Size(173, 36);
            this.cbTableSelectAll.TabIndex = 85;
            this.cbTableSelectAll.Text = "Select All";
            this.cbTableSelectAll.UseVisualStyleBackColor = true;
            this.cbTableSelectAll.CheckedChanged += new System.EventHandler(this.cbTableSelectAll_CheckedChanged);
            // 
            // cbFieldsSelectAll
            // 
            this.cbFieldsSelectAll.AutoSize = true;
            this.cbFieldsSelectAll.Location = new System.Drawing.Point(2835, 124);
            this.cbFieldsSelectAll.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cbFieldsSelectAll.Name = "cbFieldsSelectAll";
            this.cbFieldsSelectAll.Size = new System.Drawing.Size(173, 36);
            this.cbFieldsSelectAll.TabIndex = 86;
            this.cbFieldsSelectAll.Text = "Select All";
            this.cbFieldsSelectAll.UseVisualStyleBackColor = true;
            this.cbFieldsSelectAll.CheckedChanged += new System.EventHandler(this.cbFieldsSelectAll_CheckedChanged);
            // 
            // cbObjectsSelectAll
            // 
            this.cbObjectsSelectAll.AutoSize = true;
            this.cbObjectsSelectAll.Location = new System.Drawing.Point(1509, 1133);
            this.cbObjectsSelectAll.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.cbObjectsSelectAll.Name = "cbObjectsSelectAll";
            this.cbObjectsSelectAll.Size = new System.Drawing.Size(173, 36);
            this.cbObjectsSelectAll.TabIndex = 87;
            this.cbObjectsSelectAll.Text = "Select All";
            this.cbObjectsSelectAll.UseVisualStyleBackColor = true;
            this.cbObjectsSelectAll.CheckedChanged += new System.EventHandler(this.cbObjectsSelectAll_CheckedChanged);
            // 
            // txtFilterTable
            // 
            this.txtFilterTable.Location = new System.Drawing.Point(1846, 119);
            this.txtFilterTable.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtFilterTable.Name = "txtFilterTable";
            this.txtFilterTable.Size = new System.Drawing.Size(564, 38);
            this.txtFilterTable.TabIndex = 88;
            this.txtFilterTable.TextChanged += new System.EventHandler(this.txtFilterTable_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1742, 125);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 32);
            this.label5.TabIndex = 89;
            this.label5.Text = "Filter";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3067, 121);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 32);
            this.label6.TabIndex = 91;
            this.label6.Text = "Filter";
            // 
            // txtFilterFields
            // 
            this.txtFilterFields.Location = new System.Drawing.Point(3172, 115);
            this.txtFilterFields.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtFilterFields.Name = "txtFilterFields";
            this.txtFilterFields.Size = new System.Drawing.Size(492, 38);
            this.txtFilterFields.TabIndex = 90;
            this.txtFilterFields.TextChanged += new System.EventHandler(this.txtFilterFields_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1839, 1138);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 32);
            this.label7.TabIndex = 93;
            this.label7.Text = "Filter";
            // 
            // txtFilterObjects
            // 
            this.txtFilterObjects.Location = new System.Drawing.Point(1951, 1133);
            this.txtFilterObjects.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtFilterObjects.Name = "txtFilterObjects";
            this.txtFilterObjects.Size = new System.Drawing.Size(615, 38);
            this.txtFilterObjects.TabIndex = 92;
            this.txtFilterObjects.TextChanged += new System.EventHandler(this.txtFilterObjects_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(110, 1335);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 32);
            this.label8.TabIndex = 94;
            this.label8.Text = "Export Path:";
            // 
            // lblTotalTableCount
            // 
            this.lblTotalTableCount.AutoSize = true;
            this.lblTotalTableCount.Location = new System.Drawing.Point(1615, 948);
            this.lblTotalTableCount.Name = "lblTotalTableCount";
            this.lblTotalTableCount.Size = new System.Drawing.Size(31, 32);
            this.lblTotalTableCount.TabIndex = 95;
            this.lblTotalTableCount.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1509, 948);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 32);
            this.label9.TabIndex = 96;
            this.label9.Text = "Total:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2858, 948);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 32);
            this.label10.TabIndex = 98;
            this.label10.Text = "Total:";
            // 
            // lblTotalFieldsCount
            // 
            this.lblTotalFieldsCount.AutoSize = true;
            this.lblTotalFieldsCount.Location = new System.Drawing.Point(2964, 948);
            this.lblTotalFieldsCount.Name = "lblTotalFieldsCount";
            this.lblTotalFieldsCount.Size = new System.Drawing.Size(31, 32);
            this.lblTotalFieldsCount.TabIndex = 97;
            this.lblTotalFieldsCount.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1509, 1831);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 32);
            this.label11.TabIndex = 100;
            this.label11.Text = "Total:";
            // 
            // lblTotalObjectsCount
            // 
            this.lblTotalObjectsCount.AutoSize = true;
            this.lblTotalObjectsCount.Location = new System.Drawing.Point(1615, 1831);
            this.lblTotalObjectsCount.Name = "lblTotalObjectsCount";
            this.lblTotalObjectsCount.Size = new System.Drawing.Size(31, 32);
            this.lblTotalObjectsCount.TabIndex = 99;
            this.lblTotalObjectsCount.Text = "0";
            // 
            // frmMetaData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(3844, 2108);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblTotalObjectsCount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblTotalFieldsCount);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblTotalTableCount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtFilterObjects);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtFilterFields);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtFilterTable);
            this.Controls.Add(this.cbObjectsSelectAll);
            this.Controls.Add(this.cbFieldsSelectAll);
            this.Controls.Add(this.cbTableSelectAll);
            this.Controls.Add(this.txtMetaDataPath);
            this.Controls.Add(this.btnUDODataBrowsePath);
            this.Controls.Add(this.btnExportObjectData);
            this.Controls.Add(this.btnExportTableMetaData);
            this.Controls.Add(this.btnExportFieldMetaData);
            this.Controls.Add(this.btnExportObjectMetaData);
            this.Controls.Add(this.btnLoadObjects);
            this.Controls.Add(this.btnLoadFields);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgObjectList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgFieldsList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgTablesList);
            this.Controls.Add(this.btnLoadTables);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmMetaData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Meta Data";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablesList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgObjectList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField txtServiceLayerIP;
        private System.Windows.Forms.ComboBox ddlDatabaseType;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label16;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtServiceLayerIPPort;
        private System.Windows.Forms.Label label1;
        private ucDatabaseInformation ucDBInfo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLoadTables;
        private System.Windows.Forms.DataGridView dgTablesList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgFieldsList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgObjectList;
        private System.Windows.Forms.Button btnLoadFields;
        private System.Windows.Forms.Button btnLoadObjects;
        private System.Windows.Forms.Button btnExportObjectMetaData;
        private System.Windows.Forms.Button btnExportFieldMetaData;
        private System.Windows.Forms.Button btnExportTableMetaData;
        private System.Windows.Forms.Button btnExportObjectData;
        private System.Windows.Forms.Button btnUDODataBrowsePath;
        private ucDatabaseInformation ucDatabaseInformation;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cbSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn TableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TableJson;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.TextBox txtMetaDataPath;
        private System.Windows.Forms.CheckBox cbTableSelectAll;
        private System.Windows.Forms.CheckBox cbFieldsSelectAll;
        private System.Windows.Forms.CheckBox cbObjectsSelectAll;
        private System.Windows.Forms.TextBox txtFilterTable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFilterFields;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFilterObjects;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTotalTableCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTotalFieldsCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTotalObjectsCount;
        private ucDatabaseInformation ucDatabaseInformation1;
    }
}