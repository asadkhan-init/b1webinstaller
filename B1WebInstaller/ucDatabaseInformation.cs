﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using B1WebInstaller.BusinessEntities;

namespace B1WebInstaller
{
    public partial class ucDatabaseInformation : UserControl
    {
        public bool showLocalization { get; set; } = true;
        public ucDatabaseInformation()
        {
            InitializeComponent();
          

           

            BindLocalization();

        }

        public DatabaseInformation GetDatabaseInformation() {

            DatabaseInformation dbInfo = new DatabaseInformation() { SBOUserName = txtSBOUserName.Text, SBOPassword = txtSBOPassword.Text, Localization = Convert.ToString(cbLocalization.SelectedValue), DatabaseName = txtDatabaseNames.Text };

            return dbInfo;
        }

        public void setUserName(string userName) {

            txtSBOUserName.Text = userName;
        }

        public void setPassword(string password)
        {

            txtSBOPassword.Text = password;
        }

        public void setDatabase(string database)
        {

            txtDatabaseNames.Text = database;
        }

        public void showHideLocalization()
        {

            if (showLocalization)
            {
                cbLocalization.Visible = true;
                lblLocalization.Visible = true;
            }
            else
            {
                cbLocalization.Visible = false;
                lblLocalization.Visible = false;
            }
        }
        private void BindLocalization()
        {

            BusinessLogic businessLogic = new BusinessLogic();
            var ds = businessLogic.GetLocalizationList();

            cbLocalization.DataSource = ds;
            cbLocalization.DisplayMember = "LanguageFullName";
            cbLocalization.ValueMember = "LanguageShortName";

            cbLocalization.SelectedIndex = 1;
        }

    }
}
