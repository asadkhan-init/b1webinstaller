﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1WebInstaller
{
    public class Constants
    {
        public struct SystemConstants
        {

            public static double REQUIRED_WINDOWS_VERSION = 6.29;
            public static double REQUIRED_IIS_VERSION = 8.0;
            public static double REQUIRED_REWRITE_URL_VERSION = 7.19;
            public static double REQUIRED_DONTNETFRAMEWORK_VERSION = 4.6;
            public static string INSTALLATION_DATA_FOLDER = "Installationdata";
            public static string CRYSTAL_REPORT_FOLDER = "CrystalReports";
            public static string CRYSTAL_REPORT_TEMPLATE_FOLDER = "CrystalReportTemplates";
            public static string LICENSE_FILE = "user.lic";
            public static string INSTALLATION_DATA_FOLDER_ZIP = "Installationdata.zip";
            public static string LOG_DIR_PATH = @"C:/InstallerLogs";
            public static string LOG_FILE_NAME = string.Format("{0}_{1}.txt","Log",DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"));
            public static string TEMP_PATH = Environment.GetEnvironmentVariable("TEMP") + @"\";
            public static string LICENSE_API_ENDPOINT = "http://hana.b1-web.com/api/v1/getsoftwarekey";
            public static string LICENSE_API_ENDPOINT_API_KEY = "82338e3891b236863215090fa95b8990f2d048aa452af8d7882c4f098e62b037";
            public static string DEFAULT_SITE_NAME = @"B1WebClient";
            public static string PLEASE_SELECT = "Please-Select";
        }

        public struct SystemMessages
        {
            public static string PASSWORD_UNMACHTED = "Password doesnt match!";
            public static string SERVICE_FAILED = "Error in Service";
            public static string UNABLE_TO_CONNECT_WITH_SERVICE_LAYER = "Unable to connect to with Service Layer, please check credentials and IP";
            public static string GENERAL_ERROR = "Error in System";
            public static string CONNECTION_CLOSED = "The underlying connection was closed";
            public static string INVALID_WINDOWS_VERSION = string.Format("{0} {1}", "Windows version should be greater or equal to ", Constants.SystemConstants.REQUIRED_WINDOWS_VERSION);
            public static string INVALID_DOTNET_FRAMEWORK_VERSION = string.Format("{0} {1}", "Dot Net Framework version should be greater or equal to ", Constants.SystemConstants.REQUIRED_DONTNETFRAMEWORK_VERSION);
            public static string INVALID_IIS_VERSION = string.Format("{0} {1}", "IIS version should be greater or equal to ", Constants.SystemConstants.REQUIRED_IIS_VERSION);
            public static string INVALID_REWRITE_URL_VERSION = string.Format("{0} {1}", "Rewrite Url version should be greater or equal to ", Constants.SystemConstants.REQUIRED_REWRITE_URL_VERSION);
            public static string ENTRY_ALREADY_EXISTS = "already exists";
            public static string NO_RECORDS = "No records";
            
            public static string VALUES_ALREADY_EXISTS = "are values in table";
            public static string DUPLICATE_EXISTS = "Duplicate value";
            public static string INVALID_LICENSE_KEY = "License key is invalid";
            public static string DATABASE_LIST_EMPTY = "Please add/select database(s) to proceed with installation";
            public static string SELECT_INSTALLATION_OPTION = "Please select atlease one option to start installation";
            public static string DATABASE_ALREADY_EXISTS = "Database already exists in the list";
            public static string INVALID_EMAIL = "Email Address is invalid.";
            public static string INVALID_PHONE_NO = "Phone No is invalid.";
            public static string INSTALLATION_NO_REQUIRED = "Installation number is required.";
            public static string HANA_CONNECTION_FAILED = "Hana connection failed.";
            public static string SQL_CONNECTION_FAILED = "SQL connection failed.";
            public static string SQL_SERVER_NOT_FOUND = "SQL server not found.";
            public static string COPY_FILE_FAILED = "Unable to copy release files.";
            public static string KEYRESOLVER_SP_HANA_FAILED = "Error when creating IQ1KEYRESOLVER sp on HANA. ";
            public static string KEYRESOLVER_SP_SQL_FAILED = "Error when creating IQ1KEYRESOLVER sp on SQL. ";
            public static string LICENSE_KEY_SENT = "License key has been sent to {0}";
            public static string SSH_CONNECTION_SUCCESSFUL = " SSH connection Successful. ";
            public static string SSH_CONNECTION_FAILED = " Failed to establish SSH connection, you will have to restart service layer manually when prompted. ";
            public static string RESTART_SERVICE_LAYER = " Please restart service layer and press OK. ";
            public static string RESTART_SBO_Job_Service_BackEnd = " Please restart SBO Job Service BackEnd from services and press OK. ";
            public static string ENTRY_DOESNOT_EXIST = "does not exist";
            public static string HANA_CLIENT_NOT_FOUND = "HANA Client not found.";
            public static string LOGIN_FAILED = " Login failed .";
            public static string CONFIGURATION_UPDATE_FAILED = " Unable to update configuration file .";
            public static string UDS_UPDATE_FAILED = " Unable to create UDs .";
            public static string CONFIGURATION_DOESNT_EXISTS = " Configuration file doesnt exists, please fill all information in installer to continue with update.";
            public static string SELECT_LOCALIZATION = "Please select localization";

        }

        public struct DatabaseType
        {

            public static string dst_HANADB = "dst_HANADB";
            public static string dst_MSSQL2005 = "dst_MSSQL2005";
            public static string dst_MSSQL2008 = "dst_MSSQL2008";
            public static string dst_MSSQL2012 = "dst_MSSQL2012";
            public static string dst_MSSQL2014 = "dst_MSSQL2014";
            public static string dst_MSSQL2016 = "dst_MSSQL2016";
            public static string dst_MSSQL2017 = "dst_MSSQL2017";
        }

        public struct StoredProcedures
        {
            public static string KEYRESOLVER_HANA = "" +
                            "CREATE PROCEDURE \"" + "{0}" + "\".\"IQ1KEYRESOLVER\" (in tableName nvarchar(30), in primaryKeyName nvarchar(30), in foreignKeyName nvarchar(30), in foreignKeyValue nvarchar(50), in foreignKeyName2 nvarchar(30), in foreignKeyValue2 nvarchar(50)) \n" +
                            "LANGUAGE SQLSCRIPT \n" +
                            "AS \n" +
                            "BEGIN \n" +
                            "IF :foreignKeyName2 = '' \n" +
                            "THEN \n" +
                            "EXECUTE IMMEDIATE 'SELECT DISTINCT \"' || :primaryKeyName || '\" FROM \"' || :tableName || '\" WHERE \"' || :foreignKeyName || '\" = ''' || :foreignKeyValue || ''''; \n" +
                            "ELSE \n" +
                            "EXECUTE IMMEDIATE 'SELECT DISTINCT \"' || :primaryKeyName || '\" FROM \"' || :tableName || '\" WHERE \"' || :foreignKeyName || '\" = ''' || :foreignKeyValue || '''' || ' AND \"' || :foreignKeyName2 || '\" = ''' || :foreignKeyValue2 || ''''; \n" +
                            "END IF; \n" +
                            "END; ";

            public static string KEYRESOLVER_SQL = "" +
                           " CREATE PROCEDURE IQ1KEYRESOLVER @tableName nvarchar(30), @primaryKeyName nvarchar(30), @foreignKeyName nvarchar(30), @foreignKeyValue nvarchar(50), @foreignKeyName2 nvarchar(30), @foreignKeyValue2 nvarchar(50) \n" +
                            "AS \n" +
                            "BEGIN \n" +

                                "declare @query nvarchar(max) \n" +

                                "IF @foreignKeyName2 = '' \n" +

                                "begin \n" +
                                    "select @query = 'SELECT DISTINCT \"' + @primaryKeyName + '\" FROM \"' + @tableName + '\" WHERE \"' + @foreignKeyName + '\" = ''' + @foreignKeyValue + ''''; \n" +
                                "end \n" +
                                "ELSE \n" +

                                "begin \n" +
                                    "select @query = 'SELECT DISTINCT \"' + @primaryKeyName + '\" FROM \"' + @tableName + '\" WHERE \"' + @foreignKeyName + '\" = ''' + @foreignKeyValue + '''' + ' AND \"' + @foreignKeyName2 + '\" = ''' + @foreignKeyValue2 + ''''; \n" +
                                "end \n" +
                                "EXEC(@query) \n" +
                            "end";


            public static string KEYRESOLVER_SQL_DROP = " DROP PROCEDURE IQ1KEYRESOLVER ";

            public static string VIEW_CREATE_SQL_SALESB1WEBQUERY = "" +

                " create view SALESB1WEBQUERY \n" +
                  "   as \n" +
                  "   select CardCode, TaxDate, DocStatus,[Year],[Month], ItemCode, sum(LineTotal) LineTotal,SlpCode \n" +
                    "     from( \n" +
                   "      select CardCode, TaxDate, DocStatus, year(r.DocDate)[Year], month(r.DocDate)[Month], \n" +
                   "      ItemCode,-1*LineTotal LineTotal, r.SlpCode from ORIN r \n" +
                    "     inner join rin1 r1 on r.DocEntry = r1.DocEntry \n" +
                    "     where Canceled = 'N' \n" +


                      "   union all \n" +


                      "   select CardCode, TaxDate, DocStatus, year(r.DocDate)[Year], month(r.DocDate)[Month], \n" +
                       "  ItemCode, LineTotal LineTotal, r.SlpCode from OINV r \n" +
                       "  inner join INV1 r1 on r.DocEntry = r1.DocEntry \n" +
                      "   where Canceled = 'N') xx \n" +
                       "    group by CardCode, TaxDate, DocStatus,[Year],[Month], ItemCode, SlpCode \n";


            public static string VIEW_CREATE_SQL_B1WEB_ACCBAL = "" +

               " create VIEW B1WEB_ACCBAL \n" +
                 " as \n" +
                 "  SELECT T0.[transid], \n" +
                 "  T0.[line_id], \n" +
                "    T0.[account], \n" +
                  "  T0.[debit], \n" +
                  "  T0.[credit], \n" +
                  "  T0.[syscred], \n" +
                   " T0.[sysdeb], \n" +
                 "   T0.[fcdebit], \n" +
                  "  T0.[fccredit], \n" +
                 "   T0.[fccurrency], \n" +
                  " CONVERT(char(10), T0.[duedate],126) DueDate,  \n" +
                 "  T0.[sourceid],  \n" +
                  " T0.[sourceline], \n" +
                 "  T0.[shortname],  \n" +
                 "  T0.[intrnmatch],  \n" +
                 "  T0.[extrmatch],  \n" +
                 "  T0.[contraact],  \n" +
                "   T0.[linememo],  \n" +
                "   T0.[ref3line],  \n" +
                 "  T0.[transtype],  \n" +

                "   CONVERT(char(10), T0.[refdate],126) RefDate, \n" +
                 "  CONVERT(char(10), T0.[ref2date],126) Ref2Date,  \n" +
                "   T0.[ref1],  \n" +
                "   T0.[ref2],  \n" +
                "   T0.[createdby],  \n" +
                 "  T0.[baseref],  \n" +
                 "  T0.[project],  \n" +
                "   T0.[transcode],  \n" +
                 "  T0.[profitcode],  \n" +
                 "  T0.[taxdate],  \n" +
                 "  T0.[systemrate],  \n" +
                 "  T0.[mthdate],  \n" +
                 "  T0.[tomthsum],  \n" +
                 "  T0.[usersign],  \n" +
                 "  T0.[batchnum],  \n" +
                 "  T0.[finncpriod],  \n" +
                 "  T0.[reltransid],  \n" +
                 "  T0.[rellineid],  \n" +
                 "  T0.[reltype],  \n" +
                 "  T0.[loginstanc],  \n" +
                 "  T0.[vatgroup],  \n" +
                 "  T0.[basesum],  \n" +
                 "  T0.[vatrate],  \n" +
                "   T0.[indicator],  \n" +
                 "  T0.[adjtran],  \n" +
                 "  T0.[revsource],  \n" +
                 "  T0.[objtype],  \n" +
                 "  CONVERT(char(10), T0.[VatDate],126) VatDate, \n" +
                 "  T0.[paymentref],  \n" +
                "  T0.[sysbasesum],  \n" +
                 "  T0.[multmatch],  \n" +
                 "  T0.[vatline],  \n" +
                 " T0.[vatamount],  \n" +
                 "  T0.[sysvatsum],  \n" +
                 "  T0.[closed],  \n" +
                 "  T0.[grossvalue],  \n" +
                 "  T0.[checkabs],  \n" +
                "   T0.[linetype],  \n" +
                 "  T0.[debcred],  \n" +
                "   T0.[sequencenr],  \n" +
                "   T0.[stornoacc],  \n" +
                "   T0.[balduedeb],  \n" +
                 "  T0.[balduecred],  \n" +
                  " T0.[balfcdeb],  \n" +
                "   T0.[balfccred],  \n" +
                "   T0.[balscdeb],  \n" +
                "    T0.[balsccred],  \n" +
                 "   T0.[isnet],  \n" +
                 "   T0.[dunwizblck],  \n" +
                  "  T0.[dunnlevel],  \n" +
                 "   T0.[dundate],  \n" +
                 "   T0.[taxtype],  \n" +
                 "   T0.[taxpostacc],  \n" +
                 "   T0.[stacode],  \n" +
                 "   T0.[statype],  \n" +
                 "   T0.[taxcode],  \n" +
                 "   T0.[validfrom],  \n" +
                 "   T0.[grossvalfc],  \n" +
                 "   T0.[lvlupddate],  \n" +
                  "  T0.[ocrcode2],  \n" +
                 "   T0.[ocrcode3],  \n" +
                 "   T0.[ocrcode4],  \n" +
                 "   T0.[ocrcode5],  \n" +
                 "   T0.[mientry],  \n" +
                 "   T0.[miventry],  \n" +
                 "   T0.[clsintp],  \n" +
                 "   T0.[cenvatcom],  \n" +
                 "   T0.[mattype],  \n" +
                 "   T0.[pstngtype],  \n" +
                 "   T0.[validfrom2],  \n" +
                 "   T0.[validfrom3],  \n" +
                 "   T0.[validfrom4],  \n" +
                 "   T0.[validfrom5],  \n" +
                  "  T0.[location],  \n" +
                  "  T0.[wtaxcode],  \n" +
                  "  T0.[equvatrate],  \n" +
                  "  T0.[equvatsum],  \n" +
                  "  T0.[sysequsum],  \n" +
                  "  T0.[totalvat],  \n" +
                  "  T0.[systvat],  \n" +
                  "  T0.[wtliable],  \n" +
                  "  T0.[wtline],  \n" +
                  "  T0.[wtapplied],  \n" +
                 "   T0.[wtapplieds],  \n" +
                 "   T0.[wtappliedf],  \n" +
                 "   T0.[wtsum],  \n" +
                 "   T0.[wtsumfc],  \n" +
                 "   T0.[wtsumsc],  \n" +
                 "   T0.[payblock],  \n" +
                 "   T0.[payblckref],  \n" +
                 "   T0.[lictradnum],  \n" +
                 "   T0.[interimtyp],  \n" +
                 "   T0.[dprid],  \n" +
                 "   T0.[matchref],  \n" +
                 "   T0.[ordered],  \n" +
                 "   T0.[cup],  \n" +
                 "   T0.[cig],  \n" +
                 "   T0.[bplid],  \n" +
                 "   T0.[bplname],  \n" +
                 "   T0.[vatregnum],  \n" +
                 "   T0.[sledgerf],  \n" +
                 "   T0.[initref2],  \n" +
                 "   T0.[initref3ln],  \n" +
                 "   T0.[expuuid],  \n" +
                 "   T0.[expoptype],  \n" +
                 "   T0.[extransid],  \n" +
                 "   T0.[docarr],  \n" +
                 "   T0.[docline],  \n" +
                  "  T0.[myftype],  \n" +
                "    T0.[docentry],  \n" +
                 "   T0.[docnum],  \n" +
                 "   T0.[doctype],  \n" +
                 "   T0.[docsubtype],  \n" +
                 "   T0.[rmrktmpt],  \n" +
                 "   T0.[cemcode],  \n" +
                 "   T1.[user_code],  \n" +
                  "  T2.[maxreconnum],  \n" +
                 "   T3.[agrno],  \n" +
                 "   T4.[number]  \n" +
                    " FROM[dbo].[jdt1] \n" +
                      "           T0 \n" +
                       "  LEFT OUTER JOIN[dbo].[ousr] \n" +
             "        T1 \n" +
               "          ON T1.[userid] = T0.[usersign]  \n" +
           " LEFT OUTER JOIN(SELECT T0.[TransId] AS 'TransId', T0.[TransRowId] AS 'TransRowId', \n" +
             "        MAX(T0.[ReconNum]) AS 'MaxReconNum'  \n" +

               "      FROM[dbo].[ITR1] T0 GROUP BY T0.[TransId], T0.[TransRowId]) T2 \n" +
                 "     ON T2.[transid] = T0.[transid] \n" +
                   "      AND T2.[transrowid] = T0.[line_id] \n" +
        " INNER JOIN[dbo].[ojdt] \n" +
          "           T3 \n" +
            "    ON T3.[transid] = T0.[transid] \n" +
        " LEFT OUTER JOIN[dbo].[ooat] \n" +
          "           T4 \n" +
            "        ON T4.[absid] = T3.[agrno] \n";



            public static string VIEW_CREATE_SQL_B1WEB_LASTEVALPR = "" +
                       "   create view B1WEB_LASTEVALPR \n" +
                       "   as \n" +
                       "   select ItemCode, LstEvlDate, LstEvlPric from OITM \n";

            public static string VIEW_CREATE_SQL_B1WEB_LASTPURPR = "" +
                        "   create view B1WEB_LASTPURPR \n" +
                       "    as \n" +
                        "   select t.ItemCode, max(p1.Price) Price from PCH1 p1 \n" +
                        "   inner join \n" +
                        "   (select p1.ItemCode, max(p1.DocEntry) DocEntry from PCH1 p1 \n" +
                        "   inner join OPCH p on p.DocEntry= p1.DocEntry \n" +
                        "   inner join OITM i on i.ItemCode = p1.ItemCode \n" +
                        "   where p.CANCELED= 'N' \n" +
                        "   group by p1.ItemCode) t on t.DocEntry=p1.DocEntry and t.ItemCode = p1.ItemCode \n" +
                        "   group by t.ItemCode \n";

            public static string VIEW_CREATE_SQL_B1WEB_OCLGASSGN = "" +
           "    create view B1WEB_OCLGASSGN \n" +
           "    as \n" +
           "    select ClgCode, AssignedBy from OCLG \n";

            public static string VIEW_CREATE_SQL_B1WEB_OCLS = "" +

          "   create view B1WEB_OCLS \n" +
           "    as \n" +
           "    select Code, Name, Type, Active from OCLS \n";

            public static string VIEW_CREATE_SQL_B1WEB_OOIR = "" +

          "    create view B1WEB_OOIR \n" +
           "    as \n" +
           "    select Num, Descript from OOIR \n";

            public static string VIEW_CREATE_SQL_B1WEB_SCL2_1 = "" +

        "    create view B1WEB_SCL2_1 \n" +
         "    as \n" +
         "    select * from SCL2 \n";

            public static string VIEW_CREATE_SQL_B1WEB_SCL3_1 = "" +

       "    create view B1WEB_SCL3_1 \n" +
        "    as \n" +
        "    select * from SCL3 \n";

            public static string VIEW_CREATE_SQL_B1WEB_OCLO = "" +

      "    create view B1WEB_OCLO \n" +
       "    as \n" +
       "    select * from OCLO \n";

            public static string VIEW_CREATE_SQL_B1WEB_ASCL = "" +

     "    create view B1WEB_ASCL \n" +
      "    as \n" +
      "    select * from ASCL \n";

            public static string VIEW_CREATE_SQL_B1WEB_OSCL = "" +

     "    create view B1WEB_OSCL \n" +
      "    as \n" +
      "    select * from OSCL \n";

            public static string VIEW_CREATE_SQL_B1WEB_CTG1 = "" +

    "    create view B1WEB_CTG1 \n" +
     "    as \n" +
     "    select * from CTG1 \n";


            public static string VIEW_CREATE_SQL_B1WEB_OPMGOPENACC = "" +
          "    create view B1WEB_OPMGOPENACC \n" +
           "    as \n" +
           "    select FIPROJECT, Closed, ClgCode from OCLG \n";


            public static string VIEW_CREATE_SQL_B1WEB_OPRT = "" +
           "    create view B1WEB_OPRT \n" +
           "    as \n" +
           "    select PrtId, Name from OPRT \n";

            public static string VIEW_CREATE_SQL_B1WEB_ORTT = "" +
         "    create view B1WEB_ORTT \n" +
         "    as \n" +
         "    select RateDate,Currency,Rate from ORTT \n";

            public static string VIEW_CREATE_SQL_MONTHNAMEQUERY = "" +
             "    create view MONTHNAMEQUERY \n" +
           "    as \n" +
           "    WITH months(MonthNumber) AS \n" +
           "    ( \n" +
            "       SELECT 0 \n" +
            "       UNION ALL \n" +
             "      SELECT MonthNumber+1  \n" +
             "      FROM months \n" +
             "      WHERE MonthNumber < 12 \n" +
             "  ) \n" +
             "  SELECT Month(DATEADD(MONTH,-MonthNumber, GETDATE())) [Month], \n" +
             "  UPPER(DATENAME(MONTH, DATEADD(MONTH,-MonthNumber, GETDATE()))) AS[MonthName] \n" +
             "    FROM months where MonthNumber>0; \n";



            public static string SQL_DROP_QUERY = "" +
          " drop view SALESB1WEBQUERY \n" +
          "  drop view B1WEB_ACCBAL \n" +
          "  drop view B1WEB_LASTEVALPR \n" +
          "  drop view B1WEB_LASTPURPR \n" +
          "  drop view B1WEB_OCLGASSGN \n" +
          "  drop view B1WEB_OCLS \n" +
          "  drop view B1WEB_OOIR \n" +
          "  drop view B1WEB_OPMGOPENACC \n" +
          "  drop view B1WEB_OPRT \n" +
          "  drop view MONTHNAMEQUERY \n" +
          "  drop view SalesAnalysisQuery \n";
        }

        public struct MetaDataType
        {
            public static string USER_DEFINED_TABLES = "UDT";
            public static string USER_DEFINED_FIELDS = "UDF";
            public static string USER_DEFINED_OBJECTS = "UDO";
        }

        public struct SERVICEURL
        {

            public static string USER_DEFINED_TABLE = "";
            public static string USER_DEFINED_FIELDS = "";
            public static string USER_DEFINED_OBJECTS = "";
            public static string LOGIN = "";
            public static string SERVICE_LAYER = "";
            public static string USER_DEFINED_FIELDS_GET = "";
            public static string USER_DEFINED_OBJECTS_GET = "";
            public static string BATCH = "";
        }

        public struct ConnectionType
        {
            public static string HANA = "HANA";
            public static string SQL = "SQL";
            public static string SSH = "SSH";
            public static string DATABASE = "DATABASE";
        }

        public struct InstallationType
        {
            public static string HANA = "HANA";
            public static string SQL = "SQL";
        }

        public struct Localization
        {
            public static string US = "US";
            public static string DE = "DE";
        }

        public struct Query {

            public static string HANA_DATABASE_LIST = " select distinct SCHEMA_NAME from public.tables where SCHEMA_NAME not like '%_SYS%' ";
            public static string SQL_DATABASE_LIST = " SELECT name FROM master.sys.databases ";
        }

        public struct CommandLineArguments {

            public static string METADATA = " V360ModelGenerator.exe --metadata ";
            public static string IMPORT = " V360ModelGenerator.exe --import ";

        }

        public struct ModelGeneratorConfiguration {

            public static string METADATA = "configuration/applicationSettings/V360ModelGenerator.Properties.Settings/setting/value";
        }
    }
}
