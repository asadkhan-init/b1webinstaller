﻿using B1WebInstaller.BusinessEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace B1WebInstaller
{
    public partial class frmMetaData : Form
    {
        BusinessLogic _businessLogic = null;
        readonly List<string> excludedUDT;
        readonly List<string> excludedUDF;
        readonly List<string> excludedUDO;

        public frmMetaData()
        {
            InitializeComponent();

            _businessLogic = new BusinessLogic();
            ddlDatabaseType.SelectedIndex = 0;
            setScreenResolution();


            excludedUDT = Convert.ToString(ConfigurationManager.AppSettings["excludedUDT"]).Split(',').ToList();
            excludedUDF = Convert.ToString(ConfigurationManager.AppSettings["excludedUDF"]).Split(',').ToList();
            excludedUDO = Convert.ToString(ConfigurationManager.AppSettings["excludedUDO"]).Split(',').ToList();

            txtServiceLayerIP.Text = Convert.ToString(ConfigurationManager.AppSettings["MDServiceLayerIP"]);
            txtServiceLayerIPPort.Text = Convert.ToString(ConfigurationManager.AppSettings["MDPort"]);

            ucDatabaseInformation1.setUserName(Convert.ToString(ConfigurationManager.AppSettings["MDUserName"]));
            ucDatabaseInformation1.setPassword(Convert.ToString(ConfigurationManager.AppSettings["MDPassword"]));
            ucDatabaseInformation1.setDatabase(Convert.ToString(ConfigurationManager.AppSettings["MDDatabaseName"]));

            ucDatabaseInformation1.showLocalization = false;
            ucDatabaseInformation1.showHideLocalization();
        }

        private void btnLoadTables_Click(object sender, EventArgs e)
        {
            try
            {
                _businessLogic.SetUrlConstants(txtServiceLayerIP.Text, txtServiceLayerIPPort.Text);
                LoadTablesList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoadFields_Click(object sender, EventArgs e)
        {
            try
            {
                _businessLogic.SetUrlConstants(txtServiceLayerIP.Text, txtServiceLayerIPPort.Text);
                LoadFieldsList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoadObjects_Click(object sender, EventArgs e)
        {
            try
            {
                _businessLogic.SetUrlConstants(txtServiceLayerIP.Text, txtServiceLayerIPPort.Text);
                LoadObjectsList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExportTableMetaData_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgTablesList.Rows.Count > 0)
                {
                    var path = string.Format(@"{0}\{1}\{2}.{3}", txtMetaDataPath.Text, "MetaData", "UDT", "Json");

                    Directory.CreateDirectory(Path.GetDirectoryName(path));

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }



                    foreach (DataGridViewRow row in dgTablesList.Rows)
                    {
                        if ((Convert.ToBoolean(row.Cells[0].Value) == true) && row.Visible == true)
                        {
                            showBusyCursor();

                            var objName = Convert.ToString(row.Cells[1].Value);
                            var tableJson = Convert.ToString(row.Cells[2].Value);

                            using (StreamWriter w = File.AppendText(path))
                            {
                                w.Write(tableJson);
                                w.Write("$%&");
                            }
                            //_businessLogic.WriteMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_TABLE, Constants.MetaDataType.USER_DEFINED_TABLES, path);

                            showDefaultCursor();
                        }
                    }

                    FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    fs.SetLength(fs.Length - 3);
                    fs.Close();
                }
                else
                {

                    MessageBox.Show(" No data to export. ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Error while exporting UDT. " + ex.Message);
            }
        }

        private void btnExportFieldMetaData_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgFieldsList.Rows.Count > 0)
                {
                    var path = string.Format(@"{0}\{1}\{2}.{3}", txtMetaDataPath.Text, "MetaData", "UDF", "Json");

                    Directory.CreateDirectory(Path.GetDirectoryName(path));

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    foreach (DataGridViewRow row in dgFieldsList.Rows)
                    {
                        if ((Convert.ToBoolean(row.Cells[0].Value) == true))
                        {
                            showBusyCursor();

                            var tableJson = Convert.ToString(row.Cells[3].Value);

                            using (StreamWriter w = File.AppendText(path))
                            {
                                w.Write(tableJson);
                                w.Write("$%&");
                            }

                            showDefaultCursor();
                        }
                    }

                    FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    fs.SetLength(fs.Length - 3);
                    fs.Close();
                }
                else
                {

                    MessageBox.Show(" No data to export. ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Error while exporting UDF. " + ex.Message);
            }
        }

        private void btnExportObjectMetaData_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgObjectList.Rows.Count > 0)
                {
                    var path = string.Format(@"{0}\{1}\{2}.{3}", txtMetaDataPath.Text, "MetaData", "UDO", "Json");

                    Directory.CreateDirectory(Path.GetDirectoryName(path));

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    //File.Create(path);

                    foreach (DataGridViewRow row in dgObjectList.Rows)
                    {
                        if ((Convert.ToBoolean(row.Cells[0].Value) == true))
                        {
                            showBusyCursor();

                            var tableJson = Convert.ToString(row.Cells[3].Value);

                            using (StreamWriter w = File.AppendText(path))
                            {
                                w.Write(tableJson);
                                w.Write("$%&");
                            }

                            showDefaultCursor();
                        }
                    }

                    FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    fs.SetLength(fs.Length - 3);
                    fs.Close();
                }
                else
                {

                    MessageBox.Show(" No data to export. ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Error while exporting UDO. " + ex.Message);
            }
        }

        private void btnExportObjectData_Click(object sender, EventArgs e)
        {
            if (dgObjectList.Rows.Count > 0)
            {
                var sessionId = GetSessionId();

                if (!string.IsNullOrEmpty(sessionId))
                {
                    foreach (DataGridViewRow row in dgObjectList.Rows)
                    {
                        if ((Convert.ToBoolean(row.Cells[0].Value) == true))
                        {
                            showBusyCursor();

                            var objName = Convert.ToString(row.Cells[1].Value);

                            var url = string.Format("{0}{1}", Constants.SERVICEURL.SERVICE_LAYER, objName);
                            var path = string.Format(@"{0}/{1}/{2}.{3}", txtMetaDataPath.Text, "ObjectData",objName, "Json");
                            if (System.IO.File.Exists(path))
                                System.IO.File.Delete(path);

                            _businessLogic.WriteUDOData(sessionId, url, objName, path);

                            showDefaultCursor();
                        }
                    }
                }
            }
            else
            {

                MessageBox.Show(" No data to export. ");
            }
        }
        private void LoadTablesList()
        {
            try
            {
                var sessionId = GetSessionId();

                if (!string.IsNullOrEmpty(sessionId))
                {
                    showBusyCursor();

                    _businessLogic.existingUDTList = new List<UDT>();
                    _businessLogic.GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_TABLE, Constants.MetaDataType.USER_DEFINED_TABLES);

                    bindTablesList();

                    showDefaultCursor();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void LoadFieldsList()
        {
            try
            {
                if (dgTablesList.Rows.Count > 0)
                {
                    var sessionId = GetSessionId();

                    if (!string.IsNullOrEmpty(sessionId))
                    {
                        _businessLogic.existingUDFList = new List<UDF>();

                        showBusyCursor();

                        foreach (DataGridViewRow row in dgTablesList.Rows)
                        {
                            if ((Convert.ToBoolean(row.Cells[0].Value) == true) && !string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Value)) && row.Visible == true)
                            {
                                showBusyCursor();

                                var objName = Convert.ToString(row.Cells[1].Value);
                                var tableJson = Convert.ToString(row.Cells[2].Value);

                                Constants.SERVICEURL.USER_DEFINED_FIELDS_GET = string.Format("{0}?$filter=startswith(TableName,'@{1}')", Constants.SERVICEURL.USER_DEFINED_FIELDS, objName);
                                _businessLogic.GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_FIELDS_GET, Constants.MetaDataType.USER_DEFINED_FIELDS);

                                showDefaultCursor();
                            }
                        }


                        bindFieldsList();

                        showDefaultCursor();
                    }
                }
                else
                {
                    MessageBox.Show(" Please load tables list first. ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void LoadObjectsList()
        {
            try
            {
                var sessionId = GetSessionId();

                if (!string.IsNullOrEmpty(sessionId))
                {
                    _businessLogic.existingUDOList = new List<dynamic>();

                    showBusyCursor();

                    _businessLogic.GetMetaData(sessionId, Constants.SERVICEURL.USER_DEFINED_OBJECTS_GET, Constants.MetaDataType.USER_DEFINED_OBJECTS);

                    var tablesResult = _businessLogic.existingUDOList;

                    bindObjectList();

                    showDefaultCursor();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void bindTablesList()
        {
            try
            {
                clearTableRows(dgTablesList);

                var filteredTableList = _businessLogic.existingUDTList.Where(tbl => !excludedUDT.Any(e => tbl.TableName.Contains(e)) && !tbl.TableName.ToLower().Contains("test"));

                foreach (var table in filteredTableList)
                {
                    var res = JsonConvert.SerializeObject(table);
                    dgTablesList.Rows.Add(null, table.TableName, res);

                }

                lblTotalTableCount.Text = Convert.ToString(dgTablesList.Rows.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void bindFieldsList()
        {
            try
            {
                clearTableRows(dgFieldsList);

                AddAdditionalUDFs();

                var filteredFieldList = _businessLogic.existingUDFList.Where(tbl => !excludedUDF.Any(e => tbl.Name.Contains(e)) && !tbl.Name.ToLower().Contains("test"));

                foreach (var table in filteredFieldList)
                {
                    var res = JsonConvert.SerializeObject(table);
                    dgFieldsList.Rows.Add(null, table.Name, table.TableName, res);
                }

                lblTotalFieldsCount.Text = Convert.ToString(dgFieldsList.Rows.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void bindObjectList()
        {
            try
            {
                clearTableRows(dgObjectList);

                foreach (var table in _businessLogic.existingUDOList)
                {

                    string name = (string)table.Code;
                    var tableName = table.TableName;

                    if (!excludedUDO.Contains(name) && !name.ToLower().Contains("test"))
                    {
                        var res = JsonConvert.SerializeObject(table);
                        dgObjectList.Rows.Add(null, name, tableName, res);
                    }

                }

                lblTotalObjectsCount.Text = Convert.ToString(dgObjectList.Rows.Count);
            }
            catch (Exception ex)
            {

            }
        }
        private void setScreenResolution()
        {

            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            int w = 1250;//Width >= screen.Width ? screen.Width : Convert.ToInt32((screen.Width + Width) / 2.5);
            int h = 900;//Height >= screen.Height ? screen.Height : (screen.Height + Height) / 2;
            this.Location = new Point((screen.Width - w) / 2, (screen.Height - h) / 2);
            this.Size = new Size(w, h);

        }

        private void AddAdditionalUDFs() {

            var customFieldSideBar = "{\"Name\":\"SIDEBAR\",\"Type\":\"db_Alpha\",\"Size\":50,\"Description\":\"SideBar\",\"SubType\":\"st_None\",\"LinkedTable\":null,\"DefaultValue\":null,\"TableName\":\"OUSR\",\"FieldID\":1,\"EditSize\":50,\"Mandatory\":\"tNO\",\"LinkedUDO\":null,\"LinkedSystemObject\":null,\"ValidValuesMD\":[]}";
            var customFieldDashboard = "{\"Name\":\"DASHBOARD\",\"Type\":\"db_Alpha\",\"Size\":50,\"Description\":\"Dashboard\",\"SubType\":\"st_None\",\"LinkedTable\":null,\"DefaultValue\":null,\"TableName\":\"OUSR\",\"FieldID\":2,\"EditSize\":50,\"Mandatory\":\"tNO\",\"LinkedUDO\":null,\"LinkedSystemObject\":null,\"ValidValuesMD\":[]}";

            var objSIde = JsonConvert.DeserializeObject<UDF>(customFieldSideBar);
            var objDashboard = JsonConvert.DeserializeObject<UDF>(customFieldDashboard);

            _businessLogic.existingUDFList.Add(objSIde);
            _businessLogic.existingUDFList.Add(objDashboard);
        }
        private string GetSessionId()
        {
            string sessionId = string.Empty;
            var dbInfo = ucDatabaseInformation1.GetDatabaseInformation();
            var db = new SBOUserModel() { database = dbInfo.DatabaseName, sboPassword = dbInfo.SBOPassword, sboUserName = dbInfo.SBOUserName };

            if (!string.IsNullOrEmpty(db.sboUserName) && !string.IsNullOrEmpty(db.sboPassword) && !string.IsNullOrEmpty(db.database))
            {
                showBusyCursor();
                var result = _businessLogic.SendLoginRequest(db);
                sessionId = result.Message;
                showDefaultCursor();
            }
            else
            {
                MessageBox.Show(" Provide SBO information. ");
            }

            return sessionId;
        }

        private void btnUDODataBrowsePath_Click(object sender, EventArgs e)
        {

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtMetaDataPath.Text = fbd.SelectedPath;
                }
            }
        }

        private void showBusyCursor() {

            Cursor = Cursors.WaitCursor;
        }

        private void showDefaultCursor()
        {

            Cursor = Cursors.Default;
        }

        private void cbTableSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgTablesList.Rows)
            {
                if (cbTableSelectAll.Checked)
                {
                    row.Cells[0].Value = true;
                }
                else
                {
                    row.Cells[0].Value = false;
                }
            }
        }

        private void cbFieldsSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgFieldsList.Rows)
            {
                if (cbFieldsSelectAll.Checked)
                {
                    row.Cells[0].Value = true;
                }
                else
                {
                    row.Cells[0].Value = false;
                }
            }
        }

        private void cbObjectsSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgObjectList.Rows)
            {
                if (cbObjectsSelectAll.Checked)
                {
                    row.Cells[0].Value = true;
                }
                else
                {
                    row.Cells[0].Value = false;
                }
            }
        }

        private void filterTables()
        {
            if (!string.IsNullOrEmpty(this.txtFilterTable.Text))
            {
                foreach (DataGridViewRow rows in dgTablesList.Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(rows.Cells[1].Value)))
                    {
                        if (rows.Cells[1].Value.ToString().ToLower().Contains(txtFilterTable.Text.Trim().ToLower()))
                        {
                            rows.Visible = true;
                        }
                        else
                        {
                            rows.Visible = false;
                        }

                    }
                }
            }
            else
            {

                showAllRows(dgTablesList);
            }

            lblTotalTableCount.Text = Convert.ToString(dgTablesList.Rows.Cast<DataGridViewRow>().Where(r => r.Visible == true).ToList().Count);
        }

        private void filterFields()
        {
            if (!string.IsNullOrEmpty(this.txtFilterFields.Text))
            {
                foreach (DataGridViewRow rows in dgFieldsList.Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(rows.Cells[1].Value)))
                    {
                        if (rows.Cells[1].Value.ToString().ToLower().Contains(txtFilterFields.Text.Trim().ToLower()) ||
                            rows.Cells[2].Value.ToString().ToLower().Contains(txtFilterFields.Text.Trim().ToLower()))
                        {
                            rows.Visible = true;
                        }
                        else
                        {
                            rows.Visible = false;
                        }

                    }
                }
            }
            else
            {

                showAllRows(dgFieldsList);
            }

            lblTotalFieldsCount.Text = Convert.ToString(dgFieldsList.Rows.Cast<DataGridViewRow>().Where(r => r.Visible == true).ToList().Count);
        }

        private void filterObjects()
        {
            if (!string.IsNullOrEmpty(this.txtFilterObjects.Text))
            {
                foreach (DataGridViewRow rows in dgObjectList.Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(rows.Cells[1].Value)))
                    {
                        if (rows.Cells[1].Value.ToString().ToLower().Contains(txtFilterObjects.Text.Trim().ToLower()))
                        {
                            rows.Visible = true;
                        }
                        else
                        {
                            rows.Visible = false;
                        }

                    }
                }
            }
            else
            {

                showAllRows(dgObjectList);
            }

            lblTotalObjectsCount.Text = Convert.ToString(dgObjectList.Rows.Cast<DataGridViewRow>().Where(r => r.Visible == true).ToList().Count);
        }

        private void txtFilterTable_TextChanged(object sender, EventArgs e)
        {
            filterTables();
        }

        private void txtFilterFields_TextChanged(object sender, EventArgs e)
        {
            filterFields();
        }

        private void txtFilterObjects_TextChanged(object sender, EventArgs e)
        {
            filterObjects();
        }

        private void showAllRows(DataGridView dg)
        {
            foreach (DataGridViewRow rows in dg.Rows)
            {
                rows.Visible = true;
            }
        }

        private void clearTableRows(DataGridView dg)
        {
            dg.Rows.Clear();
            dg.Refresh();
        }
     
    }
}
